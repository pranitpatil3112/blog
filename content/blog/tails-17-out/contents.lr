title: Tails 1.7 is out
---
pub_date: 2015-11-03
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.7, is out.</p>

<p>This release fixes numerous security issues. All users must upgrade as soon as possible.</p>

<h2>New features</h2>

<ul>
<li>You can now start Tails in <a href="https://tails.boum.org/doc/first_steps/startup_options/offline_mode/" rel="nofollow">offline mode</a> to disable all networking for additional security. Doing so can be useful when working on sensitive documents.</li>
<li>We added <a href="https://tails.boum.org/doc/anonymous_internet/icedove/" rel="nofollow">Icedove</a>, a rebranded version of the Mozilla Thunderbird email client.
<p>Icedove is currently a technology preview. It is safe to use in the context of Tails but it will be better integrated in future versions until we remove Claws Mail. Users of Claws Mail should refer to our instructions to <a href="https://tails.boum.org/doc/anonymous_internet/claws_mail_to_icedove/" rel="nofollow">migrate their data from Claws Mail to Icedove</a>.</p></li>
</ul>

<h2>Upgrades and changes</h2>

<ul>
<li>Improve the wording of the first screen of Tails Installer.</li>
<li>Restart Tor automatically if connecting to the Tor network takes too long. (<a href="https://labs.riseup.net/code/issues/9516" rel="nofollow">#9516</a>)</li>
<li>Update several firmware packages which might improve hardware compatibility.</li>
<li>Update the Tails signing key which is now valid until 2017.</li>
<li>Update Tor Browser to 5.0.4.</li>
<li>Update Tor to 0.2.7.4.</li>
</ul>

<h2>Fixed problems</h2>

<ul>
<li>Prevent wget from leaking the IP address when using the FTP protocol. (<a href="https://labs.riseup.net/code/issues/10364" rel="nofollow">#10364</a>)</li>
<li>Prevent symlink attack on ~/.xsession-errors via tails-debugging-info which could be used by the amnesia user to bypass read permissions on any file. (<a href="https://labs.riseup.net/code/issues/10333" rel="nofollow">#10333</a>)</li>
<li>Force synchronization of data on the USB stick at the end of automatic upgrades. This might fix some reliability bugs in automatic upgrades.</li>
<li>Make the "I2P is ready" notification more reliable.</li>
</ul>

<h2>Known issues</h2>

<p>See the current list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">known issues</a>.</p>

<h2>Download or upgrade</h2>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> or <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> page.</p>

<p>If you have been updating automatically for a while and your Tails does not boot after an automatic upgrade, you can <a href="https://tails.boum.org/doc/first_steps/upgrade/#manual" rel="nofollow">update your Tails manually</a>.</p>

<h2>What's coming up?</h2>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for December 15.</p>

<p>Have a look at our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/contribute/how/donate/" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/contribute/talk/" rel="nofollow">talk to us</a>!</p>

<h2>Support and feedback</h2>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

