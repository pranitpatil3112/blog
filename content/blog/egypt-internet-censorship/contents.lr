title: New OONI & AFTE Report Details The State of Internet Censorship in Egypt
---
pub_date: 2018-07-02
---
author: agrabeli
---
tags: egypt
---
categories: global south
---
summary:

The report uncovers anomalies on Egyptian networks, including censorship and the hijacking of unencrypted HTTP connections for advertising. 
---
_html_body:

<p><em><b id="docs-internal-guid-723b40ab-5a25-9df9-c5f0-e51653cc9658">The report uncovers anomalies on Egyptian networks, including censorship and the hijacking of unencrypted HTTP connections for advertising. </b></em></p>
<ul>
<li><a href="https://ooni.io/documents/Egypt-Internet-Censorship-AFTE-OONI-2018-07.pdf"><b>Read the full report in English</b></a></li>
<li><a href="https://ooni.io/documents/Egypt-Internet-Censorship-AFTE-OONI-2018-07.AR.pdf"><b>Read the full report in Arabic</b></a></li>
<li><a href="https://ooni.io/documents/summary-egypt-internet-censorship-arabic.pdf"><b>Read the summary of the report in Arabic</b></a></li>
</ul>
<p dir="ltr">Last year, Egypt <a href="https://cpj.org/2017/05/egypt-blocks-access-to-21-news-websites.php">ordered the blocking of 21 news websites</a>. <a href="https://ooni.torproject.org/">OONI</a>, a censorship measurement project under the Tor Project, responded by publishing a <a href="https://ooni.torproject.org/post/egypt-censors/">report</a> on the blocking of (at least) 10 media websites, including <a href="https://explorer.ooni.torproject.org/measurement/20170527T022637Z_AS24863_a2cVejuvTKddBoVLe3DyDGnPfEnaoUAmP51zo3jTJNEdF6iJJT?input=https:%2F%2Fmadamasr.com">Mada Masr</a> and <a href="https://explorer.ooni.torproject.org/measurement/20170524T220920Z_AS36935_oaDJX0uN8fMLncEu9NQ794dvX4bC3zFejdVouFgsmlHukkjCsn?input=https:%2F%2Fwww.aljazeera.net">Al Jazeera</a>. In an attempt to identify the remaining blocked sites, Egypt’s <a href="https://afteegypt.org/">Association for Freedom of Thought and Expression (AFTE)</a> ran <a href="https://ooni.torproject.org/install/">OONI Probe</a> across multiple networks in Egypt. They subsequently published <a href="https://afteegypt.org/right_to_know-2/publicationsright_to_know-right_to_know-2/2017/06/04/13069-afteegypt.html?lang=en">two research reports</a>, uncovering the blocking of hundreds of URLs (which expand beyond media sites).</p>
<p dir="ltr">OONI and AFTE joined forces. Today, we publish a joint research report on internet censorship in Egypt, based on our analysis of <a href="https://api.ooni.io/files/by_country/EG">OONI network measurements</a> collected between January 2017 to May 2018.</p>
<p>
Our research report is available <a href="https://ooni.io/post/egypt-internet-censorship/">here</a>. In this post, we share some of the key findings.</p>
<h2>Pervasive media censorship</h2>
<p dir="ltr"><a href="https://ooni.io/post/egypt-internet-censorship/eg-anomalous-urls.csv">More than 1,000 URLs</a> presented network anomalies throughout the testing period, <a href="https://ooni.io/post/egypt-internet-censorship/eg-blocked-sites.csv">178</a> of which consistently presented a high ratio of HTTP failures, strongly suggesting that they were blocked. Rather than serving block pages (which would have provided a notification of the blocking), Egyptian ISPs appear to primarily block sites through the use of Deep Packet Inspection (DPI) technology that resets connections.</p>
<p dir="ltr">In some cases, instead of RST injection, ISPs drop packets, suggesting a variance in filtering rules. In other cases, ISPs <a href="https://ooni.io/post/egypt-internet-censorship/eg-ssl-interference-cloudflare.csv">interfere with the SSL encrypted traffic</a> between Cloudflare’s Point-of-Presence in Cairo and the backend servers of sites (<a href="https://explorer.ooni.torproject.org/measurement/20180326T100410Z_AS8452_gn91NHg6tJwnsfwGML6PDsOErBReeB02oh9isOrkRIRgKs30R7?input=https:%2F%2Fpsiphon.ca">psiphon.ca</a>, <a href="https://explorer.ooni.torproject.org/measurement/20180318T090929Z_AS8452_gf96lwrIRx9WnZ1ed8QRhi1sMu2ONRcdleOPfxVGw2yd3Yvf7o?input=https:%2F%2Fwww.purevpn.com%2F">purevpn.com</a> and <a href="https://explorer.ooni.torproject.org/measurement/20180103T093454Z_AS8452_sVEGr9loIPSudsem4KhVtVUT7MYgSonjcQ0itRWWo1DmMGAPEW?input=https:%2F%2Fwww.ultrasawt.com">ultrasawt.com</a>) hosted outside of Egypt. Latency measurements over the last year and a half also suggest that Egyptian ISPs may have changed their filtering equipment and/or techniques, since the latency-based detection of middleboxes has become more challenging.</p>
<p dir="ltr">The chart below illustrates the types of sites that presented the highest amount of network anomalies and are therefore considered to more likely have been blocked.</p>
<p dir="ltr"><img alt="Chart: Blocked sites in Egypt" src="/static/images/blog/inline-images/eg-blocked-sites-chart.png" /></p>
<p dir="ltr"><a href="https://ooni.io/post/egypt-internet-censorship/eg-blocked-news.csv">More than 100 URLs</a> that belong to media organizations appear to have been blocked, even though Egyptian authorities only <a href="https://cpj.org/2017/05/egypt-blocks-access-to-21-news-websites.php">ordered the blocking of 21 news websites</a> last year. These include Egyptian news outlets (such as <a href="https://explorer.ooni.torproject.org/measurement/20170524T220659Z_AS36935_KOMTdBwGsyVovBhs8tYihdTP4ucPSWA5iMH4PqXEfX5TU1ZWK3?input=https:%2F%2Fwww.madamasr.com">Mada Masr</a>, <a href="https://explorer.ooni.torproject.org/measurement/20170711T093730Z_AS8452_JpTTfKV4CebXaq18Vi2xNqgrkh7tmtIpFHUsHgDxLDEecVk1sm?input=http:%2F%2Fwww.almesryoon.com">Almesryoon</a>, <a href="https://explorer.ooni.torproject.org/measurement/20180417T120217Z_AS8452_GSsK1LV6eTr9eP664dZm8U8vqvtmgKa4pR1PEX3B3WdyVMFQ5B?input=http:%2F%2Fwww.masralarabia.com">Masr Al Arabia</a> and <a href="https://explorer.ooni.torproject.org/measurement/20170615T131624Z_AS36935_XbM7CT7wIvIDj338bud9oonjQNKYae4blQfuwMY4ZCrpCO5GJY?input=http:%2F%2Fwww.dailynewsegypt.com%2F">Daily News Egypt</a>), as well as international media sites (such as <a href="https://explorer.ooni.torproject.org/measurement/20180520T073156Z_AS24863_iWUATOpTygIDRHTrgFtxPNlACi2P2m6VeCOeANwu0udfBdTKom?input=http:%2F%2Fwww.aljazeera.com">Al Jazeera</a> and <a href="https://explorer.ooni.torproject.org/measurement/20170615T131624Z_AS36935_XbM7CT7wIvIDj338bud9oonjQNKYae4blQfuwMY4ZCrpCO5GJY?input=http:%2F%2Fwww.huffpostarabi.com%2F">Huffington Post Arabic</a>). Various Turkish and Iranian news websites were blocked (such as <a href="https://explorer.ooni.torproject.org/measurement/20180517T122830Z_AS8452_BQDNLmRk7yc1k0j06HQumDnsZMHP4OQ1Y4OUSqHqdf2rezSpi5?input=http:%2F%2Fturkpress.co">turkpress.co</a> and <a href="https://explorer.ooni.torproject.org/measurement/20180520T073156Z_AS24863_iWUATOpTygIDRHTrgFtxPNlACi2P2m6VeCOeANwu0udfBdTKom?input=http:%2F%2Fwww.alalam.ir">alalam.ir</a>), suggesting that politics and security concerns may have influenced censorship decisions. In an attempt to circumvent censorship, some Egyptian media organizations set up <a href="https://afteegypt.org/right_to_know-2/publicationsright_to_know-right_to_know-2/2017/06/04/13069-afteegypt.html?lang=en">alternative domains</a>, but (in a few cases) they got <a href="https://explorer.ooni.torproject.org/measurement/20180417T120217Z_AS8452_GSsK1LV6eTr9eP664dZm8U8vqvtmgKa4pR1PEX3B3WdyVMFQ5B?input=http:%2F%2Fthedailynewsegypt.com">blocked</a> as well.</p>
<p dir="ltr">
To examine the <em>impact</em> of these censorship events, AFTE interviewed staff members working with some of the Egyptian media organizations whose websites got blocked. They reported that the censorship has had a severe impact on their work. In addition to not being able to publish and losing part of their audience, the censorship has also had a financial impact on their operations and deterred sources from reaching out to their journalists. A number of Egyptian media organizations have <a href="https://www.facebook.com/korabia/posts/768019583402612">suspended</a> their work entirely, as a result of persisting internet censorship.</p>
<p dir="ltr">
Many other websites, beyond media, appear to have been blocked as well. These include human rights websites (such as <a href="https://explorer.ooni.torproject.org/measurement/20180519T000039Z_AS24863_QA46GNkhtspz364ER9SJtRDd6fBWrGMZNm8RIheeNRd3QaKRMH?input=https:%2F%2Fwww.hrw.org%2F">Human Rights Watch</a>, <a href="https://explorer.ooni.torproject.org/measurement/20180520T131028Z_AS8452_5VILUU3mOu51zwtD1E6n2E6Q0KGyq5cuWApNkAaZSCMoHZXxJc?input=https:%2F%2Frsf.org%2F">Reporters without Borders</a>, the <a href="https://explorer.ooni.torproject.org/measurement/20170818T190042Z_AS8452_cNDTrIxYihq4jRlmoqnn8EUoa6scX0v8HwdEmXx67tX3LUMpj2?input=http:%2F%2Fanhri.net">Arabic Network for Human Rights Information</a>, the <a href="https://explorer.ooni.torproject.org/measurement/20180512T065418Z_AS24863_YaAAZE3p4CPwmBElLdEKSMMUH3XD2zCo41NLphcbO5vqS6GsjH?input=http:%2F%2Fwww.ec-rf.org">Egyptian Commission for Rights and Freedoms</a>, and the <a href="https://explorer.ooni.torproject.org/measurement/20180513T124516Z_AS8452_J5Orre5l0MAo8uwzg76CCJ7XcRm8Ya1K6NAI9ASskCFsShUZHI?input=http:%2F%2Fwww.jatoeg.org">Journalists Observatory against Torture</a>) and sites expressing political criticism (such as the <a href="https://explorer.ooni.torproject.org/measurement/nbqhw5kWI9gMrX2qLs2ogfVk0Ukul7imncvAaDCs0kDabAxKQXNfrl6Mfk3fJZyZ?input=http:%2F%2F6april.org">April 6 Youth Movement</a>), raising the question of whether censorship decisions were politically motivated.</p>
<h2>“Defense in depth” tactics for network filtering</h2>
<p dir="ltr">Security experts are probably familiar with the “<a href="https://en.wikipedia.org/wiki/Defense_in_depth_(computing)">defense in depth</a>” concept in which multiple layers of security controls (defense) are placed throughout an IT system, providing redundancy in the event that a security control fails. In Egypt, ISPs seem to apply “defense in depth” tactics for network filtering by creating multiple layers of censorship that make circumvention harder.</p>
<p dir="ltr">This is particularly evident when looking at the <a href="https://explorer.ooni.torproject.org/measurement/20180601T082146Z_AS24835_zCgwzOSmbXDftZn5OftmYZCDf5tw6SrDclPlgiMJ7YREynuOu0?input=http:%2F%2Fwww.fj-p.com">blocking</a> of Egypt’s Freedom and Justice Party (FJP) site. Our testing shows that different versions of this site (<a href="http://www.fj-p.com">http://www.fj-p.com</a> and <a href="http://fj-p.com">http://fj-p.com</a>) were blocked by two different middleboxes. In doing so, Egyptian ISPs added extra layers of censorship, ensuring that circumvention requires extra effort.</p>
<p dir="ltr">Not only were numerous circumvention tool sites (including <a href="https://explorer.ooni.torproject.org/measurement/20180520T131028Z_AS8452_5VILUU3mOu51zwtD1E6n2E6Q0KGyq5cuWApNkAaZSCMoHZXxJc?input=http:%2F%2Fwww.torproject.org">torproject.org</a> and <a href="https://explorer.ooni.torproject.org/measurement/20180520T063449Z_AS24863_dWOpnp9jqv4jOJxMWgAjRLyuWDLEXA1i22J88Y4eTrglGo8g2u?input=https:%2F%2Fpsiphon.ca%2F">psiphon.ca</a>) blocked, but access to the Tor network appears to be blocked as well. Measurements collected from <a href="https://explorer.ooni.torproject.org/measurement/20180522T161301Z_AS24863_oOvnh7hhWXOXQZ4GQfe83vDoB4ORpWotUvDhJeVM50NTcI1d15">Link Egypt (AS24863)</a> and <a href="https://explorer.ooni.torproject.org/measurement/20180521T131355Z_AS8452_BVPG1Lh8gTJJfdphKQKRH38XF2XYwlolxFEiwGlGrKhEt8wUuy">Telecom Egypt (AS8452)</a> suggest that the Tor network is inaccessible, since the tests weren’t able to bootstrap connections to the Tor network within 300 seconds. In recent months, more than 460 measurements show connections to the Tor network failing consistently. Similarly, measurements collected from <a href="https://explorer.ooni.torproject.org/measurement/20171125T004035Z_AS36992_vGkIUAMBKLcmFEcrRVIl9G80tqnl2Xpq95s0xz2vHhDe6KZgyC">Etisalat Misr (AS36992)</a>, <a href="https://explorer.ooni.torproject.org/measurement/20171125T003356Z_AS37069_CpepgnxXwaPzD4OfMwcKnrxwsvlFZLyZKEHtLYRDZua3zN7kM7">Mobinil (AS37069)</a> and <a href="https://explorer.ooni.torproject.org/measurement/20171124T002601Z_AS36935_EjL8eS55NuackoBc1YTlLPYLU4uqhIRpAxltWVhSHooLpN8ZtX">Vodafone (AS36935)</a> indicate that access to the Tor network is blocked. The Tor bootstrap process is likely being disrupted via the <a href="https://ooni.torproject.org/post/egypt-network-interference/#attempts-to-block-tor">blocking of requests to directory authorities</a>.</p>
<p dir="ltr">“Defense in depth” tactics also seem to be applied in relation to the blocking of <a href="https://bridges.torproject.org/">Tor bridges</a>, which enable Tor censorship circumvention. Vodafone appears to be <a href="https://explorer.ooni.torproject.org/measurement/20170606T163348Z_AS36935_FbKgm7SUyJTvlqJuxe5nSHNk98fMPcqjExFsOIEJcdBs1ixqoi?input=obfs4%20176.56.237.144:80">blocking obfs4</a> (shipped as part of Tor Browser), since all attempted connections were unsuccessful (though it remains unclear if private bridges work). All measurements collected from Telecom Egypt show that <a href="https://explorer.ooni.torproject.org/measurement/20170608T001210Z_AS8452_EUzggnPD52mbPUs53DNdtsqg5zTg5SzGXVTiHKvCJzMo0cS39N?input=obfs4%2045.32.175.206:9443%20B44C65B5A61FF946AA33B651C74249A45F3DE945%20cert%3D3w2NyqUsDKODpYdTVuL9tbvqFU1PABzPTyH877gixECvZ%2F0YJeEqp7xfH%2F2ou%2BiNZpJBcw%20iat-mode%3D0">obfs4 works</a>. Given that bridges.torproject.org is <a href="https://explorer.ooni.torproject.org/measurement/20180618T063810Z_AS24835_VbotQZ97S1Ma1fMhdiLfCVsPpjYw7xbNGtWbmvogC0xtHZyCIj?input=https:%2F%2Fbridges.torproject.org">blocked</a>, users can alternatively get Tor bridges by sending an email to <a href="mailto:bridges@torproject.org">bridges@torproject.org</a> (from a <a href="https://riseup.net/">Riseup</a>, <a href="https://mail.google.com/">Gmail</a>, or <a href="https://mail.yahoo.com/">Yahoo</a> account).</p>
<h2 dir="ltr">Ad campaign</h2>
<p dir="ltr">Back in 2016, OONI <a href="https://ooni.torproject.org/post/egypt-network-interference/#advertisement-and-malware-injection">uncovered</a> that state-owned Telecom Egypt was using DPI (or similar networking equipment) to hijack users’ unencrypted HTTP connections and inject redirects to revenue-generating content, such as affiliate ads. The Citizen Lab expanded upon this research, <a href="https://citizenlab.ca/2018/03/bad-traffic-sandvines-packetlogic-devices-deploy-government-spyware-turkey-syria/">identifying</a> the use of Sandvine PacketLogic devices and redirects being injected by (at least) 17 Egyptian ISPs.</p>
<p dir="ltr">
Over the last year, hundreds of <a href="https://api.ooni.io/files/by_country/EG">OONI Probe network measurements</a> (collected from multiple ASNs) <a href="https://ooni.io/post/egypt-internet-censorship/eg-ad-campaign.csv">show</a> the hijacking of unencrypted HTTP connections and the injection of redirects to affiliate ads and cryptocurrency mining scripts. A wide range of different types of URLs were affected, including the sites of the <a href="https://explorer.ooni.torproject.org/measurement/20170814T100330Z_AS8452_dBRZHbzwg0CQy5iGYZn4IXW8xki1TcvvjTd2KxXagPsr2u7ArP?input=http:%2F%2Fwww.ppsmo.org%2F">Palestinian Prisoner Society</a> and the <a href="https://explorer.ooni.torproject.org/measurement/20170622T111520Z_AS24863_HjEg5xCsNPntFqu7BbweBxBXvRVbYgnrXU9USKOUJayrumRpUF?input=http:%2F%2F4genderjustice.org%2F">Women’s Initiatives for Gender Justice</a>, as well as <a href="https://explorer.ooni.torproject.org/measurement/20180225T160034Z_AS8452_hWaybE5mgssZYMFzuKJSP0MN0b4up9ai26W9P8DmFCXAUFySFX?input=http:%2F%2Fwww.bglad.com">LGBTQI</a>, <a href="https://explorer.ooni.torproject.org/measurement/20180103T131521Z_AS36935_4iaqfAGjWfIxPD0u30nvbk2YtCp9UHpH5kcSUFy919YXnsxlsS?input=http:%2F%2Fwww.connectionvpn.com">VPN</a> and <a href="https://explorer.ooni.torproject.org/measurement/20180103T131521Z_AS36935_4iaqfAGjWfIxPD0u30nvbk2YtCp9UHpH5kcSUFy919YXnsxlsS?input=http:%2F%2Fwww.likud.org.il">Israeli</a> sites. Even the sites of the United Nations, such as <a href="https://explorer.ooni.torproject.org/measurement/20171228T105443Z_AS8452_27e7Pg7AwCoT66hN3mSsEZr75YIzpClxFoYmYTa92r7w627Yta?input=http:%2F%2Fwww.un.org%2Frights%2F">un.org</a> and <a href="https://explorer.ooni.torproject.org/measurement/20170622T111520Z_AS24863_HjEg5xCsNPntFqu7BbweBxBXvRVbYgnrXU9USKOUJayrumRpUF?input=http:%2F%2Fwww.ohchr.org%2Fenglish%2Fbodies%2Fhrcouncil%2F">ohchr.org</a>, were among those affected by redirects to ads.</p>
<h2 dir="ltr">Expand upon our research</h2>
<p dir="ltr">This study is part of an ongoing effort to monitor internet censorship in Egypt and around the world. Since this research was carried out through the use of <a href="https://github.com/TheTorProject/ooni-probe">free and open source software</a>, <a href="https://ooni.torproject.org/docs/">open methodologies</a> and <a href="https://api.ooni.io/">open data</a>, it can be reproduced and expanded upon.</p>
<p dir="ltr">Anyone can <a href="https://ooni.torproject.org/install/">run OONI Probe</a> on Android, iOS, macOS, Linux, and on Raspberry Pis. Tens of thousands of OONI Probe users from <a href="https://api.ooni.io/stats">more than 200 countries</a> do so every month. Thanks to their testing, millions of network measurements have been <a href="https://explorer.ooni.torproject.org/world/">published</a>, shedding light on information controls worldwide.</p>
<p dir="ltr">But censorship findings are only as interesting as the types of sites and services that are tested. We therefore encourage you to <a href="https://ooni.torproject.org/get-involved/contribute-test-lists/">contribute to the review and creation of test lists</a>, to help advance future research in Egypt and beyond.</p>
<p dir="ltr">
<em>To learn more about this study, read the full report <a href="https://ooni.io/post/egypt-internet-censorship/">here</a>.</em></p>

---
_comments:

<a id="comment-276043"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276043" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 02, 2018</p>
    </div>
    <a href="#comment-276043">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276043" class="permalink" rel="bookmark">This is all due to Egyptians…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is all due to Egyptians foolishly overthrowing their first democratically elected president for a military regime by the most incapable ruler in its history: al-Sisi</p>
<p>Ta7ya Misr ya as7ab Tor!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276045"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276045" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Yuri (not verified)</span> said:</p>
      <p class="date-time">July 02, 2018</p>
    </div>
    <a href="#comment-276045">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276045" class="permalink" rel="bookmark">The most weak place in Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The most weak place in Tor is bootstrap. I.e., how to get private bridges. If, for example, Tor client send specific broadcast and, then, all nearest private obfuscated bridges reply "We're here, you're welcome!" - like DHCP does, for example - this scheme will more simple for end-users and more reliable. To protect from probing, Tor, for example, can use some specific crypto schemes for auth on bridges. Just an idea.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-276051"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276051" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 03, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-276045" class="permalink" rel="bookmark">The most weak place in Tor…</a> by <span>Yuri (not verified)</span></p>
    <a href="#comment-276051">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276051" class="permalink" rel="bookmark">Does that help?
*user…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does that help?</p>
<p>*user broadcast* "let me join tor"<br />
*censor* "no"</p>
<p>Or the censor may send their own broadcast and block all responding bridges.</p>
<p>The Internet doesn't have any broadcast feature, unless you include nmap/massscan. Very noisy tools. Censorship resistance usually needs to be quiet!</p>
<p>The tor developers are actively researching alternatives. Hyphae is promising.<br />
<a href="https://patternsinthevoid.net/hyphae/hyphae.pdf" rel="nofollow">https://patternsinthevoid.net/hyphae/hyphae.pdf</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-276062"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276062" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>walk like an... (not verified)</span> said:</p>
      <p class="date-time">July 04, 2018</p>
    </div>
    <a href="#comment-276062">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276062" class="permalink" rel="bookmark">shouldn&#039;t the egyptian in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>shouldn't the egyptian in this illustration be wearing a ballcap or something because we can't see the face but the features of the head piece are obvious it's an egyptian!</p>
</div>
  </div>
</article>
<!-- Comment END -->
