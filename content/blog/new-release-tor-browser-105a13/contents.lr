title: New Release: Tor Browser 10.5a13
---
pub_date: 2021-04-05
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.5
---
categories: applications
---
summary: Tor Browser 10.5a13 is now available from the Tor Browser Alpha download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.5a13 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a13/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-10015">latest stable release</a> instead.</p>
<p>This release updates Firefox to 78.9.0esr for desktop and Firefox for Android to 87.0.0. Additionally, we update Tor to 0.4.6.1-alpha and OpenSSL to 1.1.1k and NoScript to 11.2.3. This release includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-11/">security updates</a> to Firefox for Desktop, and similar important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-10/">security updates</a> to Firefox for Android.</p>
<p><b>Important Note</b>: This is the first Tor Browser version that does <b>not</b> support version 2 onion services. Please see the previously published <a href="https://blog.torproject.org/v2-deprecation-timeline">deprecation timeline</a>.</p>
<p><strong>Note:</strong> Tor Browser 10.5 <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40089">does not support CentOS 6</a>.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a11 (windows, macOS, and Linux) and 10.5a12 (Android) is:</p>
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.9.0esr</li>
<li>Update NoScript to 11.2.3</li>
<li>Update Openssl to 1.1.1k</li>
<li>Update Tor to 0.4.6.1-alpha</li>
<li>Translations update</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40030">Bug 40030</a>: DuckDuckGo redirect to html doesn't work</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40032">Bug 40032</a>: Remove Snowflake survey banner from TB-alpha</li>
</ul>
</li>
<li>Android
<ul>
<li>Update Fenix to 87.0.0</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40047">Bug 40047</a>: Rebase android-components patches for Fenix 87.0.0</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40153">Bug 40153</a>: Rebase Fenix patches to Fenix 87.0.0</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40365">Bug 40365</a>: Rebase 10.5 patches on 87.0</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40383">Bug 40383</a>: Disable dom.enable_event_timing</li>
</ul>
</li>
<li>Build System
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Go to 1.15.10</li>
<li><a href="https://bugs.torproject.org/23631">Bug 23631</a>: Use rootless containers [tor-browser-build]</li>
<li><a href="https://bugs.torproject.org/tpo/applications/rbm/40016">Bug 40016</a>: getfpaths is not setting origin_project</li>
</ul>
</li>
<li>Windows
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40252">Bug 40252</a>: Bump mingw-w64 and clang for Firefox 78.9</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291523"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291523" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 05, 2021</p>
    </div>
    <a href="#comment-291523">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291523" class="permalink" rel="bookmark">The last warning: https:/…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The last warning: <a href="https://blog.golang.org/go116-module-changes" rel="nofollow">https://blog.golang.org/go116-module-changes</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291530"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291530" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Thorin (not verified)</span> said:</p>
      <p class="date-time">April 06, 2021</p>
    </div>
    <a href="#comment-291530">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291530" class="permalink" rel="bookmark">why is Bug 40383: Disable…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>why is Bug 40383: Disable dom.enable_event_timing listed as android only?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291577"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291577" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">April 15, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291530" class="permalink" rel="bookmark">why is Bug 40383: Disable…</a> by <span>Thorin (not verified)</span></p>
    <a href="#comment-291577">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291577" class="permalink" rel="bookmark">Because that came in FF87.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Because that came in FF87.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291557"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291557" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>wolf (not verified)</span> said:</p>
      <p class="date-time">April 13, 2021</p>
    </div>
    <a href="#comment-291557">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291557" class="permalink" rel="bookmark">When will Snowflake come to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When will Snowflake come to the stable released of the Tor browser?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291578"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291578" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">April 15, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291557" class="permalink" rel="bookmark">When will Snowflake come to…</a> by <span>wolf (not verified)</span></p>
    <a href="#comment-291578">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291578" class="permalink" rel="bookmark">The current plan is later…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The current plan is later this year.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
