title: Anonymous Publishing and Risking Execution
---
pub_date: 2008-06-01
---
author: phobos
---
tags:

anonymity advocacy
anonymous publishing
risking execution
Terry Eagleton
London Review of Books
---
categories: advocacy
---
_html_body:

<p>Here's a timely reminder of why anonymous publishing tools like Tor<br />
are so critical to free expression. A recent book, <a href="http://www.gyford.com/phil/writing/2008/05/22/risking_executio.php" rel="nofollow">Anonymity: A Secret History of English Literature</a>, covers the history of anonymous publication in English literature, noting that many authors and publishers were imprisoned, tortured, or killed for expressing politically unpopular views.</p>

<blockquote><p>
Risking execution<br />
In the current London Review of Books <a href="http://www.lrb.co.uk/v30/n10/eagl01_.html " rel="nofollow">Terry Eagleton  writes</a> on the history of publishing books anonymously:</p>
<p>There were … legal and political reasons for the ubiquity of Anon. There were times when the state needed to know the author or printer of a work in order to know who to prosecute for heresy or sedition. In 1579, John Stubbs had his right hand cut off for writing a work opposing the marriage of Elizabeth I to a French nobleman. Elizabeth herself urged that the printers of the anti-Anglican Marprelate tracts should be subjected to torture. In 1663, a London printer who published a pamphlet which argued that the monarch should be accountable to his subjects, and justified the people’s right to rebellion, was sentenced to be hanged, drawn and quartered. He refused, even so, to reveal the name of the pamphlet’s author, though the disclosure might have saved his life. Between the 16th and the 18th century, printers were fined, imprisoned and pilloried for publishing supposedly treasonable works whose authors remained concealed. Being Jonathan Swift’s printer was not a job for the faint-hearted.</p>
<p>Looking for modern parallels, can you imagine the head of an ISP risking execution to defend the anonymity of a person publishing something via their servers when a government or company takes exception to it?</p>
<p>I know, ISPs rightly try to avoid being classed as “publishers”, but they’re still the ones offended parties go to to halt online “publication” or reveal authorial identity. I guess a closer parallel would be the owner of a website risking execution to safeguard the identity of a writer whose work they’d knowingly published on their site. Actually, the closest modern day parallel is, duh, a book<br />
publisher, but that doesn’t seem much more likely a situation either.
</p></blockquote>

---
_comments:

<a id="comment-82"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-82" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://SpyBlog.org.uk" content="Watching Them, Watching Us">Watching Them,… (not verified)</a> said:</p>
      <p class="date-time">June 01, 2008</p>
    </div>
    <a href="#comment-82">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-82" class="permalink" rel="bookmark">anonymous publishing tools like Tor and  free expression</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The actual book being reviewed is<br />
<a href="http://www.amazon.co.uk/Anonymity-Secret-History-English-Literature/dp/0571195148" rel="nofollow">Anonymity: A Secret History of English Literature</a> by John Mullan,</p>
<p>Publisher: Faber and Faber (17 Jan 2008)<br />
ISBN-10: 0571195148<br />
ISBN-13: 978-0571195145</p>
<p>For examples of, say, bloggers, who are facing arrest and torture, and possible  execution today, see <a href="http://committeetoprotectbloggers.org/" rel="nofollow">The Committee to Protect Bloggers</a> and the internet specific category at <a href="http://www.rsf.org/rubrique.php3?id_rubrique=273" rel="nofollow">Reporters without Borders</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-83"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-83" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 01, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-82" class="permalink" rel="bookmark">anonymous publishing tools like Tor and  free expression</a> by <a rel="nofollow" href="http://SpyBlog.org.uk" content="Watching Them, Watching Us">Watching Them,… (not verified)</a></p>
    <a href="#comment-83">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-83" class="permalink" rel="bookmark">Thank you for the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for the correction.  I've updated the original entry.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-239"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-239" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>toner  (not verified)</span> said:</p>
      <p class="date-time">September 08, 2008</p>
    </div>
    <a href="#comment-239">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-239" class="permalink" rel="bookmark">So it&#039;s better to remain</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So it's better to remain anonymous than to admit our beliefs? Lucky for us we live in a world where there's a right to free speech.<br />
________<br />
<a href="http://www.cfmenterprises.com/laser-printer-toner.html" rel="nofollow">toner</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-240"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-240" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 08, 2008</p>
    </div>
    <a href="#comment-240">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-240" class="permalink" rel="bookmark">privoxy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i run debian etch.<br />
got the tor(2.0.3) and privoxy(3.0.6) installed.<br />
BUT: privoxy wouldnt accept the line "forward-socks5 / 127..0.0.1:9050"<br />
BUt : same thing works fine with socks4a!<br />
it just sais "unrecognized directive, ignore"!!<br />
im freaking out here !<br />
need some help please.</p>
</div>
  </div>
</article>
<!-- Comment END -->
