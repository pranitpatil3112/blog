title: Our Latest Release of TorBirdy for Thunderbird Includes New Enigmail Features
---
pub_date: 2017-08-04
---
author: sukhbir
---
tags:

TorBirdy
Thunderbird
email
icedove
---
_html_body:

<p class="text-align-center"><em>Photo by Adam Jones. License: CC-BY-4.0</em></p>
<p>TorBirdy is an extension for <a href="https://www.mozilla.org/en-US/thunderbird/">​Mozilla Thunderbird</a> that configures it to make connections over the Tor network. TorBirdy automatically enhances the privacy settings of Thunderbird and configures it for use over Tor -- think of it as <a href="https://www.torproject.org/docs/torbutton/">​Torbutton</a> for Thunderbird.</p>
<p>We are pleased to announce the ninth beta release of TorBirdy: TorBirdy 0.2.3.</p>
<p>This release enables encrypted email headers in Enigmail (as defined by the <a href="http://modernpgp.org/memoryhole/">Memory Hole</a> standard) that helps prevent metadata leaks. Please see Bug <a href="https://bugs.torproject.org/21880">21880</a> for the general discussion on this topic and why we decided to enable this preference (<span class="geshifilter"><code class="php geshifilter-php">extensions<span style="color: #339933;">.</span>enigmail<span style="color: #339933;">.</span>protectHeaders</code></span>) in TorBirdy.</p>
<p>This Enigmail feature encrypts the <em>Subject</em> and <i>References </i>headers by moving them into the encrypted message body. The <em>Subject</em> header text is replaced with the dummy text "<em>Encrypted Message</em>" instead of the original text, which is decrypted automatically when the recipient opens the email if they are using Enigmail. If not, and because this feature is not implemented in other mail clients yet, they will see "Encrypted Message" instead of the <em>subject</em>. Note that the <em>Subject </em>header is not lost -- it is still a part of the message and can be decrypted manually if required. This seems to be a rather small trade off compared to sending the <em>Subject</em> header in plain text.</p>
<p>Note that it is possible to disable this from TorBirdy's preferences in case it breaks your email setup. (Please help us by filing a bug report in case this happens.) For an-easy-to-understand introduction to Memory Hole, please refer to this <a href="https://www.ietf.org/proceedings/92/slides/slides-92-appsawg-0.pdf">presentation</a> (PDF) by Daniel Kahn Gillmor.</p>
<p>If you are using TorBirdy for the first time, visit the <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy">wiki</a> to get started.</p>
<p>There are currently no known leaks in TorBirdy but please note that we are still in beta, so the usual caveats apply.</p>
<p>Here is the complete changelog since v0.2.2:</p>
<blockquote><p><break> 0.2.3, 04 Aug 2017<br />
* Bug <a href="https://bugs.torproject.org/21880">21880</a>: Enable encrypted email headers for Enigmail (Memory Hole)<br />
* Bug <a href="https://bugs.torproject.org/22569">22569</a>: Update Enigmail values for custom proxy settings<br />
* Bug <a href="https://bugs.torproject.org/22318">22318</a>, <a href="https://bugs.torproject.org/22567">22567</a>: Disable Microsoft Family Safety and Google Safe Browsing<br />
* Update keyserver port to 9150 (Tor Browser default) </break></p></blockquote>
<p>We offer two ways of installing TorBirdy: by visiting our <a href="https://dist.torproject.org/torbirdy/torbirdy-0.2.3.xpi">website</a> (<a href="https://dist.torproject.org/torbirdy/torbirdy-0.2.3.xpi.asc">GPG signature</a>; signed by <a href="https://www.torproject.org/docs/signing-keys.html.en">0xB01C8B006DA77FAA</a>) or by visiting the <a href="https://addons.mozilla.org/en-us/thunderbird/addon/torbirdy/">Mozilla Add-ons page</a> for TorBirdy.</p>
<p>Please note that there may be a delay -- which can range from a few hours to days -- before the extension is reviewed by Mozilla and updated on the Add-ons page. </p>
<p>The <a href="https://packages.debian.org/sid/xul-ext-torbirdy">TorBirdy package</a> for Debian GNU/Linux will be uploaded shortly by Ulrike Uhlig.</p>
<p><break> </break></p>

---
_comments:

<a id="comment-270159"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270159" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>lino (not verified)</span> said:</p>
      <p class="date-time">August 04, 2017</p>
    </div>
    <a href="#comment-270159">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270159" class="permalink" rel="bookmark">Great works, thanks for your…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great works, thanks for your great work.</p>
<p>What's the risk that thunderbird may leak urls like Firefox does via gvfs? (<a href="https://blog.torproject.org/blog/tor-browser-703-released" rel="nofollow">https://blog.torproject.org/blog/tor-browser-703-released</a>)</p>
<p>Tbb/Tor Button allow the use of Unix sockets ports, is it possible to use them in Torbirdy, too?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270180"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270180" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270159" class="permalink" rel="bookmark">Great works, thanks for your…</a> by <span>lino (not verified)</span></p>
    <a href="#comment-270180">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270180" class="permalink" rel="bookmark">As long as you don&#039;t use…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As long as you don't use HTML emails and don't click on weird links in spam emails I think the risk is not very big.</p>
<p>Regarding unix domain sockets: I think you should be able to configure Torbirdy to use them (but I have not tried). There are no Tor Browser patches regarding those sockets anymore that are not ustreamed.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270185"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270185" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>lino (not verified)</span> said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-270185">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270185" class="permalink" rel="bookmark">mcs wrote https://blog…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>mcs wrote <a href="https://blog.torproject.org/comment/268858#comment-268858" rel="nofollow">https://blog.torproject.org/comment/268858#comment-268858</a></p>
<blockquote><p>1. Open about:config in a browser window.<br />
2. Toggle the following two preferences so that their value becomes true:<br />
extensions.torlauncher.control_port_use_ipc<br />
extensions.torlauncher.socks_port_use_ipc<br />
3. Restart Tor Browser.</p></blockquote>
<p>Unfortunately, thunderbird/torbirdy doesn't use torlauncher -- could you give me a hint, what I have to do to use thunderbird/torbirdy with Unix sockets ports?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-270160"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270160" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Engels Peralta (not verified)</span> said:</p>
      <p class="date-time">August 04, 2017</p>
    </div>
    <a href="#comment-270160">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270160" class="permalink" rel="bookmark">Is there a mobile version?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there a mobile version?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270162"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270162" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>carlosdanger (not verified)</span> said:</p>
      <p class="date-time">August 04, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270160" class="permalink" rel="bookmark">Is there a mobile version?</a> by <span>Engels Peralta (not verified)</span></p>
    <a href="#comment-270162">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270162" class="permalink" rel="bookmark">Well, it&#039;s a Thunderbird…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, it's a Thunderbird extension, so, is there a mobile version of Thunderbird? According to Mozilla's website, Thunderbirds supports Windows, macOS, Linux (<a href="https://www.mozilla.org/en-US/thunderbird/" rel="nofollow">https://www.mozilla.org/en-US/thunderbird/</a>). So I guess, that their's somewhere an Andoid/*Linux* version of it, otherwise they wouldn't say so, would they?</p>
<p>Additionaly, it might be not the best idea to put your OpenPGP key/login data on your stock Android phone.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-270168"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270168" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jerec reyes (not verified)</span> said:</p>
      <p class="date-time">August 05, 2017</p>
    </div>
    <a href="#comment-270168">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270168" class="permalink" rel="bookmark">Nice..</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nice..</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-270169"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270169" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 05, 2017</p>
    </div>
    <a href="#comment-270169">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270169" class="permalink" rel="bookmark">Anyone has a quick advice?…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Anyone has a quick advice?</p>
<p>My TorBirdy is set for "Transparent Torification" needed when Tor is running in a separate Whonix gateway VM.  Clicking the "Test Proxy" button loads the "Success" page, and Thunderbird receives all emails OK.<br />
The sent emails are arriving OK to the few email recipients on the same domain / mail server. However, the emails sent to the recipients at other mail services (all other email recipients) never arrive there. There are no visible error messages when sending email.</p>
<p>What's wrong?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270199"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270199" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>cypherpunk (not verified)</span> said:</p>
      <p class="date-time">August 07, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270169" class="permalink" rel="bookmark">Anyone has a quick advice?…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-270199">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270199" class="permalink" rel="bookmark">If you are using Whonix, you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you are using Whonix, you should use the following settings under TorBirdy preferences.</p>
<p>Proxy -&gt; Choose an anonymization service -&gt; Whonix</p>
<p>May be this help, may be not.</p>
<p>Cheers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270500"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270500" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 16, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270199" class="permalink" rel="bookmark">If you are using Whonix, you…</a> by <span>cypherpunk (not verified)</span></p>
    <a href="#comment-270500">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270500" class="permalink" rel="bookmark">Choosing the &quot;Whonix&quot;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Choosing the "Whonix" setting: I don't use standard Whonix completely (not in Whonix Workstation behind Gateway setup).  I'm in a custom workstation behind Whonix gateway. Because of that, I chose Transparent Torification.</p>
<p>"May be this will help": no, even choosing "Whonix" option does not help. You see, Thunderbird communicates with my mail server OK. The messages are sent successfully. And my own server has no problem at all. But perhaps most other servers don't like something Torbirdy does to the email headers?</p>
<p>Please help!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270870"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270870" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 19, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270500" class="permalink" rel="bookmark">Choosing the &quot;Whonix&quot;…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-270870">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270870" class="permalink" rel="bookmark">However, the emails sent to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><em>However, the emails sent to the recipients at other mail services (all other email recipients) never arrive there. There are no visible error messages when sending email.</em></p>
<p>What's your email provider? Does your provider support starttls, tls v1.2? Have you set the right smtp settings (name, port) are you using a tor onion service?<br />
(Does it work with older torbirdy versions? Try not to enforce tls 1.2 but set tls min to 1.0)</p>
<p>Maybe the Whonix forum/irc channel is a better place for help (other whonix users may have solved the issue already).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-270177"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270177" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Sam (not verified)</span> said:</p>
      <p class="date-time">August 06, 2017</p>
    </div>
    <a href="#comment-270177">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270177" class="permalink" rel="bookmark">Great awesome work. Should…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great awesome work. Should be continued</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-270202"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270202" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>david (not verified)</span> said:</p>
      <p class="date-time">August 07, 2017</p>
    </div>
    <a href="#comment-270202">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270202" class="permalink" rel="bookmark">Hi, …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi, </p>
<p>here is my error report:</p>
<p>Torbirdy 0.2.3, Thunderbird 52.2.1 , Debian Stretch.</p>
<p>I start the Tor Browser Bundle and then start Thunderbird with TorBirdy. I try to send a message. But I have this message :<br />
Send Message Error<br />
Sending of message failed.<br />
The message could not be sent because connecting to SMTP server disroot.org failed. The server may be unavailable or is refusing SMTP connections. Please verify that your SMTP server settings are correct and try again, or contact the server administrator.</p>
<p>I tried to send with another server, but I got the same message.</p>
<p>Any idea?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270307"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270307" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 08, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270202" class="permalink" rel="bookmark">Hi, …</a> by <span>david (not verified)</span></p>
    <a href="#comment-270307">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270307" class="permalink" rel="bookmark">try,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>try,</p>
<p>a) click on "torbirdy,enabled" -&gt; "Open Torbirdy Preferences" -&gt; "Test Proxy Settings", does it say "Congratulations. This browser is configured to use Tor."</p>
<p>b) does the server support tls 1.2 and modern ciphers?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-270293"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270293" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>David (not verified)</span> said:</p>
      <p class="date-time">August 08, 2017</p>
    </div>
    <a href="#comment-270293">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270293" class="permalink" rel="bookmark">Ok, I found my problem:…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ok, I found my problem: torbirdy simpy changed my smtp configuration from STARTTLS to SSL. I put back STARTTLS and it works like a charm!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270312"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270312" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 08, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270293" class="permalink" rel="bookmark">Ok, I found my problem:…</a> by <span>David (not verified)</span></p>
    <a href="#comment-270312">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270312" class="permalink" rel="bookmark">STARTTLS doesn&#039;t enforce TLS…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>STARTTLS doesn't enforce TLS (ergo plaintext is a possible fallback) and it seems as if they only support starttls for smtp [1]. Additionally, disroot.org is using Diffie-Hellman parameter's with only 1024 bits (there are rumors that at least one three letter agency is able to decipher them in real-time) and has a questionable set of ciphers [2, 3]. You should at least consider to choose a different mail provider that has a somewhat more 'secure' tls configuration (and/or uses an onion address) like mailbox.org [4, 5]</p>
<p>[1] <a href="https://forum.disroot.org/t/email-how-to-setup-email-clients/213" rel="nofollow">https://forum.disroot.org/t/email-how-to-setup-email-clients/213</a><br />
[2] <a href="https://www.htbridge.com/ssl/?id=sJRTvHf6" rel="nofollow">https://www.htbridge.com/ssl/?id=sJRTvHf6</a><br />
[3] <a href="https://tls.imirhil.fr/tls/disroot.org:995" rel="nofollow">https://tls.imirhil.fr/tls/disroot.org:995</a><br />
[4] smtp <a href="https://tls.imirhil.fr/tls/smtp.mailbox.org:465" rel="nofollow">https://tls.imirhil.fr/tls/smtp.mailbox.org:465</a><br />
[5] mx-server  <a href="https://tls.imirhil.fr/smtp/tls.mailbox.org" rel="nofollow">https://tls.imirhil.fr/smtp/tls.mailbox.org</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270398"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270398" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 10, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270312" class="permalink" rel="bookmark">STARTTLS doesn&#039;t enforce TLS…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-270398">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270398" class="permalink" rel="bookmark">&quot;using Diffie-Hellman…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"using Diffie-Hellman parameter's with only 1024 bits (there are rumors that at least one three letter agency is able to decipher them in real-time)"</p>
<p>rumors:<br />
<a href="https://theintercept.com/2017/05/11/nyu-accidentally-exposed-military-code-breaking-computer-project-to-entire-internet/" rel="nofollow">https://theintercept.com/2017/05/11/nyu-accidentally-exposed-military-c…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-270867"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270867" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>arix (not verified)</span> said:</p>
      <p class="date-time">August 19, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-270398" class="permalink" rel="bookmark">&quot;using Diffie-Hellman…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-270867">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270867" class="permalink" rel="bookmark">In 2015 at the c3c, there&#039;s…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In 2015 at the c3c, there's was at talk about possible attacks on the Diffie-Hellman key exchange by exploiting the fact most services use the same prime number and by doing pre-computations for, say 2-3 prime numbers, one could, at least for those 2-3 prime numbers, attack the Diffie-Hellman 1024 bit key exchange for 80--90% of all websites (the estimated (2015) development costs for semi-custom chips for the post-"pre-computation"-phase were by approx. 100 million dollar).</p>
<p>Link to the talk: <a href="https://media.ccc.de/v/32c3-7288-logjam_diffie-hellman_discrete_logs_the_nsa_and_you" rel="nofollow">https://media.ccc.de/v/32c3-7288-logjam_diffie-hellman_discrete_logs_th…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-270508"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-270508" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>pause | break (not verified)</span> said:</p>
      <p class="date-time">August 16, 2017</p>
    </div>
    <a href="#comment-270508">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-270508" class="permalink" rel="bookmark">the bird is mocking me with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>the bird is mocking me with his eyes. please make it stop.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271160"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271160" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ещё зелёный совсем (not verified)</span> said:</p>
      <p class="date-time">September 01, 2017</p>
    </div>
    <a href="#comment-271160">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271160" class="permalink" rel="bookmark">Привет всем! Ребята, иногда…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Привет всем! Ребята, иногда не запускается orbot на android. Включаю "методом тыка". Почему так происходит и что нужно сделать, чтобы наладить   TOR? Я в этих делах ещё совсем "чайник". Помогите разобраться, <strong>пожалуйста<strong></strong></strong>. Заранее благодарен. Громко не смейтесь смейтесь надо мной!))<br />
Всем добра.<br />
Крым</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-275845"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275845" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>By (not verified)</span> said:</p>
      <p class="date-time">June 25, 2018</p>
    </div>
    <a href="#comment-275845">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275845" class="permalink" rel="bookmark">Can anyone tell me once and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can anyone tell me once and for all if this is correct:</p>
<p>When checking multiple accounts with Thunderbird using TorBirdy, the same IP is used for all accounts; unless one manually changes identity between each single fetching.</p>
<p>(I could not find the answer anywhere else and my poor english does not allow me to hover the whole site fast enough.)</p>
<p>Would it be somehow possible to make TorBirdy change Tor identity for each account checking (or assign one at startup to each account)?</p>
<p>Thnks a lot.</p>
</div>
  </div>
</article>
<!-- Comment END -->
