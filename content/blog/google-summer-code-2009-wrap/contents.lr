title: Google Summer of Code 2009 Wrap-up
---
pub_date: 2009-10-27
---
author: karsten
---
tags:

gsoc
gsoc 2009
---
categories: internships
---
_html_body:

<p>Attending the Google Summer of Code Mentor Summit feels like the perfect time to finally write the wrap up of this year's <a href="http://socghop.appspot.com/program/home/google/gsoc2009" rel="nofollow">Google Summer of Code</a>. So, what did we learn in our third year of participation?</p>

<p>We had 5 students funded by Google to work on Tor over this summer, plus 1 more for The <a href="https://www.eff.org/" rel="nofollow">Electronic Frontier Foundation</a>. We had to pick these 6 out of 32 applications, which was a pretty hard process for us. In retrospect, there were at least 2 more students that we'd really have wanted to work on Tor but that we were not able to pick. Fortunately, they stuck with the project anyway, writing a neat <a href="https://blog.torproject.org/blog/summer-conclusion-arm-project" rel="nofollow">relay monitor</a> and helping reimplement Tor in Java for mobile devices.</p>

<p>Does that mean we should have asked Google for more slots than 5+1? Definitely no. 5 students are the limit for the Tor project. We did make the mistake of asking for too many slots and mentoring too many students in 2008, and we have learned our lesson. It was good to have 5+1 slots—even 4+1 would have worked fine. The limiting factor simply is mentoring time. Even this year, some of our mentors bit off more than they could chew. Mentoring takes more time than one would expect: answering students' questions, keeping track of their code, deciding whether they are still on track or need more help, etc. At the same time, the mentors' schedules are already overfull, communication is hard while traveling, unexpected things happen in life, and so forth. So, in order to compensate for these situations, we need backup mentors to step in and help out. That doesn't allow for accepting even more students.</p>

<p>Another lesson we learned this year is that both students and mentors need to become more open with respect to letting the world know about their progress. Having to write up status reports every week or two is a great way to avoid surprises. Students know that, but it still takes time and effort to write status updates. Mentors know that, but they feel they already know how far their students have got. Organization admins know that, but they can only appeal to their students and mentors so often and then need to trust them to do the right thing. This is bad. The lesson learned should be that students that do not write three status report before mid-term or final evaluation automatically fail. Sounds harsh, but it might have avoided having to fail one of our students at this year's final evaluation.</p>

<p>So much about learning lessons. What did our students achieve in their summer projects? We asked all of them to write up their experiences in a blog post. Read more about <a href="https://blog.torproject.org/blog/summer-torbutton" rel="nofollow">Kory's</a>, <a href="https://blog.torproject.org/blog/polipo-portability-enhancements-summary" rel="nofollow">Chris's</a>, <a href="https://blog.torproject.org/blog/bittorrent-support-thandy" rel="nofollow">Sebastian's</a>, and <a href="https://blog.torproject.org/blog/website-translation-support-translationtorprojectorg" rel="nofollow">Runa's</a> Summer of Code of experiences. We are really happy to see that all four of them stick with the Tor project and keep on contributing code. Kory is working next on the implementation of <a href="https://www.torproject.org/hidden-services.html.en" rel="nofollow">hidden services</a> in a Java version of Tor. Chris keeps sending us patches for <a href="http://www.monkey.org/~provos/libevent/" rel="nofollow">libevent</a> that fix problems with Windows. Sebastian helps in diagnosing and fixing bugs in Tor and does a good share of user support. Runa helps us set up a new virtual machine hosting our translation portal.</p>

<p>In retrospect, we think that one of the main motivating factors for our students to stick around in our community is the fact that we met with them in person. We met Chris (who worked on Polipo) and Damian (who worked on the relay monitor) at this year's <a href="http://petsymposium.org/2009/" rel="nofollow">Privacy Enhancing Technologies Symposium</a> in August in Seattle, WA, USA. A few days later we met Sebastian at <a href="https://wiki.har2009.org/page/Main_Page" rel="nofollow">Hacking at Random</a> in the Netherlands. Roger and Jake met Kory in person in September in Philadelphia. Only Runa we did not meet in person, but we hope to see her at the <a href="http://events.ccc.de/congress/2009/wiki/index.php/Welcome" rel="nofollow">Chaos Communication Congress</a> in December in Berlin, Germany.</p>

<p>Thank you, Google, for the Summer of Code program! We really appreciate being part of it!</p>

---
_comments:

<a id="comment-2967"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2967" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>SwissTorExit (not verified)</span> said:</p>
      <p class="date-time">October 28, 2009</p>
    </div>
    <a href="#comment-2967">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2967" class="permalink" rel="bookmark">re:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello and thanks to the students for there work.</p>
<p>I hope to be in Berlin this year in Congress and meet some peoples from Tor project.</p>
<p>I hope the 4 students will stay and continue to help and developp Tor.</p>
<p>Best Regards</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2993"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2993" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>NoTory (not verified)</span> said:</p>
      <p class="date-time">November 03, 2009</p>
    </div>
    <a href="#comment-2993">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2993" class="permalink" rel="bookmark">download link</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>since the download link at the main page <a href="http://www.torproject.org" rel="nofollow">www.torproject.org</a> has always been blocked, I hope it could be added here so that we have more chances to for downloads or upgrades. thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3083"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3083" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 11, 2009</p>
    </div>
    <a href="#comment-3083">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3083" class="permalink" rel="bookmark">Thank you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3084"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3084" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 11, 2009</p>
    </div>
    <a href="#comment-3084">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3084" class="permalink" rel="bookmark">Hello, may I ask tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello, may I ask tor software support "Windows 7" operating system? If not, the trouble to ask, tor the software can be achieved when the support of "Windows 7" operating system, thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3088"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3088" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">November 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3084" class="permalink" rel="bookmark">Hello, may I ask tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-3088">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3088" class="permalink" rel="bookmark">re: tor win7</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's been reported to work fine.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
