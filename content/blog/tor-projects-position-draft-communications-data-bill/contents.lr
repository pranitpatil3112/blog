title: The Tor Project's position on the draft Communications Data Bill
---
pub_date: 2012-11-29
---
author: sjmurdoch
---
_html_body:

<p>The UK government has proposed a new bill which would allow UK law enforcement agencies to require that "telecommunication operators" (e.g. ISPs and website operators) intercept and record their users' traffic data (i.e. details of who is communicating with whom, when, from where, and how much, but not the content of communications). The <a href="http://www.official-documents.gov.uk/document/cm83/8359/8359.pdf" rel="nofollow">draft of this bill</a>, the Communications Data Bill (dubbed the "Snoopers' Charter" by some), has been published and has met widespread criticism for the unprecedented intrusion of privacy it would permit.</p>

<p>The impact on Tor is less than some have feared, because it is likely that The Tor Project is not a telecommunication operator for the purposes of the bill (because the nodes which carry data are not run by The Tor Project) and Tor's distributed architecture reduces the harm which may be caused by the compromise of traffic data. However, the proposed bill is still bad for privacy, especially for users of systems which don't offer the same protections as Tor, so I submitted written evidence to the parliamentary committee investigating the bill, on behalf of the Tor Project.</p>

<p><a href="http://www.cl.cam.ac.uk/~sjm217/papers/parliament12commsdata.pdf" rel="nofollow">Our submission</a> gave an introduction to Tor, how it works, and how it is used, and in particular how important Tor was for maintaining the safety of human rights activists working in repressive regimes. The submission also discusses the risks of the proposed bill, especially the harm which would come if traffic data collected were compromised (as happened <a href="http://www.wired.com/threatlevel/2010/01/operation-aurora/" rel="nofollow">to Google</a>) or interception equipment installed for complying with the bill were enabled without authorization (as happend <a href="http://spectrum.ieee.org/telecom/security/the-athens-affair/" rel="nofollow">to Vodafone in Greece</a>).</p>

<p>Our submission has been published with the <a href="http://www.parliament.uk/documents/joint-committees/communications-data/written%20evidence%20Volume.pdf" rel="nofollow">others</a>, and I was also invited to give evidence to the committee in person. The <a href="http://www.parliament.uk/documents/joint-committees/communications-data/uc060912ev8HC479viii%20(consol%20panel%202).pdf" rel="nofollow">transcript of this session</a> has been published with some minor redactions requested by other companies presenting evidence, but none of my answers have been redacted. Further information about the activities of this committee, including transcripts of other sessions, can be found on <a href="http://www.parliament.uk/business/committees/committees-a-z/joint-select/draft-communications-bill/" rel="nofollow">committee's page</a>.</p>

<p>Based on the discussions which have taken place, it appears that the committee has serious reservations about the bill but we will not know for sure until the committee publishes their report, expected within a few weeks. Efforts to campaign against the bill continue, particularly by the <a href="http://www.openrightsgroup.org/issues/ccdp" rel="nofollow">Open Rights Group</a>.</p>

---
_comments:

<a id="comment-18116"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18116" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2012</p>
    </div>
    <a href="#comment-18116">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18116" class="permalink" rel="bookmark">I love your generous</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I love your generous assistance. Will donate regularly to your org.<br />
Love Torproject.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-18117"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18117" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 03, 2012</p>
    </div>
    <a href="#comment-18117">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18117" class="permalink" rel="bookmark">hi.i use tor for write my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi.i use tor for write my comment on a government news website in iran! but when they publish the comment i found my location is detected and is iran! how it is possible?!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-18163"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18163" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 10, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-18117" class="permalink" rel="bookmark">hi.i use tor for write my</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-18163">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18163" class="permalink" rel="bookmark">Two possible</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Two possible explanations:</p>
<p>1. You didn't use tor properly. Use tor browser bundle for browsing internet and keep it updated.<br />
2. Someone from Iran shared exit node - nothing to worry about.</p>
<p>You should check if you're using tor here: <a href="https://check.torproject.org/" rel="nofollow">https://check.torproject.org/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-18127"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18127" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 04, 2012</p>
    </div>
    <a href="#comment-18127">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18127" class="permalink" rel="bookmark">I am wondering how this bill</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am wondering how this bill would affect me as someone who is running several high capacity Tor exit nodes in the UK.</p>
</div>
  </div>
</article>
<!-- Comment END -->
