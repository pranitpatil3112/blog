title: Join Our DocsHackathon Next Week
---
pub_date: 2019-08-27
---
author: pili
---
tags:

documentation
hackathon
---
categories: community
---
summary:

Documentation is extremely valuable to the health of open source software projects, but it is often overlooked. Due to the amount of interest we received during our search for a Google Season of Docs candidate, we're kicking off a week-long documentation hackathon Monday 2nd September 00:00UTC to Friday 6th September 23:59UTC. *Update: The DocsHackathon has been extended to Monday 9th September 14:59 UTC. 
---
_html_body:

<p><strong>*Update: The DocsHackathon has been extended to Monday 9th September 14:59 UTC. </strong></p>
<p>Documentation is extremely valuable to the health of open source software projects, but <a href="https://opensourcesurvey.org/2017/">it is often overlooked</a>. Due to the amount of interest we received during our search for a <a href="https://blog.torproject.org/google-season-docs-2019-help-tor-improve-our-documentation">Google Season of Docs</a> candidate, we're kicking off a week-long documentation hackathon Monday 2nd September 00:00UTC to Friday 6th September 23:59UTC. <strong>This is your opportunity to help us keep our documentation up to date and relevant for millions of Tor users around the world. We’ll also be rewarding the top 3 contributors with prizes at the end of the week.</strong><br />
 <br />
We are looking for copywriters, front-end devs, testers, and content reviewers to help us improve our documentation and its relevancy. Don't feel like any of these apply to you but still want to help out? Chat with us on IRC (#tor-www - irc.oftc.net) or the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-community-team">community team mailing list</a> to join us and get involved.</p>
<h2>How to participate in the DocsHackathon</h2>
<ol>
<li>Register: <a href="https://survey.torproject.org/index.php/726752?lang=en">https://survey.torproject.org/index.php/726752?lang=en</a></li>
<li>Take a look at all of the tickets marked with the <a href="https://dip.torproject.org/groups/web/-/issues?scope=all&amp;utf8=%E2%9C%93&amp;state=opened&amp;label_name[]=DocsHackathon">"DocsHackathon" keyword on Gitlab</a>.</li>
<li>If you have a documentation issue that is not currently reflected in Trac, Gitlab, or GitHub, create it, tag it, and let one of us know on <a href="https://trac.torproject.org/projects/tor/wiki/org/onboarding/IRC">IRC</a> channel #tor-www.</li>
<li>Choose a ticket and start working on it! You can submit Pull Requests on GitHub or register on our Gitlab instance to submit merge requests there.</li>
<li>A contribution will be counted when your PR or merge request is merged to the master branch of the relevant repository.</li>
<li>The awards and prizes to contributors will be announced after all the merges are done.</li>
</ol>
<p>For more details on how you can contribute, check out our <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/CommunityTeam/DocsHackathon">DocsHackathon wiki.</a><br />
 <br />
We are a small nonprofit with a big mission, and we sincerely appreciate your help getting our documentation up to speed. We're looking forward to working with you next week!</p>
<hr />
<p><em>Thanks to clash for the image: <a href="aadibajpai.me">aadibajpai.me</a></em></p>

---
_comments:

<a id="comment-283555"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283555" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Abdulmueed (not verified)</span> said:</p>
      <p class="date-time">August 27, 2019</p>
    </div>
    <a href="#comment-283555">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283555" class="permalink" rel="bookmark">I really want to be part of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I want to be part of this</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-283557"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283557" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">August 27, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-283555" class="permalink" rel="bookmark">I really want to be part of…</a> by <span>Abdulmueed (not verified)</span></p>
    <a href="#comment-283557">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283557" class="permalink" rel="bookmark">Great! We look forward to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great! We look forward to working with you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-283573"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283573" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Nemo (not verified)</span> said:</p>
      <p class="date-time">August 28, 2019</p>
    </div>
    <a href="#comment-283573">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283573" class="permalink" rel="bookmark">Is it safe to help ? Are…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it safe to help ? Are there risks to helping ?<br />
Are there security measures participants should take in order to take part ?</p>
<p>Thanks !</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-283593"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283593" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  ggus
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Gus</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">Gus said:</p>
      <p class="date-time">August 30, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-283573" class="permalink" rel="bookmark">Is it safe to help ? Are…</a> by <span>Nemo (not verified)</span></p>
    <a href="#comment-283593">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283593" class="permalink" rel="bookmark">Hello, it&#039;s safe as…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello, it's safe as commenting on Tor Project blog or writing any other public document.</p>
<p>Participants concerned with their security can create throwaway git and email accounts and double check if they aren't submitting their real email/username, e.g.: <a href="https://help.github.com/en/articles/setting-your-commit-email-address" rel="nofollow">https://help.github.com/en/articles/setting-your-commit-email-address</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-283606"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283606" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 31, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-283573" class="permalink" rel="bookmark">Is it safe to help ? Are…</a> by <span>Nemo (not verified)</span></p>
    <a href="#comment-283606">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283606" class="permalink" rel="bookmark">The documentation for how to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The <a href="https://trac.torproject.org/projects/tor/wiki/doc/SupportPrograms" rel="nofollow">documentation for how to torify programs</a> on Trac does not list git.  That should be counted for a documentation improvement to prepare for this hackathon.</p>
<p>Personally, I wouldn't use GitHub at all as it is a 3rd party.  I would try to use Tor Project's <a href="https://dip.torproject.org/groups/web/-/issues?scope=all&amp;utf8=%E2%9C%93&amp;state=opened&amp;label_name[]=DocsHackathon" rel="nofollow">Gitlab</a> and <a href="https://trac.torproject.org/projects/tor/query?status=accepted&amp;status=assigned&amp;status=merge_ready&amp;status=needs_information&amp;status=needs_review&amp;status=needs_revision&amp;status=new&amp;status=reopened&amp;keywords=~DocsHackathon&amp;col=id&amp;col=summary&amp;col=status&amp;col=type&amp;col=priority&amp;col=milestone&amp;col=component&amp;order=priority" rel="nofollow">Trac</a>/<a href="https://gitweb.torproject.org/" rel="nofollow">GitWeb</a> instances in the "project/web/" repositories.</p>
<p><a href="https://stackoverflow.com/questions/10274879/how-to-contribute-on-github-anonymously-via-tor" rel="nofollow">Stackoverflow: How to contribute on github [and other git servers] anonymously via Tor?</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-283605"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283605" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Min (not verified)</span> said:</p>
      <p class="date-time">August 31, 2019</p>
    </div>
    <a href="#comment-283605">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283605" class="permalink" rel="bookmark">I so want to get involved it…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I so want to get involved it would be great to become a tester or anything like it reall</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-283607"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-283607" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 31, 2019</p>
    </div>
    <a href="#comment-283607">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-283607" class="permalink" rel="bookmark">Out of curiosity, did you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Out of curiosity, did Tor Project consider Gitea when choosing Gitlab?  I've seen a lot more onion services running Gitea than running Gitlab.</p>
</div>
  </div>
</article>
<!-- Comment END -->
