title: Tor's Innovative Metrics Program Receives Award from Mozilla
---
pub_date: 2016-06-22
---
author: karsten
---
tags:

funding
metrics
---
categories:

fundraising
metrics
---
_html_body:

<p>Good news for data enthusiasts who trust numbers more than words: The Tor Project has just received an award from Mozilla's Open Source Support program to <a href="https://blog.mozilla.org/blog/2016/06/22/mozilla-awards-385000-to-open-source-projects-as-part-of-moss-mission-partners-program/" rel="nofollow">improve Tor metrics</a> over the next 12 months. </p>

<p>While some analytics programs collect data in ways that violate the privacy of users, Tor's metrics program seeks to keep users safe as we collect and analyze data. We use the data to develop ways to allow more people to access the free Internet via Tor, and we make all data available to the world, so that Tor users, developers, journalists, and funders can see and understand the ways that people use Tor worldwide.</p>

<p><a href="https://www.mozilla.org/en-US/mission/" rel="nofollow">Mozilla's mission</a> is to ensure the Internet is a global public resource, open and accessible to all.  <a href="https://wiki.mozilla.org/MOSS" rel="nofollow">Mozilla Open Source Support (MOSS)</a> is an awards program specifically focused on supporting the Open Source and Free Software movement.  Their <a href="https://wiki.mozilla.org/MOSS/Mission_Partners" rel="nofollow">Mission Partners track</a> is open to any open source/free software project undertaking an activity which significantly furthers Mozilla's mission.</p>

<p>Over the coming year, our main goals for this project will be:</p>

<p> 1. To make  <a href="https://collector.torproject.org/" rel="nofollow">CollecTor</a> (our primary data collection service) more resilient to single-point failures, by enabling multiple CollecTor instances to gather data independently and exchange it in an automated fashion.  Doing this will reduce the number of gaps in our data, and make it less likely that an error at one server will make the data invalid.</p>

<p> 2. To create an easy-to-use observation kit containing <a href="https://dist.torproject.org/descriptor/" rel="nofollow">DescripTor</a> (our library for parsing and analyzing Tor servers' descriptions of themselves) together with user-friendly tutorials for evaluating Tor network data.  This will make it easier for programmers to write tools that examine historical and current data about the servers that make up the Tor network. </p>

<p> 3. To set up more instances of the network status service <a href="https://onionoo.torproject.org/" rel="nofollow">Onionoo</a> to improve its availability, and work on the most pressing usability issues of the <a href="https://atlas.torproject.org/" rel="nofollow">Atlas</a> network status service;</p>

<p> 4. To further reduce the amount of sensitive usage data (such as bandwidth totals and connections-per-country) stored on Tor relays and reported to the Tor directory authorities. While we believe that this data is safe the way we handle it today, we believe that improved cryptographic and statistical techniques would allow us to store and share even less data.  </p>

<p> 5. To improve the accuracy of performance measurements by developing better methods and tools to analyze and simulate average user behavior;</p>

<p> 6. To make the <a href="https://metrics.torproject.org/" rel="nofollow">Tor Metrics</a> website more usable, so that users, developers, and researchers can more easily find, compare, and interpret information about Tor's usage and performance.</p>

<p>We're excited about this news for a great many reasons.</p>

<p>First, it is one more important step in diversifying Tor's funding.</p>

<p>Second, while the project focuses on improving six important aspects of Tor metrics, it also aims at more general improvements to make Tor metrics software more stable, scalable, maintainable, and usable.  These improvements are typically harder to "sell" in funding proposals because their results are less visible to funders.  It's reassuring that Mozilla understands that these improvements are important, too.</p>

<p>Third, this award is the first one awarded to Tor's young metrics team, only established 12 months ago in June, 2015.  It's an appreciation of the initial work done by the metrics team and a very good basis for the upcoming 12 months.</p>

<p>Writing the award proposal was a successful cooperation of a number of Tor people: it would simply not have happened without Isabela, who made contact with Mozilla people; it would not have been readable without Cass's remarkable ability to translate from tech to English; it would not have contained as many good reasons for getting accepted without iwakeh's invaluable input; and it would not have been accepted without Shari's efforts in asking a leading security expert to write an endorsement of our award request.  Finally, this blog post would certainly not have been as readable without Kate's and Nick's editorial capabilities.  And now let's go write some code.</p>

---
_comments:

<a id="comment-187912"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-187912" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 22, 2016</p>
    </div>
    <a href="#comment-187912">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-187912" class="permalink" rel="bookmark">Mozilla awesomeness!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mozilla awesomeness! $152,500 to Tor - $77,000 to Tails ... free Internet 4 all &lt;3</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-188003"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188003" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 23, 2016</p>
    </div>
    <a href="#comment-188003">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188003" class="permalink" rel="bookmark">Congratulations!!! But to be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Congratulations!!! But to be honest, I wish Mozilla would accept the TBB patches into Firefox.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-188718"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188718" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 27, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-188003" class="permalink" rel="bookmark">Congratulations!!! But to be</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-188718">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188718" class="permalink" rel="bookmark">Yeah, I wonder whether</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yeah, I wonder whether that's actually possible?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-188073"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188073" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 23, 2016</p>
    </div>
    <a href="#comment-188073">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188073" class="permalink" rel="bookmark">well done! every step</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>well done! every step forward is appreciated</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-188572"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188572" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 26, 2016</p>
    </div>
    <a href="#comment-188572">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188572" class="permalink" rel="bookmark">TP deserves praise for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TP deserves praise for trying to provide statistical evidence that the Tor network is helping to promote grassroots democracy around the world.  So plus one for the congragulations.</p>
<p>But it should be said that there is a dark side to "evidence-based" decision-making (such as the decision by a philanthropist to fund Tor Project): USG agencies have been busily adopting on a massive scale individualized "evidence-based" algorithmic decision making targeting every US person.  Critics (including this commentator) argue that the details matter, and that because of the secret unaccountable unreviewable way in which USG is going about this ugly business, these initiatives amount to enshrining new and ancient prejudices (e.g. anti-Muslim, anti-black, anti-"nutcase") in government policies, hidden under a veil of "scientifically validated" [sic] algorithmic decision-making which is allegedly "free from human biases" [sic].  </p>
<p>So while I applaud TP's metrics program, I also urge TP to speak out, along with like-minded civil liberties advocacy groups such as ACLU and EFF, against the dangers posed by the kind of real-time continuously updated government computed "citizenship scores" which are being openly introduced in "Asian authoritarian" nations such as China and also being introduced in secret in "Western democratic" nations including the USA.</p>
<p>There is a further irony: even as agencies like FBI are strongly pushing for ever broader powers to tap ever more sources of personal information on every US citizen, to be fed into their precrime risk scores, even as FBI loudly trumpets the alleged virtues of "evidence-based" risk scoring, FBI is strongly opposing every proposal to apply metrics to FBI's own programs.  A perfect example is the scandalous lack of oversight of FBI's enormously expensive NEXTGEN biometric database, also known as NGI (NEXTGEN Initiative).  As EFF and ACLU FOIA's have revealed in great detail, more than a decade ago, FBI tried to define some evidence-based criteria which its facial identification system should meet, but as it became clear that the system was failing to come anywhere near to meeting these goals, FBI kept watering down the criteria until they became meaningless, then removed them entirely.</p>
<p>This is why FBI has insisted on exempting NGI from the Privacy Act: it was anxious to prevent the public from finding out that NGI is nothing but an enormous boondoggle.  For the same reason, FBI is demanding that no public servant question the effectiveness/safety of other secret programs such as </p>
<p>o widespread abuse of Stingray type "cell site simulators" to target (in particular) everyone attending peaceful public events such as political rallies,</p>
<p>o widespread, expensive, and potentially unsafe FBI spyplane flights over US cities, often at low levels, where they interfere with civilian airliner approach paths, conflict with civilian microdrones and birds, etc,</p>
<p>o widespread abuse of public utility poles to secretly install FBI spycams,</p>
<p>o widespread use of perhaps a hundred thousand paid informants (the Stasi would be proud) to help infiltrate such alleged "national security threats" [sic] as Bread Not Bombs (a group which plans to feed citizens who cannot get to groceries due to disruptions caused by police blockades occasioned by the forthcoming RNC),</p>
<p>o abusive and unconstitutional sting operations targeting developmentally disabled youths,</p>
<p>o programs which pressure state and local police agencies to adopt increasingly intrusive surveillance technologies, including inside-the-home surveillance methods just coming on the market which use a new generation of miniaturized covert audio bugs, electronic surveillance devices, thermal and radar imagers, to watch what "subjects" are doing inside their own homes,</p>
<p>o programs which expand the militarization of American policing, by providing free of charge armored vehicles, automatic weapons, poison gas grenades, surveillance drones, guidebooks on military-intelligence style "operations" targeting peaceful civilian advocacy groups,</p>
<p>o programs which coach local police agencies on how to lie to judges, prosecutors, defense attorneys, and juries, for example by concealing the fact that critical evidence came from an illegal burglary, hidden spycam, audio bug, or electronic surveillance,</p>
<p>o secretive information-sharing programs which strengthen dangerous ties between FBI, local police agencies, and military "force protection" paramilitary intelligence/special-forces units, as well as military agencies such as NCIS and AFOSI which have been enthusiastically targeting non-military Tor users (yes, ironic, given that other arms of the vast US military octopus have played a role in the genesis and continued not-very-well-laundered financial support for Tor),</p>
<p>o FBI organized Shared Responsibility Committees which will bring together FBI agents, police officials, psychologists, social workers, and educators, to continuously review and determine sanctions for "troubled" schoolchildren, beginning with American high schoolers but soon to include FBI's ultimate target group, preschoolers aged 2-7 with such measures as forcible administration of psychological testing while the child is undergoing fMRI brain scans (because FBI thinks this will enable them to determine decades in advance who will be the anti-RNC demonstrators of the future).</p>
<p>Yes, FBI, Big Brother would be proud of your incredibly dangerous and unbelievably useless organization.  No wonder you are terrified by the thought that some of Milton Friedman's loyal devotees might suggest that FBI itself should come in for a spot of rational "does it work?" examination by those public servants who have sworn to oversee federal agencies, in the public interest and in the name of sensible government.</p>
<p>Once again, we see the USG double standard operating in full-on deception mode: "more and more metrics for us [the government targeting the citizens], no metrics for you [citizens who wish to keep an eye on the government]."</p>
<p>That has to change.</p>
<p>FBI and USIC will oppose review, because they know very well that if they were forced to provide statistical evidence bearing on whether their agencies make sense, the answer would be crystal clear: FBI, NSA, CIA, and all the rest of the super-secret alphabet soup are far more trouble that they are worth, and are doing nothing whatever, once you examine some hard ground truth, to "keep America safe".</p>
<p>For more about FBI's NGI, see the links here: </p>
<p><a href="https://www.eff.org/foia/fbis-next-generation-identification-biometrics-database" rel="nofollow">https://www.eff.org/foia/fbis-next-generation-identification-biometrics…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-188928"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188928" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 28, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-188572" class="permalink" rel="bookmark">TP deserves praise for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-188928">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188928" class="permalink" rel="bookmark">Forgot to say:  a critical</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Forgot to say:  a critical difference between USG metrics and Tor Project metrics is that when USG says it is "anonymizing" information that is invariably a sham, but TP is really serious about protecting its users.  I wish everyone took that attitude!</p>
<p>Still, protecting users from having their anonymity compromised during collection of "metrics" data is worthless if you (looking at Roger, not Karsten) hire a CIA officer as a TP Project Manager and give him access to critical internal TP discussions even before he quits CIA to join TP.  See what I'm saying?</p>
<p>Technical protections are not enough.  You also need legal protections for your staff and for TP itself, jurisdictional variety, and you need to vet future job candidates very carefully.  And I think you need to gently encourage all TP employees to be aware that considering how intensely all the worlds spooks are targeting Tor, what they do when they are not on the clock at TP could hurt our community, if it helps the bad guys to apply further disinformation and disruption operations.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-188582"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188582" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 26, 2016</p>
    </div>
    <a href="#comment-188582">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188582" class="permalink" rel="bookmark">This is great news,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is great news, congratulations all! And it surely would not have been possible without Karsten's steady lead in the Tor Metrics project. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-188925"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188925" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 28, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-188582" class="permalink" rel="bookmark">This is great news,</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-188925">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188925" class="permalink" rel="bookmark">Plus one &lt;8 &lt;8 &lt;8</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Plus one &lt;8 &lt;8 &lt;8</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-188924"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188924" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 28, 2016</p>
    </div>
    <a href="#comment-188924">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188924" class="permalink" rel="bookmark">&gt; First, it is one more</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; First, it is one more important step in diversifying Tor's funding.</p>
<p>Yes, and Mozilla is about as good a funding source as you could find, other than individual user contributions.</p>
<p>So thanks to everyone who contributed to TP getting this prize money!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-190087"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-190087" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 05, 2016</p>
    </div>
    <a href="#comment-190087">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-190087" class="permalink" rel="bookmark">https://metrics.torproject.or</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://metrics.torproject.org/userstats-censorship-events.html" rel="nofollow">https://metrics.torproject.org/userstats-censorship-events.html</a><br />
— Error<br />
Oops! Something went wrong here! We encountered a 500 Internal Server Error when processing your request!<br />
----<br />
WTF?!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-190847"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-190847" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 08, 2016</p>
    </div>
    <a href="#comment-190847">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-190847" class="permalink" rel="bookmark">Karsten. Any idea on the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Karsten. Any idea on the order/priority of completion? It looks like 1,3,6 are the easiest to target.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-191489"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-191489" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 11, 2016</p>
    </div>
    <a href="#comment-191489">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-191489" class="permalink" rel="bookmark">i like tor and i wish to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i like tor and i wish to every one good  chance</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-195811"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195811" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 30, 2016</p>
    </div>
    <a href="#comment-195811">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195811" class="permalink" rel="bookmark">First-time Tor browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>First-time Tor browser attempted user, and this is what I get after 5 minutes, and no way out of this dead end:</p>
<p>403. That’s an error.</p>
<p>Your client does not have permission to get URL /sorry/IndexRedirect?continue=<a href="https://www.google.cz/%3Fgfe_rd%3Dcr%26ei%3DCDWdV_6qNM3R8gfZz4HgBw&amp;q=CGMSBFnqnf4Yier0vAUiGQDxp4NLQKz3Lv4sMUFO7ds-KT4VLd1V_Ts" rel="nofollow">https://www.google.cz/%3Fgfe_rd%3Dcr%26ei%3DCDWdV_6qNM3R8gfZz4HgBw&amp;q=CG…</a> from this server. That’s all we know.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-211932"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-211932" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-195811" class="permalink" rel="bookmark">First-time Tor browser</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-211932">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-211932" class="permalink" rel="bookmark">same here</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>same here</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
