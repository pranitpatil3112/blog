title: Announcing new board members
---
pub_date: 2023-01-24
---
author: Kendra Albert
---
summary: We are excited to announce the result of our open call for board members - three new members are joining the Tor Project’s Board of Directors: Esra'a Al Shafei, Sarah Gran and Christian Kaufman!
---
categories:

announcements
---
body:

We are excited to announce the result of our open call for board members - three new members are joining the Tor Project’s Board of Directors: **Esra'a Al Shafei**, **Sarah Gran** and **Christian Kaufman**!  Each new member comes to Tor with a different set of expertise that will help the organization and our community. At the end of this post, you can read each of their bios.

Please join us in welcoming Esra’a, Sarah, and Christian to the board!

Esra'a Al Shafei is a Bahraini human rights activist and founder and director of Majal.org, a network of digital platforms that amplify under-reported and marginalized voices in the Middle East and North Africa.

"Tor’s privacy technologies have been critical resources for my human rights advocacy work. It is an honor to have this opportunity to support an organization and community that made my work and the work of many other activists possible."

Sarah is on the Board of Directors for the Tor Project. She is also VP of the Brand & Donor Development team at ISRG, the nonprofit behind Let's Encrypt, Prossimo, and Divvi Up.

"I'm a longtime fan of the Tor Project and its efforts to provide people with privacy and anonymity while using the Internet and I'm honored to join its Board of Directors."

Christian Kaufmann has over 20 years of experience in the internet environment as an architect, manager and board member in various roles.

"We find ourselves in a surveillance state, where many private companies collect as much data as they can get. I see the Tor networks as the privacy and anonymity layer of the Internet that so many of us rely on, and I am proud to be an active part of it."

And as a reminder, the other six members of the Tor Project’s Board are: Kendra Albert, Desigan Chinniah, Gabriella Coleman, Alissa Cooper, Nighat Dad and Julius Mittenzwei.

## Full Biographies of Incoming Board Members

**Esra'a Al Shafei**:  Esra'a Al Shafei is a Bahraini human rights activist and founder of Majal.org, a network of digital platforms that amplify under-reported and marginalized voices in the Middle East and North Africa. This work includes Mideast Tunes, a web and mobile application for independent musicians in the MENA who use music as a tool for social justice advocacy, Ahwaa.org, a discussion tool for Arab LGBTQ+ youth which leverages game mechanics to protect and engage its community, and Migrant-Rights.org, the primary resource on the plight of migrant workers in the Gulf region. Esra’a was a Senior TED Fellow, Echoing Green Fellow, MIT Media Lab Fellow, and Shuttleworth Foundation Fellow. She is a co-founder of the Numun Fund, an initiative that aims to seed, resource, and sustain a feminist technology ecosystem. She currently serves as Vice Chair of the Board of Trustees of the Wikimedia Foundation, the nonprofit organization that hosts Wikipedia. Previously, she served on the Board of Directors of Access Now, an international non-profit dedicated to an open and free Internet.

**Sarah Gran**: Sarah Gran is the VP of the Brand and Donor Development team at Internet Security Research Group ([ISRG](https://www.abetterinternet.org/)), the nonprofit entity behind [Let's Encrypt](https://letsencrypt.org/). Sarah joined ISRG in early 2016, shortly after the Let’s Encrypt launch and has helped it become a household name in software development. Sarah has helped to shape ISRG’s latest projects, one focused on bringing memory-safe code to security-sensitive software, called [Prossimo](https://memorysafety.org/), and [Divvi Up](https://memorysafety.org/), a privacy-respecting metrics service. Previously, Sarah worked as a Vice President at Edelman and Deutsch in brand and communications strategy groups.

**Christian Kaufmann**: As Vice President Technology Christian Kaufmann leads the Technology department which is responsible for driving the technical evolution of the Akamai Edge platform, including a wide set of responsibilities like hardware engineering, datacenter architecture, network architecture and engineering, and software and systems development. Christian has gained extensive board experience including hiring CEOs and handling M&A, during his 15+ years on various boards in the Internet ecosystem. Currently he serves as a board member at ICANN, the RIPE NCC and the Tor Project. Before joining Akamai, Christian worked at several ISPs and Carriers like Telia Sonera, Easynet and Cable & Wireless, in various technical and managerial roles. Christian has a Master of Science in Advanced Networking from The Open University in the UK. He also holds various technical certifications, including both a CCIE & JCNIE.
