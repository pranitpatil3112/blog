title: You’re Invited: State of the Onion 2021
---
pub_date: 2021-11-09
---
author: isabela
---
tags: events
---
categories: community
---
summary: We will be hosting another State of the Onion livestream, a compilation of updates from the Tor Project's different teams discussing  highlights of their work during the year and what we are excited about in the upcoming year, on November 17 from 17:00 - 19:00 UTC.
---
_html_body:

<p>Last year, we held our first 100% <a href="https://www.youtube.com/watch?v=ZnjaGmJppQ0&amp;list=PLwyU2dZ3LJEqMUYbYaVQUqx-my7b93xhW">virtual State of the Onion</a>, a compilation of updates from the Tor Project's different teams discussing highlights of their work during the year and what we are excited about in the upcoming year. Our 2020 State of the Onion was our first time doing livestream iteration, and it was not only a great success because it allowed us to reach thousands of people all around the world, but also because it allowed for more projects from our community to participate, giving an opportunity for them to also share their updates.</p>
<p>We are happy to announce that we will be hosting our <a href="https://www.youtube.com/watch?v=mNhIjtXuVzk">2021 State of the Onion livestream</a> on <strong>November 17 from 17:00 - 19:00 UTC</strong>.</p>
<p>This year we have another exciting program for you. Following last year's program structure, we will start with updates from the Tor Project's teams followed by updates from people in our community. Isabela Bagueros, our executive director, will be our host for the event.</p>
<h3>Program</h3>
<p>Note: More details about each session will be posted on the <a href="https://forum.torproject.net/t/you-re-invited-state-of-the-onion-2021/542">Tor Forum discussion post related to the 2021 State of the Onion</a>.</p>
<h4>Opening — Isabela Bagueros, Executive Director</h4>
<h4>Part I — The Tor Project</h4>
<ol>
<li>Censorship Circumvention, Cecylia</li>
<li>Applications &amp; Tor Browser, Richard</li>
<li>UX Team, Duncan</li>
<li>Applications &amp; VPN, Matt Finkel</li>
<li>Community Team, Gus</li>
<li>Network Health, GeKo</li>
<li>Metrics, Hiro</li>
<li>Shadow, Jim Newsome</li>
<li>Tor network, little "t" tor, Mike/ahf</li>
<li>Arti, Nick Mathewson</li>
<li>TPA, Anarcat</li>
<li>Year End Fundraising, Al</li>
</ol>
<h4>Intermission — Roger Dingledine, Co-Founder</h4>
<h4>Part 2 — Tor Community</h4>
<p>We have also invited other projects who are part of the Tor community to present and share their updates.</p>
<ol>
<li><a href="https://securedrop.org/">SecureDrop</a>, Conor Schaefer - Chief Technology Officer at Freedom of the Press Foundation</li>
<li><a href="https://libraryfreedom.org/">Library Freedom Project</a>, Alison Macrina - Library Freedom Project</li>
<li><a href="https://www.ricochetrefresh.net/">Ricochet Refresh</a>, Richard</li>
<li><a href="https://ooni.org">Open Observatory of Network Interference</a>, Arturo Filastò - Project Lead</li>
<li><a href="https://guardianproject.info/">The Guardian Project</a>, Nathan Freitas - Founder of the Guardian Project</li>
<li><a href="https://tails.boum.org/">Tails</a>, Sajolida</li>
<li><a href="https://calyxinstitute.org/">Calyx Institute</a>, Nick Merrill - Founder, Executive Director</li>
</ol>
<p>The State of the Onion will be streamed over our <a href="https://www.youtube.com/channel/UCglZ5lXxOpxFF281h6WBj3g">YouTube</a>, <a href="https://www.facebook.com/TorProject">Facebook</a>, and <a href="https://twitter.com/torproject">Twitter</a> accounts.</p>
<p>You can join the conversation on Twitter using the hashtag: #StateOfTheOnion2021, or post your questions and comments in the Youtube chat.</p>

