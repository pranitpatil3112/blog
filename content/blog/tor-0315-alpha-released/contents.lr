title: Tor 0.3.1.5-alpha is released
---
pub_date: 2017-08-01
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>Hello again! This post announces the fifth alpha in the 0.3.1.x series, which we just released today. I'll try to get a new 0.3.0.x release out tomorrow.</p>
<p>Tor 0.3.1.5-alpha improves the performance of consensus diff calculation, fixes a crash bug on older versions of OpenBSD, and fixes several other bugs. If no serious bugs are found in this version, the next version will be a release candidate.</p>
<p>Since this is an alpha release, you can expect more bugs than usual. If you'd rather have a more stable experience, stick to the stable releases.</p>
<p>If you build Tor from source, you can find Tor 0.3.1.5-alpha at the usual place (at the Download page on our website). Otherwise, you'll probably want to wait until packages are available. There should be a new Tor Browser release early next week.</p>
<p>This release also marks the end of support for the Tor 0.2.4.x, 0.2.6.x, and 0.2.7.x release series. Those releases will receive no further bug or security fixes. Anyone still running or distributing one of those versions should upgrade.</p>
<h2>Changes in version 0.3.1.5-alpha - 2017-08-01</h2>
<ul>
<li>Major features (build system, continuous integration):
<ul>
<li>Tor's repository now includes a Travis Continuous Integration (CI) configuration file (.travis.yml). This is meant to help new developers and contributors who fork Tor to a Github repository be better able to test their changes, and understand what we expect to pass. To use this new build feature, you must fork Tor to your Github account, then go into the "Integrations" menu in the repository settings for your fork and enable Travis, then push your changes. Closes ticket <a href="https://bugs.torproject.org/22636">22636</a>.</li>
</ul>
</li>
<li>Major bugfixes (openbsd, denial-of-service):
<ul>
<li>Avoid an assertion failure bug affecting our implementation of inet_pton(AF_INET6) on certain OpenBSD systems whose strtol() handling of "0xfoo" differs from what we had expected. Fixes bug <a href="https://bugs.torproject.org/22789">22789</a>; bugfix on 0.2.3.8-alpha. Also tracked as TROVE-2017-007.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (relay, performance):
<ul>
<li>Perform circuit handshake operations at a higher priority than we use for consensus diff creation and compression. This should prevent circuits from starving when a relay or bridge receives a new consensus, especially on lower-powered machines. Fixes bug <a href="https://bugs.torproject.org/22883">22883</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor features (bridge authority):
<ul>
<li>Add "fingerprint" lines to the networkstatus-bridges file produced by bridge authorities. Closes ticket <a href="https://bugs.torproject.org/22207">22207</a>.</li>
</ul>
</li>
<li>Minor features (directory cache, consensus diff):
<ul>
<li>Add a new MaxConsensusAgeForDiffs option to allow directory cache operators with low-resource environments to adjust the number of consensuses they'll store and generate diffs from. Most cache operators should leave it unchanged. Helps to work around bug <a href="https://bugs.torproject.org/22883">22883</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the July 4 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor features (relay, performance):
<ul>
<li>Always start relays with at least two worker threads, to prevent priority inversion on slow tasks. Part of the fix for bug <a href="https://bugs.torproject.org/22883">22883</a>.</li>
<li>Allow background work to be queued with different priorities, so that a big pile of slow low-priority jobs will not starve out higher priority jobs. This lays the groundwork for a fix for bug <a href="https://bugs.torproject.org/22883">22883</a>.</li>
</ul>
</li>
<li>Minor bugfixes (build system, rust):
<ul>
<li>Fix a problem where Rust toolchains were not being found when building without --enable-cargo-online-mode, due to setting the $HOME environment variable instead of $CARGO_HOME. Fixes bug <a href="https://bugs.torproject.org/22830">22830</a>; bugfix on 0.3.1.1-alpha. Fix by Chelsea Komlo.</li>
</ul>
</li>
<li>Minor bugfixes (compatibility, zstd):
<ul>
<li>Write zstd epilogues correctly when the epilogue requires reallocation of the output buffer, even with zstd 1.3.0. (Previously, we worked on 1.2.0 and failed with 1.3.0). Fixes bug <a href="https://bugs.torproject.org/22927">22927</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation warnings):
<ul>
<li>Suppress -Wdouble-promotion warnings with clang 4.0. Fixes bug <a href="https://bugs.torproject.org/22915">22915</a>; bugfix on 0.2.8.1-alpha.</li>
<li>Fix warnings when building with libscrypt and openssl scrypt support on Clang. Fixes bug <a href="https://bugs.torproject.org/22916">22916</a>; bugfix on 0.2.7.2-alpha.</li>
<li>Compile correctly when both openssl 1.1.0 and libscrypt are detected. Previously this would cause an error. Fixes bug <a href="https://bugs.torproject.org/22892">22892</a>; bugfix on 0.3.1.1-alpha.</li>
<li>When building with certain versions of the mingw C header files, avoid float-conversion warnings when calling the C functions isfinite(), isnan(), and signbit(). Fixes bug <a href="https://bugs.torproject.org/22801">22801</a>; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (coverity build support):
<ul>
<li>Avoid Coverity build warnings related to our BUG() macro. By default, Coverity treats BUG() as the Linux kernel does: an instant abort(). We need to override that so our BUG() macro doesn't prevent Coverity from analyzing functions that use it. Fixes bug <a href="https://bugs.torproject.org/23030">23030</a>; bugfix on 0.2.9.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory authority):
<ul>
<li>When a directory authority rejects a descriptor or extrainfo with a given digest, mark that digest as undownloadable, so that we do not attempt to download it again over and over. We previously tried to avoid downloading such descriptors by other means, but we didn't notice if we accidentally downloaded one anyway. This behavior became problematic in 0.2.7.2-alpha, when authorities began pinning Ed25519 keys. Fixes bug <a href="https://bugs.torproject.org/22349">22349</a>; bugfix on 0.2.1.19-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (error reporting, windows):
<ul>
<li>When formatting Windows error messages, use the English format to avoid codepage issues. Fixes bug <a href="https://bugs.torproject.org/22520">22520</a>; bugfix on 0.1.2.8-alpha. Patch from "Vort".</li>
</ul>
</li>
<li>Minor bugfixes (file limits, osx):
<ul>
<li>When setting the maximum number of connections allowed by the OS, always allow some extra file descriptors for other files. Fixes bug <a href="https://bugs.torproject.org/22797">22797</a>; bugfix on 0.2.0.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp2 sandbox):
<ul>
<li>Avoid a sandbox failure when trying to re-bind to a socket and mark it as IPv6-only. Fixes bug <a href="https://bugs.torproject.org/20247">20247</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (memory leaks):
<ul>
<li>Fix a small memory leak when validating a configuration that uses two or more AF_UNIX sockets for the same port type. Fixes bug <a href="https://bugs.torproject.org/23053">23053</a>; bugfix on 0.2.6.3-alpha. This is CID 1415725.</li>
</ul>
</li>
<li>Minor bugfixes (unit tests):
<ul>
<li>test_consdiff_base64cmp would fail on OS X because while OS X follows the standard of (less than zero/zero/greater than zero), it doesn't follow the convention of (-1/0/+1). Make the test comply with the standard. Fixes bug <a href="https://bugs.torproject.org/22870">22870</a>; bugfix on 0.3.1.1-alpha.</li>
<li>Fix a memory leak in the link-handshake/certs_ok_ed25519 test. Fixes bug <a href="https://bugs.torproject.org/22803">22803</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
</ul>

