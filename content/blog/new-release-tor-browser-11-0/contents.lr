title: New Release: Tor Browser 11.0
---
pub_date: 2021-11-08
---
author: duncan
---
tags:

tor browser
tbb-11.0
tbb
---
categories: applications
---
summary: Tor Browser 11.0 is now available from the Tor Browser download page and our distribution directory. This is the first stable release based on Firefox ESR 91, and includes an important update to Tor 0.4.6.8.
---
_html_body:

<p>Tor Browser 11.0 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and our <a href="https://dist.torproject.org/torbrowser/11.0/">distribution directory</a>. This is the first stable release based on Firefox ESR 91, and includes an important update to Tor 0.4.6.8.</p>
<p><strong>Update</strong>: The version of Tor Browser for Windows available on the <a href="https://www.torproject.org/download/" tabindex="-1">Tor Browser download page</a> has been temporarily reverted to 10.5.10 while we investigate an issue with NoScript. Please see <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40695" tabindex="-1">Bug 40695 </a> for updates.</p>
<h2>What’s new?</h2>
<p><img alt="Tor Browser 11's connection screen in light and dark themes" src="/static/images/blog/inline-images/tb-11-browser.jpg" /></p>
<h3>Tor Browser gets a new look</h3>
<p>Earlier this year, Firefox’s user interface <a href="https://www.mozilla.org/en-US/firefox/89.0/releasenotes/">underwent a significant redesign</a> aimed at simplifying the browser chrome, streamlining menus and featuring an all-new tab design. Firefox ESR 91 introduces the new design to Tor Browser for the first time.</p>
<p><img alt="Tor Browser 11's redesigned icons" src="/static/images/blog/inline-images/tb-11-icons.jpg" /></p>
<p>To ensure it lives up to the new experience, each piece of custom UI in Tor Browser has been modernized to match Firefox’s new look and feel. That includes everything from updating the fundamentals like color, typography and buttons to redrawing each of our icons to match the new thinner icon style.</p>
<p>In addition to the browser chrome itself, the connection screen, circuit display, security levels and onion site errors all received a sprucing-up too – featuring some small but welcome quality of life improvements to each.</p>
<p><img alt="Invalid Onion Site Address error resulting from v2 deprecation" src="/static/images/blog/inline-images/tb-11-deprecation.jpg" /></p>
<h3>Final deprecation of v2 onion services</h3>
<p>Last year we announced that <a href="https://blog.torproject.org/v2-deprecation-timeline">v2 onion services would be deprecated in late 2021</a>, and since its <a href="https://blog.torproject.org/new-release-tor-browser-105">10.5 release</a> Tor Browser has been busy warning users who visit v2 onion sites of their upcoming retirement. At long last, that day has finally come. Since updating to Tor 0.4.6.8 v2 onion services are no longer reachable in Tor Browser, and users will receive an “Invalid Onion Site Address” error instead.</p>
<p>Should you receive this error when attempting to visit a previously working v2 address, there is nothing wrong with your browser – instead, the issue lies with the site itself. If you wish, you can notify the onion site’s administrator about the problem and encourage them to upgrade to a v3 onion service as soon as possible.</p>
<p>It’s easy to tell if you still have any old v2 addresses saved in your bookmarks that are in need of removal or updating too: although both end in .onion, the more secure v3 addresses are 56 characters long compared to v2’s modest 16 character length.</p>
<h2>Known issues</h2>
<p>Tor Browser 11.0 comes with a number of known issues:</p>
<ul>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40668">Bug 40668</a>: DocumentFreezer &amp; file scheme</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40671">Bug 40671</a>: Fonts don't render</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40679">Bug 40679</a>: Missing features on first-time launch in esr91 on MacOS</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40689">Bug 40689</a>: Change Blockchair Search provider's HTTP method</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40667">Bug 40667</a>: AV1 videos shows as corrupt files in Windows 8.1</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40677">Bug 40677</a>: Since the update to 11.0a9 some addons are inactive and need disabling-reenabling on each start</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40666">Bug 40666</a>: Switching svg.disable affects NoScript settings</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40690">Bug 40690</a>: Browser chrome breaks when private browsing mode is turned off</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40693">Bug 40693</a>: Potential Wayland dependency (new)</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40692">Bug 40692</a>: Picture-in-Picture is enabled on tbb 11.0a10 (new)</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40700">Bug 40700</a>: Switch Firefox recommendations off by default (new)</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40705">Bug 40705</a>: "visit our website" link on about:tbupdate pointing to different locations (new)</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40706">Bug 40706</a>: Fix issue in https-e wasm (new)</li>
</ul>
<h2>Join the discussion</h2>
<p>Last week <a href="https://blog.torproject.org/tor-forum-a-new-discussion-platform">we announced a new discussion and user support platform</a>: the Tor Forum. Starting today, we’re going to be doing things a little bit differently – rather than commenting on the blog posts themselves, we’d like to invite you to create an account and join in the discussion on the forum instead:</p>
<p>☞ <a href="https://forum.torproject.net/t/new-release-tor-browser-11-0/504">Tor Browser 11.0 discussion thread</a></p>
<h3>Give feedback</h3>
<p>If you find a bug or have a suggestion for how we could improve this release, <a href="https://support.torproject.org/misc/bug-or-feedback/">please let us know</a>. Thanks to all of the teams across Tor, and the many volunteers, who contributed to this release.</p>
<h2>Full changelog</h2>
<p>The full changelog since Tor Browser 10.5.10 is:</p>
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 91.3.0esr</li>
<li>Update Tor to tor-0.4.6.8</li>
<li><a href="https://bugs.torproject.org/32624">Bug 32624</a>: localStorage is not shared between tabs</li>
<li><a href="https://bugs.torproject.org/33125">Bug 33125</a>: Remove xpinstall.whitelist.add* as they don't do anything anymore</li>
<li><a href="https://bugs.torproject.org/34188">Bug 34188</a>: Cleanup extensions.* prefs</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-launcher/40004">Bug 40004</a>: Convert tl-protocol to async.</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-launcher/40012">Bug 40012</a>: Watch all requested tor events</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40027">Bug 40027</a>: Make torbutton_send_ctrl_cmd async</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40042">Bug 40042</a>: Add missing parameter of createTransport</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40043">Bug 40043</a>: Delete all plugin-related protections</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40045">Bug 40045</a>: Teach the controller about status_client</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40046">Bug 40046</a>: Support arbitrary watch events</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40047">Bug 40047</a>: New string for Security Level panel</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40048">Bug 40048</a>: Protonify Circuit Display Panel</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40053">Bug 40053</a>: investigate fingerprinting potential of extended TextMetrics interface</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40083">Bug 40083</a>: Make sure Region.jsm fetching is disabled</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40177">Bug 40177</a>: Clean up obsolete preferences in our 000-tor-browser.js</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40220">Bug 40220</a>: Make sure tracker cookie purging is disabled</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40342">Bug 40342</a>: Set `gfx.bundled-fonts.activate = 1` to preserve current bundled fonts behaviour</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40463">Bug 40463</a>: Disable network.http.windows10-sso.enabled in FF 91</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40483">Bug 40483</a>: Deutsche Welle v2 redirect</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40534">Bug 40534</a>: Cannot open URLs on command line with Tor Browser 10.5</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40547">Bug 40547</a>: UX: starting in offline mode can result in difficulty to connect later</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40548">Bug 40548</a>: Set network.proxy.failover_direct to false in FF 91</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40561">Bug 40561</a>: Refactor about:torconnect implementation</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40567">Bug 40567</a>: RFPHelper is not init until after about:torconnect bootstraps</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40597">Bug 40597</a>: Implement TorSettings module</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40600">Bug 40600</a>: Multiple pages as home page unreliable in 11.0a4</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40616">Bug 40616</a>: UX: multiple about:torconnect</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40624">Bug 40624</a>: TorConnect banner always visible in about:preferences#tor even after bootstrap</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40626">Bug 40626</a>: Update Security Level styling to match Proton UI</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40628">Bug 40628</a>: Checkbox wrong color in about:torconnect in dark mode theme</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40630">Bug 40630</a>: Update New Identity and New Circuit icons</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40631">Bug 40631</a>: site identity icons are not being displayed properly</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40632">Bug 40632</a>: Proton'ify Circuit Display Panel</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40634">Bug 40634</a>: Style updates for Onion Error Pages</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40636">Bug 40636</a>: Fix about:torconnect 'Connect' border radius in about:preferences#tor</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40641">Bug 40641</a>: Update Security Level selection in about:preferences to match style as tracking protection option bubbles</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40648">Bug 40648</a>: Replace onion pattern divs/css with tiling SVG</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40653">Bug 40653</a>: Onion Available text not aligned correctly in toolbar in ESR91</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40655">Bug 40655</a>: esr91 is suggesting to make Tor Browser the default browse</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40657">Bug 40657</a>: esr91 is missing "New identity" in hamburger menu</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40680">Bug 40680</a>: Prepare update to localized assets for YEC</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40686">Bug 40686</a>: Update Onboarding link for 11.0</li>
</ul>
</li>
<li>Build System
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Go to 1.16.9</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40048">Bug 40048</a>: Remove projects/clang-source</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40347">Bug 40347</a>: Make the list of toolchain updates needed for firefox91</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40363">Bug 40363</a>: Change bsaes git url</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40366">Bug 40366</a>: Use bullseye to build https-everywhere</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40368">Bug 40368</a>: Use system's python3 for https-everywhere</li>
</ul>
</li>
<li>Windows + Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40357">Bug 40357</a>: Update binutils to 2.35.2</li>
</ul>
</li>
<li>Windows
<ul>
<li><a href="https://bugs.torproject.org/28240">Bug 28240</a>: switch from SJLJ exception handling to Dwarf2 in mingw for win32</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40306">Bug 40306</a>: Update Windows toolchain to switch to mozilla91</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40376">Bug 40376</a>: Use python3 for running pe_checksum_fix.py</li>
</ul>
</li>
<li>OS X
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40307">Bug 40307</a>: Update macOS toolchain to switch to mozilla91</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40222">Bug 40222</a>: Bump GCC to 10.3.0 for Linux</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40305">Bug 40305</a>: Update Linux toolchain to switch to mozilla91</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40353">Bug 40353</a>: Temporarily disable rlbox for linux builds</li>
</ul>
</li>
</ul>
</li>
</ul>

