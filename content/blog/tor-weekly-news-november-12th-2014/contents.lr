title: Tor Weekly News — November 12th, 2014
---
pub_date: 2014-11-12
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the forty-fifth issue in 2014 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Mozilla announces Polaris Privacy Initiative</h1>

<p>Mozilla, makers of the Firefox browser upon which Tor Browser is based, <a href="https://blog.mozilla.org/privacy/2014/11/10/introducing-polaris-privacy-initiative-to-accelerate-user-focused-privacy-online" rel="nofollow">announced</a> a series of projects to “accelerate pragmatic and user-focused advances in privacy technology for the Web, giving users more control, awareness and protection in their Web experiences”. The Tor Project is one of Mozilla’s two partners in this Polaris Privacy Initiative, and the collaboration will involve looking at the Firefox codebase to see if its relationship to Tor Browser and the Tor development process can be made more efficient, giving Tor engineers more time to focus on other important issues. Mozilla also stated their intention to run several high-capacity Tor middle relays, contributing to a faster and more stable Tor network.</p>

<p>As Andrew Lewman <a href="https://blog.torproject.org/blog/partnering-mozilla" rel="nofollow">wrote</a> on the Tor blog, “the Tor Browser is one of the best ways to protect privacy on the web and this partnership is a huge step in advancing people’s right to freedom of expression online”. Watch for more announcements as work on these two fronts continues.</p>

<h1>Tor and Operation Onymous</h1>

<p>An international coalition of law enforcement authorities <a href="https://www.europol.europa.eu/content/global-action-against-dark-markets-tor-network" rel="nofollow">announced</a> the seizure of over 400 Tor hidden services allegedly engaging in illegal activity. Once the desired headlines had been written, something approaching the facts began to emerge, with the claimed number of seized services <a href="http://www.dailydot.com/politics/oops-we-counted-wrong-silk-road-dark-net-tor/" rel="nofollow">dropping sharply to 27</a>; more troublingly, several high-capacity Tor relays with no apparent connection to the hidden services were <a href="https://blog.torservers.net/20141109/three-servers-offline-likely-seized.html" rel="nofollow">also</a> <a href="https://raided4tor.wordpress.com/" rel="nofollow">seized</a>.  However, in contrast to the <a href="https://blog.torproject.org/blog/tor-security-advisory-relay-early-traffic-confirmation-attack" rel="nofollow">last major takedown of hidden services</a>, which involved one shared hidden service hosting platform, there was no obvious single feature linking all of the seized sites, leading to concern in the Tor community that an exploit against the Tor network may have been responsible for their discovery.</p>

<p>It could be that these services were deanonymized individually over a period of months using a variety of means, then all seized at once for maximum effect: as Andrew Lewman and others wrote in a <a href="https://blog.torproject.org/blog/thoughts-and-concerns-about-operation-onymous" rel="nofollow">response</a> posted to the Tor blog, these methods could include operational security mistakes by service operators, exploitation of flaws in poorly-written website code, or attacks on the Bitcoin cryptocurrency that is widely used on hidden service marketplaces. On the other hand, if an attack on the Tor network itself is at play, it may be a variant of the class of attack known as “traffic confirmation”, like the one <a href="https://lists.torproject.org/pipermail/tor-news/2014-August/000057.html" rel="nofollow">observed earlier this year</a>. “Unfortunately,” as the blog post notes, ”the authorities did not specify how they managed to locate the hidden services”; even if they had, recent disclosures concerning <a href="https://en.wikipedia.org/wiki/Parallel_construction" rel="nofollow">“parallel construction” in law enforcement</a> mean that the public would not necessarily be able to trust their explanation.</p>

<p>“Hidden services need some love” has become a familiar refrain in recent months, and even though the story behind these seizures may remain unknown, they have reinvigorated some long-running threads on improvements to the security of this important technology. George Kadianakis <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007730.html" rel="nofollow">coded</a> a patch that allows hidden service operators to “specify a set of nodes that will be pinned as middle nodes in hidden service rendezvous circuits”, while the theory behind this <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007726.html" rel="nofollow">continues to be discussed</a>, as does the <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007735.html" rel="nofollow">hidden service authorization feature</a> and <a href="https://lists.torproject.org/pipermail/tor-dev/2014-November/007744.html" rel="nofollow">how widely it is used in practice</a>.</p>

<p>“The attention hidden services have received is minimal compared to their social value and compared to the size and determination of their adversaries.” If you are a hidden service operator concerned by these seizures, or you want to help ensure the possibility of free and uncensorable publishing online, see the group blog post for more details, and feel free to join in with the discussions on the tor-dev mailing list.</p>

<h1>More monthly status reports for October 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the  month of October continued, with reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000693.html" rel="nofollow">Isis Lovecruft</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000694.html" rel="nofollow">Nicolas Vigier</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000695.html" rel="nofollow">Damian Johnson</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000696.html" rel="nofollow">Karsten Loesing</a>.</p>

<p>Roger Dingledine sent out the report for <a href="https://lists.torproject.org/pipermail/tor-reports/2014-November/000697.html" rel="nofollow">SponsorF</a>.</p>

<h1>Miscellaneous news</h1>

<p>Arturo Filastò <a href="https://blog.torproject.org/blog/ooni-bridge-reachability-study-and-hackfest" rel="nofollow">reported</a> on OONI’s ongoing study of Tor bridge reachability in different countries, and the recent hackfest on the same topic.</p>

<p>Karsten Loesing offered an <a href="https://lists.torproject.org/pipermail/onionoo-announce/2014/000002.html" rel="nofollow">update</a> on developments in the world of Onionoo, including new mirrors and search improvements.</p>

<h1>Help desk round up</h1>

<p>The help desk has been asked how to run Tor Browser on a Chromebook. ChromeOS does not allow any programs to be executed except Google Chrome, including other browsers like Tor Browser. The workaround for this is to install a Debian or Ubuntu environment within ChromeOS using <a href="https://github.com/dnschneid/crouton" rel="nofollow">crouton</a>. Once crouton is ready, Tor Browser for Linux can be downloaded and installed in the Debian or Ubuntu environment. Crouton users should seek support from the crouton team and not from the Tor help desk.</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, Matt Pagan, Karsten Loesing, and Lunar.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

