title: Tor 0.2.2.6-alpha released
---
pub_date: 2009-12-03
---
author: phobos
---
tags:

openssl
alpha release
enhancements
new features
android
---
categories:

applications
releases
---
_html_body:

<p>On November 19, we released the latest in the Tor alpha series, version 0.2.2.6-alpha. This release lays the groundwork for many upcoming features:<br />
support for the new lower-footprint "microdescriptor" directory design,<br />
future-proofing our consensus format against new hash functions or<br />
other changes, and an Android port. It also makes Tor compatible with<br />
the upcoming OpenSSL 0.9.8l release, and fixes a variety of bugs.</p>

<p>It can be downloaded at <a href="https://www.torproject.org/download.html.en" rel="nofollow">https://www.torproject.org/download.html.en</a></p>

<p><strong>Major features:</strong></p>

<ul>
<li>Directory authorities can now create, vote on, and serve multiple<br />
      parallel formats of directory data as part of their voting process.<br />
      Partially implements Proposal 162: "Publish the consensus in<br />
      multiple flavors".
</li>
<li>Directory authorities can now agree on and publish small summaries<br />
      of router information that clients can use in place of regular<br />
      server descriptors. This transition will eventually allow clients<br />
      to use far less bandwidth for downloading information about the<br />
      network. Begins the implementation of Proposal 158: "Clients<br />
      download consensus + microdescriptors".
</li>
<li>The directory voting system is now extensible to use multiple hash<br />
      algorithms for signatures and resource selection. Newer formats<br />
      are signed with SHA256, with a possibility for moving to a better<br />
      hash algorithm in the future.
</li>
<li>New DisableAllSwap option. If set to 1, Tor will attempt to lock all<br />
      current and future memory pages via mlockall(). On supported<br />
      platforms (modern Linux and probably BSD but not Windows or OS X),<br />
      this should effectively disable any and all attempts to page out<br />
      memory. This option requires that you start your Tor as root --<br />
      if you use DisableAllSwap, please consider using the User option<br />
      to properly reduce the privileges of your Tor.
</li>
<li>Numerous changes, bugfixes, and workarounds from Nathan Freitas<br />
      to help Tor build correctly for Android phones.
</li>
</ul>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Work around a security feature in OpenSSL 0.9.8l that prevents our<br />
      handshake from working unless we explicitly tell OpenSSL that we<br />
      are using SSL renegotiation safely. We are, but OpenSSL 0.9.8l<br />
      won't work unless we say we are.
</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Fix a crash bug when trying to initialize the evdns module in<br />
      Libevent 2. Bugfix on 0.2.1.16-rc.
</li>
<li>Stop logging at severity 'warn' when some other Tor client tries<br />
      to establish a circuit with us using weak DH keys. It's a protocol<br />
      violation, but that doesn't mean ordinary users need to hear about<br />
      it. Fixes the bug part of bug 1114. Bugfix on 0.1.0.13.
</li>
<li>Do not refuse to learn about authority certs and v2 networkstatus<br />
      documents that are older than the latest consensus. This bug might<br />
      have degraded client bootstrapping. Bugfix on 0.2.0.10-alpha.<br />
      Spotted and fixed by xmux.
</li>
<li>Fix numerous small code-flaws found by Coverity Scan Rung 3.
</li>
<li>If all authorities restart at once right before a consensus vote,<br />
      nobody will vote about "Running", and clients will get a consensus<br />
      with no usable relays. Instead, authorities refuse to build a<br />
      consensus if this happens. Bugfix on 0.2.0.10-alpha; fixes bug 1066.
</li>
<li>If your relay can't keep up with the number of incoming create<br />
      cells, it would log one warning per failure into your logs. Limit<br />
      warnings to 1 per minute. Bugfix on 0.0.2pre10; fixes bug 1042.
 </li>
<li>Bridges now use "reject *:*" as their default exit policy. Bugfix<br />
      on 0.2.0.3-alpha; fixes bug 1113.
</li>
<li>Fix a memory leak on directory authorities during voting that was<br />
      introduced in 0.2.2.1-alpha. Found via valgrind.
</li>
</ul>

<p>The original announcement can be found at <a href="http://archives.seul.org/or/talk/Nov-2009/msg00106.html" rel="nofollow">http://archives.seul.org/or/talk/Nov-2009/msg00106.html</a></p>

---
_comments:

<a id="comment-3433"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3433" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Cav Edwards (not verified)</span> said:</p>
      <p class="date-time">December 06, 2009</p>
    </div>
    <a href="#comment-3433">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3433" class="permalink" rel="bookmark">Tor Performance...</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor Performance...</p>
<p>Hi Blog,</p>
<p>I have been doing some testing with Tor, after downloading the code and tinkering with it a bit.</p>
<p>I first did a burn in with this online keep-alive tool: <a href="http://www.sector101.fsnet.co.uk/keepalive/keepalive.html?timer=1&amp;gr=nayncvmfnniwdysa&amp;ms=1259982155461" rel="nofollow">Ping tool</a><br />
Set to 30 Seconds refresh.I burnt Tor in for up to 120 built circuits.</p>
<p>Response and Bandwidth Performance<br />
I then took 3 results and found the highest throughput and lowest Circuit Build time (this is very closely correlated with the response time for Tor and as such I use them interchangeably.<a href="http://docs.google.com/Doc?docid=0AaYAzYus3pVYZGY1djVuandfMjlkbWs5ZjNjOQ&amp;hl=en" rel="nofollow">Graph</a></p>
<p>As you can see, the lower ping times and hence lower response times are achieved with the tuned tor. In the same manner higher throughput is achieved with the custom Tor builds.</p>
<p>I may complete correlation testing at a future date, to prove this).</p>
<p>Circuit Build Distribution<br />
I also found several distribution curves for Tor standard with no tuning, and customised versions, built using the same libraries as the standard version, with varying Pareto Distribution Quantiles and CircuitBuildTimeouts. The Pareto Quantile has been reduced to observe its affect on build timeouts. Lowering the Pareto Quantile resulted in more pointed graphs, indicating more circuits being built at lower response times.<a href="http://docs.google.com/Doc?docid=0AaYAzYus3pVYZGY1djVuandfMTljNjluZHhkdw&amp;hl=en" rel="nofollow">Graph</a></p>
<p>Bandwidth Ping Combined<br />
I then combined the Ping and Throughput graphs to arrive at the highest overall performing configuration. The highest performing configuration is with Tor able to raise its timeout rapidly to match varying network conditions. It has a Pareto Quantile of 0.3 or 30%. It also has longer recent circuits with a very low minimum timeout.<a href="http://docs.google.com/Doc?docid=0AaYAzYus3pVYZGY1djVuandfMTdjazlyazhnMg&amp;hl=en" rel="nofollow">Graph</a></p>
<p>Lan to Mobile to Lan<br />
I also tested the ability of Tor, with its new Pareto Distribution feature to track network speeds and adjust accordingly. This graph shows on the left the high ping time for the mobile network, and the low ping time for the wireless network. On the right, Tor attempts to track these varying networks when it sets a new Network Connection Speed timeout. You can see the change in ping time for the Mobile network (slower) and the wireless network (quicker), with the Network Connection Speed timeout on the right.</p>
<p>When Tor is switched from the wireless to Mobile network Tor starts increasing its Network Connection Speed timeout. At the point the rapid dip occurs in the Network Connection Speed timeout, Tor was switched from Mobile to wireless with the IpAddress and then DNS server resetting, to pick up the new network. Tor's Network Connection Speed timeout rapidly reduces to reflect timeout occuring on a faster network. <a href="http://docs.google.com/Doc?docid=0AaYAzYus3pVYZGY1djVuandfMzFjOHNqbjNnZA&amp;hl=en" rel="nofollow">Graph</a></p>
<p>Taking a look at the Network Map in Vidalia all looks ok, with circuits being generated all over the globe, but slightly centered in Europe, where my Tor client resides.</p>
<p>What does the jury think about these results ?</p>
<p>To me they suggest that you can set a low initial timeout, and with a reduction in the Pareto Quantile and the ability to let Tor raise its timeout rapidly, we have a Tor that will adjust to network conditions and provide high performance.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3469"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3469" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="lovecreatesbeauty@gmail.c0m">lovecreatesbea… (not verified)</span> said:</p>
      <p class="date-time">December 09, 2009</p>
    </div>
    <a href="#comment-3469">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3469" class="permalink" rel="bookmark">per-tab settings, blogspot comment function</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Dear Tor developer,</p>
<p>Thanks for presenting the great Tor software!</p>
<p>(I'm not a native English speaker and not good at this language, so I go to the point directly :-)</p>
<p>I'm using latest[1] Tor + Firefox combination. Can Tor be set per Tab in Firefox? I can post to your dearest blog.torproject.org by Tor + Firefox, but can not post to blogspot.com. I had the problme with previous Tor bundles.</p>
<p>Regards</p>
<p>[1] vidalia-bundle-0.2.2.6-alpha-0.2.6.exe + Firefox 3.5.5</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3477"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3477" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 09, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3469" class="permalink" rel="bookmark">per-tab settings, blogspot comment function</a> by <span content="lovecreatesbeauty@gmail.c0m">lovecreatesbea… (not verified)</span></p>
    <a href="#comment-3477">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3477" class="permalink" rel="bookmark">Tor is not per-tab because</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is not per-tab because of the protections needed inside the core of firefox that are shared amongst tabs.  You would be sad if one tab exposed your identity while another was using tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-3475"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3475" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>CAv (not verified)</span> said:</p>
      <p class="date-time">December 09, 2009</p>
    </div>
    <a href="#comment-3475">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3475" class="permalink" rel="bookmark">Tor Black Belt Edition.... coming soon.... ?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://docs.google.com/fileview?id=0B6YAzYus3pVYOGE1YmQzZTItMDJlMS00YWRkLThjOTAtNGMzM2ZkMzE5NjQx&amp;hl=en" rel="nofollow">http://docs.google.com/fileview?id=0B6YAzYus3pVYOGE1YmQzZTItMDJlMS00YWR…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3478"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3478" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 09, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3475" class="permalink" rel="bookmark">Tor Black Belt Edition.... coming soon.... ?</a> by <span>CAv (not verified)</span></p>
    <a href="#comment-3478">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3478" class="permalink" rel="bookmark">perhaps the tor-assistants</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>perhaps the tor-assistants email address is a better place for this than a blog comment.   arma and mikeperry may be interested in this, i've let them know about it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-3494"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3494" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>CAv (not verified)</span> said:</p>
      <p class="date-time">December 11, 2009</p>
    </div>
    <a href="#comment-3494">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3494" class="permalink" rel="bookmark">Its here.... Tor Black Belt Edition....</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Its finally here, after exhaustive testing, a high-performance pack has been put together for those on Windows who enjoy broadband connections, but want their privacy back - easily.</p>
<p><a href="http://thepiratebay.org/torrent/5210170/Tor_Black_Belt_Edition.exe" rel="nofollow">http://thepiratebay.org/torrent/5210170/Tor_Black_Belt_Edition.exe</a>.</p>
<p>This pack contains:<br />
    Tuned version of Polipo.<br />
    Tuned version of Tor<br />
    much faster install and uninstall time</p>
<p>Other goodies that you'll have to find out by downloading...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3501"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3501" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 11, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3494" class="permalink" rel="bookmark">Its here.... Tor Black Belt Edition....</a> by <span>CAv (not verified)</span></p>
    <a href="#comment-3501">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3501" class="permalink" rel="bookmark">documentation? specs?  you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>documentation? specs?  you should document what this is and how to re-create it from scratch, otherwise most will assume it's a trojan or spyware.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-3504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3504" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 11, 2009</p>
    </div>
    <a href="#comment-3504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3504" class="permalink" rel="bookmark">Documentation...</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have a documentation that gets setup.</p>
<p>I am taking what you say on board and will do my best to satisfy the request for more detail.</p>
<p>I am more than happy to work within any restrictions or augmentations that the community places.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3508"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3508" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 11, 2009</p>
    </div>
    <a href="#comment-3508">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3508" class="permalink" rel="bookmark">Black Belt Edition v00.00.02</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Black Belt Edition v00.00.02 is the current one, be great to hear some feedback.<br />
Still not sure if this is the right place for all this - but I had to start somewhere.</p>
<p><a href="http://thepiratebay.org/search/tor+black+belt/0/3/300" rel="nofollow">http://thepiratebay.org/search/tor+black+belt/0/3/300</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3512"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3512" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3508" class="permalink" rel="bookmark">Black Belt Edition v00.00.02</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-3512">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3512" class="permalink" rel="bookmark">PLEASE UPLOAD Tor Black Belt</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>PLEASE UPLOAD Tor Black Belt Edition v00.00.02 AT THE <a href="http://hotfile.com" rel="nofollow">http://hotfile.com</a> OR<br />
<a href="http://uploading.com" rel="nofollow">http://uploading.com</a>  (NOT RAPIDSHARE) AND GIVE US THE LINK , AS WE HAVE PROBLEMS DOWNLOAD IT FROM THE TORRENTS</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-3515"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3515" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2009</p>
    </div>
    <a href="#comment-3515">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3515" class="permalink" rel="bookmark">ok - im a bit busy right</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ok - im a bit busy right now.,.. and working on an even better version.</p>
<p>I will be in a position to upload that once tested, and will do so to those links.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3516"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3516" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3515" class="permalink" rel="bookmark">ok - im a bit busy right</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-3516">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3516" class="permalink" rel="bookmark">THANKS !</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>THANKS !</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-3528"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3528" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2009</p>
    </div>
    <a href="#comment-3528">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3528" class="permalink" rel="bookmark">The new Tor -- both alpha</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The new Tor -- both alpha and stable are completely not working for me. I think it has something to do with the switch to polipo.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3541"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3541" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 14, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3528" class="permalink" rel="bookmark">The new Tor -- both alpha</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-3541">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3541" class="permalink" rel="bookmark">What does Message Log state?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What does Message Log state?  What errors do you receive?  Have you joined #tor or asked tor-assistants for help?  </p>
<p>giving details solves problems, not wild speculation.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-3664"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3664" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2009</p>
    </div>
    <a href="#comment-3664">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3664" class="permalink" rel="bookmark">Hello there, OS X 10.4.11 on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello there, OS X 10.4.11 on 1gen black macbook, the new Vidalia doesn't work in any shape or form, the program does not even boot up, only jumps once and dies. I only "updated" because my previous working version prompted me to do so. Now I feel dejected. I hope the previous version is still kicking around somewhere.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3960"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3960" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 13, 2010</p>
    </div>
    <a href="#comment-3960">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3960" class="permalink" rel="bookmark">0.2.2.6 alpha for windows</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>0.2.2.6 alpha for windows works with firefix, ms ie 8 and chrome 4.0.249.64 (35722) beta. thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-3961"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3961" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 13, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-3960" class="permalink" rel="bookmark">0.2.2.6 alpha for windows</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-3961">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3961" class="permalink" rel="bookmark">newest firefox of course:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>newest firefox of course: 3.5.7 :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-5577"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-5577" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 04, 2010</p>
    </div>
    <a href="#comment-5577">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-5577" class="permalink" rel="bookmark">i play diablo 2 the game...</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i play diablo 2 the game... but atm im using a proxyfirewall and using tor. my queston is is there a way i can set up the rout where the points are closer to me. and also is there any download to play with the routing. my ping on the game is 3000+ witch is hardly usable for me... if you cant would you refer to me to a place where i can pay to try to get a faster connection?</p>
</div>
  </div>
</article>
<!-- Comment END -->
