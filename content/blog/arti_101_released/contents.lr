title: Arti 1.0.1 is released: Bugfixes and groundwork
---
author: nickm
---
pub_date: 2022-10-04
---
categories: announcements
---
summary:

Arti 1.0.1 is released and ready for download.
---
body:

Arti is our ongoing project to create an next-generation Tor client
in Rust.  Last month, we released [Arti 1.0.0].  Now we're announcing
the next release in its series, Arti 1.0.1.

The last month, our team's time has been filled with company meetings,
vacations, COVID recovery, groundwork for anticensorship features, and
followup from Arti 1.0.0.  Thus, this has been a fairly small release,
but we believe it's worth upgrading for.

This is a fairly small release: it fixes a few annoying bugs
(including one that would cause a [busy-loop]), tightens [log security],
[improves] [portability], exposes an API for [building circuits manually],
and contains some [preparatory work] for anticensorship support, which
we hope to deliver in early November.

You can find a more complete list of changes in our [CHANGELOG].

For more information on using Arti, see our top-level [README],
and the docmentation for the [`arti` binary].

Thanks to everybody who has helped with this release, including
Alexander Færøy, Trinity Pointard, and Yuan Lyu.

Also, our deep thanks to [Zcash Community Grants] for funding the
development of Arti!


[Arti 1.0.0]: https://blog.torproject.org/arti_100_released/
[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-101-3-october-2022
[busy-loop]: https://gitlab.torproject.org/tpo/core/arti/-/issues/572
[log security]: https://gitlab.torproject.org/tpo/core/arti/-/issues/553
[building circuits manually]: https://gitlab.torproject.org/tpo/core/arti/-/merge_requests/738
[improves]: https://gitlab.torproject.org/tpo/core/arti/-/issues/570
[portability]: https://gitlab.torproject.org/tpo/core/arti/-/merge_requests/728
[preparatory work]: https://gitlab.torproject.org/tpo/core/arti/-/issues/543
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti