title: Tails 3.2 is out
---
pub_date: 2017-09-26
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_3.1/">many security issues</a> and users should upgrade as soon as possible.</p>
<h1>Changes</h1>
<h2>New features</h2>
<ul>
<li>
<p>We added support for PPPoE and dial-up Internet connections.</p>
<p>Please <a href="mailto:tails-testers@boum.org">tell us</a> if this still doesn't work for you!</p>
</li>
<li>
<p>We installed <a href="http://kjo.herbesfolles.org/bookletimposer/"><em>BookletImposer</em></a> to convert linear PDF documents into booklets, and vice-versa.</p>
</li>
<li>
<p>We added <em>GNOME Screen Keyboard</em> to replace <em>Florence</em>, the previous virtual keyboard, which had <a href="https://labs.riseup.net/code/projects/tails/issues?utf8=%E2%9C%93&amp;set_filter=1&amp;f%5B%5D=cf_18&amp;op%5Bcf_18%5D=%3D&amp;v%5Bcf_18%5D%5B%5D=On-screen+keyboard&amp;f%5B%5D=subject&amp;op%5Bsubject%5D=%7E&amp;v%5Bsubject%5D%5B%5D=Florence&amp;f%5B%5D=fixed_version_id&amp;op%5Bfixed_version_id%5D=%3D&amp;v%5Bfixed_version_id%5D%5B%5D=294&amp;f%5B%5D=&amp;c%5B%5D=status&amp;c%5B%5D=priority&amp;c%5B%5D=fixed_version&amp;c%5B%5D=subject&amp;c%5B%5D=category&amp;c%5B%5D=cf_15&amp;c%5B%5D=assigned_to&amp;c%5B%5D=cf_9&amp;group_by=&amp;t%5B%5D=">many issues</a>.</p>
</li>
</ul>
<h2>Upgrades and changes</h2>
<ul>
<li>
<p>tails releasesUpgrade <em>Linux</em> to 4.12.12. This should improve the support for newer hardware, especially NVIDIA Maxwell graphics card.</p>
</li>
<li>
<p>Upgrade <em>Thunderbird</em> from 45.8 to 52.3.</p>
</li>
</ul>
<h3>User experience</h3>
<ul>
<li>
<p>Require a 8 GB USB stick to install Tails. 4 GB USB sticks that are already installed can still be upgraded.</p>
</li>
<li>
<p><em>Tails Installer</em> now detects when the target USB stick has Tails installed already and automatically proposes to upgrade. This made possible to remove the initial splash screen.</p>
</li>
</ul>
<h3>Security</h3>
<ul>
<li>
<p>Disable <em>Bluetooth</em> to protect against the <a href="https://www.armis.com/blueborne/"><em>BlueBorne</em> attack</a>. <a href="https://labs.riseup.net/code/issues/14655">#14655</a></p>
<p>Please <a href="mailto:tails-testers@boum.org">let us know</a> if this makes it hard for you to use Tails!</p>
</li>
<li>
<p>Increase the randomization of <span class="definition"><a href="https://en.wikipedia.org/wiki/Address%5Fspace%5Flayout%5Frandomization">ASLR</a></span> to the maximum. <a href="https://labs.riseup.net/code/issues/11840">#11840</a></p>
</li>
<li>
<p>Deny access to the D-Bus service of <em>Pidgin</em> to prevent other applications to access and modify its configuration. <a href="https://labs.riseup.net/code/issues/14612">#14612</a>.</p>
</li>
</ul>
<h2>Fixed problems</h2>
<ul>
<li>
<p>Fix the import of secret OpenPGP keys in <em>Password and Keys</em>. <a href="https://labs.riseup.net/code/issues/12733">#12733</a></p>
</li>
</ul>
<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog">changelog</a>.</p>
<p><a id="known-issues"></a></p>
<h1>Known issues</h1>
<p>None specific to this release.</p>
<p>See the list of <a href="https://tails.boum.org/support/known_issues/">long-standing issues</a>.</p>
<h1>Get Tails 3.2</h1>
<ul>
<li>
<p>To install, follow our <a href="https://tails.boum.org/install/">installation instructions</a>.</p>
</li>
<li>
<p>To upgrade, automatic upgrades are available from 3.0 and 3.1 to 3.2 (but not from 3.0.1 due to <a href="https://labs.riseup.net/code/issues/13426">#13426</a>).</p>
<p>If you cannot do an automatic upgrade or if you fail to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/upgrade/">manual upgrade</a>.</p>
</li>
<li>
<p><a href="https://tails.boum.org/install/download/">Download Tails 3.2.</a></p>
</li>
</ul>
<h1>What's coming up?</h1>
<p>Tails 3.3 is <a href="https://tails.boum.org/contribute/calendar/">scheduled</a> for November 14.</p>
<p>Have a look at our <a href="https://tails.boum.org/contribute/roadmap">roadmap</a> to see where we are heading to.</p>
<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/">contribute to Tails</a> (<a href="https://tails.boum.org/donate/#3.2">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev">talk to us</a>!</p>
<h1>Support and feedback</h1>
<p>For support and feedback, visit the <a href="https://tails.boum.org/support/">Support section</a> on the Tails website.</p>

