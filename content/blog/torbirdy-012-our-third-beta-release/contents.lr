title: TorBirdy: 0.1.2 - Our third beta release!
---
pub_date: 2013-11-04
---
author: ioerror
---
tags:

TorBirdy
Thunderbird
---
_html_body:

<p>TorBirdy 0.1.2 is out! All users are encouraged to upgrade as soon as possible, especially if you are using Thunderbird 24.</p>

<p>Notable changes in this release include:</p>

<p>0.1.2, 04 Nov 2013</p>

<ul>
<li>New options:
<ul>
<li>restore default TorBirdy settings
</li>
<li>toggle checking of new messages automatically for all accounts
</li>
</ul>
</li>
<li>The minimum version of Thunderbird we now support is 10.0 (closes <a href="https://trac.torproject.org/projects/tor/ticket/9569" rel="nofollow">#9569</a>)
 </li>
<li>`--throw-keyids' is now disabled by default (closes <a href="https://trac.torproject.org/projects/tor/ticket/9648" rel="nofollow">#9648</a>)
 </li>
<li>We are no longer forcing Thunderbird updates (closes <a href="https://trac.torproject.org/projects/tor/ticket/8341" rel="nofollow">#8341</a>)
 </li>
<li>Add support for Thunderbird 24 (Gecko 17+) (closes <a href="https://trac.torproject.org/projects/tor/ticket/9673" rel="nofollow">#9673</a>)
 </li>
<li>Enhanced support for Thunderbird chat
 </li>
<li>We have a new TorBirdy logo. Thanks to Nima Fatemi!
 </li>
<li>Improved documentation:
<ul>
<li><a href="https://trac.torproject.org/projects/tor/wiki/torbirdy" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/torbirdy</a>
</li>
</ul>
</li>
<li>Add new translations and updated existing ones
<ul>
<li>Please see the Transifex page for more information and credits
</li>
</ul>
</li>
</ul>

<p>We offer two ways to install TorBirdy -- either by visiting <a href="https://www.torproject.org/dist/torbirdy/torbirdy-0.1.2.xpi" rel="nofollow">our website</a> (<a href="https://www.torproject.org/dist/torbirdy/torbirdy-0.1.2.xpi.asc" rel="nofollow">sig</a>) or by visiting the <a href="https://addons.mozilla.org/en-us/thunderbird/addon/torbirdy/" rel="nofollow">Mozilla Add-ons page for TorBirdy</a>. Note that there may be a delay -- which can range from a few hours to days -- before the extension is reviewed by Mozilla and updated on the Add-ons page.</p>

<p>As a general anonymity and security note: we are still working on <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy#InfoLeaks" rel="nofollow">two known anonymity issues</a> with Mozilla. Please make sure that you read the <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy#BeforeusingTorBirdy" rel="nofollow">Before Using TorBirdy</a> and <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy#KnownTorBirdyIssues" rel="nofollow">Known TorBirdy Issues</a> sections on the wiki before using TorBirdy.</p>

<p>We had love help with translations, programming or anything that you think will improve TorBirdy!</p>

---
_comments:

<a id="comment-36866"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36866" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2013</p>
    </div>
    <a href="#comment-36866">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36866" class="permalink" rel="bookmark">Thank you!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-36889"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36889" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2013</p>
    </div>
    <a href="#comment-36889">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36889" class="permalink" rel="bookmark">Does TorBirdy support</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does TorBirdy support reading/posting news (nntp) over Tor, or just mail?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-36936"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36936" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 05, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-36889" class="permalink" rel="bookmark">Does TorBirdy support</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-36936">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36936" class="permalink" rel="bookmark">they cover this in the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>they cover this in the manual <a href="https://trac.torproject.org/projects/tor/wiki/torbirdy#Usenet" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/torbirdy#Usenet</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-36890"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-36890" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2013</p>
    </div>
    <a href="#comment-36890">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-36890" class="permalink" rel="bookmark">Hurray! Big, big thanks to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hurray! Big, big thanks to everyone involved in making/maintaining this excellent piece of FOSS. :)</p>
<p>Cheers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-38975"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-38975" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 25, 2013</p>
    </div>
    <a href="#comment-38975">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-38975" class="permalink" rel="bookmark">Do we verify the sig with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do we verify the sig with Erin's key, or another key?  I tried with Erin's and it didn't work, but I could be doing something wrong.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39045"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39045" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-38975" class="permalink" rel="bookmark">Do we verify the sig with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-39045">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39045" class="permalink" rel="bookmark">$ gpg</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>$ gpg torbirdy-0.1.2.xpi.asc<br />
gpg: Signature made Mon 04 Nov 2013 06:43:51 AM EST using RSA key ID 1245F783<br />
gpg: Can't check signature: public key not found<br />
$ gpg --recv-key 1245F783<br />
gpg: requesting key 1245F783 from hkp server pool.sks-keyservers.net<br />
gpg: key 4193A197: public key "Jacob Appelbaum (offline long term identity key) " imported<br />
gpg: 3 marginal(s) needed, 1 complete(s) needed, PGP trust model<br />
gpg: depth: 0  valid:   1  signed:  36  trust: 0-, 0q, 0n, 0m, 0f, 1u<br />
gpg: depth: 1  valid:  36  signed: 111  trust: 36-, 0q, 0n, 0m, 0f, 0u<br />
gpg: next trustdb check due at 2013-12-07<br />
gpg: Total number processed: 1<br />
gpg:               imported: 1  (RSA: 1)<br />
$ gpg torbirdy-0.1.2.xpi.asc<br />
gpg: Signature made Mon 04 Nov 2013 06:43:51 AM EST using RSA key ID 1245F783<br />
gpg: Good signature from "Jacob Appelbaum (offline long term identity key) "<br />
gpg:                 aka "Jacob Appelbaum (offline long term identity key) "<br />
Primary key fingerprint: 228F AD20 3DE9 AE7D 84E2  5265 CF9A 6F91 4193 A197<br />
     Subkey fingerprint: E729 FE2D EE92 DB51 1AC9  FF91 590C 7D91 1245 F783</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-39059"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-39059" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-39059">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-39059" class="permalink" rel="bookmark">Thanks, that did the trick.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks, that did the trick.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
