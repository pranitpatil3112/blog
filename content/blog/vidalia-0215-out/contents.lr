title: Vidalia 0.2.15 is out!
---
pub_date: 2011-10-11
---
author: chiiph
---
tags:

vidalia
bug fixes
vidalia release
---
categories:

applications
releases
---
_html_body:

<p>Hello everybody,</p>

<p>I'm happy to announce a new version for Vidalia, 0.2.15.</p>

<p>If you find any bugs or have ideas on how to improve Vidalia, please<br />
remember to go to <a href="https://trac.torproject.org/" rel="nofollow">https://trac.torproject.org/</a> and file a ticket for it!</p>

<p>You can find the source tarball and its signature in here:<br />
<a href="https://www.torproject.org/dist/vidalia/vidalia-0.2.15.tar.gz" rel="nofollow">https://www.torproject.org/dist/vidalia/vidalia-0.2.15.tar.gz</a><br />
<a href="https://www.torproject.org/dist/vidalia/vidalia-0.2.15.tar.gz.asc" rel="nofollow">https://www.torproject.org/dist/vidalia/vidalia-0.2.15.tar.gz.asc</a></p>

<p>TBB and other packages are going to be here soon, please be patient.</p>

<p>Here's what changed:</p>

<p><strong>0.2.15  07-Oct-2011</strong></p>

<ul>
<li> Draw the bandwidth graph curves based on the local maximum, not<br />
    the global maximum. Fixes bug 2188.</li>
<li> Add an option for setting up a non-exit relay to the Sharing<br />
    configuration panel. This is meant to clarify what an exit policy<br />
    and an exit relay are. Resolves bug 2644.</li>
<li> Display time statistics for bridges in UTC time, rather than local<br />
    time. Fixes bug 3342.</li>
<li> Change the parameter for ordering the entries in the Basic Log<br />
    list from currentTime to currentDateTime to avoid missplacing<br />
    entries from different days.</li>
<li> Check the tor version and that settings are sanitized before<br />
    trying to use the port autoconfiguration feature. Fixes bug 3843.</li>
<li> Provide a way to hide Dock or System Tray icons in OSX. Resolves<br />
    ticket 2163.</li>
<li> Make new processes appear at front when they are started (OSX<br />
    specific).</li>
</ul>

---
_comments:

<a id="comment-12102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12102" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 12, 2011</p>
    </div>
    <a href="#comment-12102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12102" class="permalink" rel="bookmark">I repeatedly receive the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I repeatedly receive the warning message "Tried to establish rendezvous on non-OR or non-edge circuit." Is this something to be concerned about? If not, why is it classified as a "warning"? I'm running Vidalia 0.2.14 and Tor 0.2.2.32</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-12902"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12902" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 28, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-12102" class="permalink" rel="bookmark">I repeatedly receive the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12902">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12902" class="permalink" rel="bookmark">I also get this &quot;Tried to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I also get this "Tried to establish rendezvous on non-OR or non-edge circuit" on 0.2.2.34</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12565"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12565" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 07, 2011</p>
    </div>
    <a href="#comment-12565">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12565" class="permalink" rel="bookmark">Will you be making a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Will you be making a Vidalia-only installer available like you did for 0.2.12? The file I want is:<br />
<a href="https://www.torproject.org/dist/vidalia/vidalia-0.2.15.msi" rel="nofollow">https://www.torproject.org/dist/vidalia/vidalia-0.2.15.msi</a></p>
<p>Basically like the Tor "expert" installer. How else can I upgrade Vidalia without affecting my other apps or config?</p>
</div>
  </div>
</article>
<!-- Comment END -->
