title: New Release: Tor Browser 9.5a7
---
pub_date: 2020-03-11
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-9.5
---
categories: applications
---
_html_body:

<p>Tor Browser 9.5a7 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/9.5a7/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-905">latest stable release</a> instead.</p>
<p>This release resolves breakage introduced in version 9.5a6 where <a href="https://trac.torproject.org/projects/tor/ticket/33514">non-en-US versions are not starting up</a>. The easiest solution for this problem is deleting the current Tor Browser Alpha installation and <a href="https://www.torproject.org/download/alpha/">downloading a full version 9.5a7</a> instead of upgrading the current instance. Updating an existing Tor Browser Alpha installation that is currently experiencing the start-up crash requires manually applying the "incremental update" because the browser crashes before the automatic updater is executed. Please follow the <a href="https://trac.torproject.org/projects/tor/wiki/doc/TorBrowser/Updating#ManuallyApplyingMARUpdate">instructions</a> on the wiki page.</p>
<p>As an alternative, an existing installation may be modified so it uses the en-US locale and avoids the bug causing the crash. This modification is only needed so the browser can fully start and install the update. After the update is installed, the locale modification can be undone. Within the <a href="https://trac.torproject.org/projects/tor/wiki/doc/TorBrowser/Platform_Installation">user's profile directory</a>, open the file "prefs.js" in a text editor. Add a line in the file (at the bottom is a good location):</p>
<blockquote><p>user_pref("intl.locale.requested", "en-US");</p>
</blockquote>
<p>You may force the browser's update check by opening "Help-&gt;About Tor Browser" or the browser will automatically check for an update within the next 12 hours. After you receive a notification that an update is available restart the browser as usual. After the browser restarts you may quit the program and delete the line you added into "prefs.js". When you open the file in a text editor you may need to search for "intl.locale.requested" because the line may be at a different location than where you added it.</p>
<p> </p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">changelog</a> since Tor Browser 9.5a6 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Translations update</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li>Update Tor Launcher to 0.2.21.3
<ul>
<li>Translations update</li>
<li>Bug 33514: non-en-US Tor Browser 9.5a6 won't start up</li>
</ul>
</li>
<li>Bug 32645: Update URL bar onion indicators</li>
</ul>
</li>
</ul>

