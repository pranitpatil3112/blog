title: Tor Browser 6.0a2 is released
---
pub_date: 2016-02-15
---
author: gk
---
tags:

tor browser
tbb
tbb-6.0
---
categories:

applications
releases
---
_html_body:

<p>A new alpha Tor Browser release is available for download in the <a href="https://dist.torproject.org/torbrowser/6.0a2/" rel="nofollow">6.0a2 distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-alpha" rel="nofollow">alpha download page</a>.</p>

<p>This release features important <a href="https://www.mozilla.org/en-US/security/known-vulnerabilities/firefox-esr/#firefoxesr38.6.1" rel="nofollow">security updates</a> to Firefox. Users on the security level "High" or "Medium-High" were not affected by the bugs in the Graphite font rendering library.</p>

<p>Additionally, we fixed a number of issues found with the release of Tor Browser 5.5, which already got addressed in Tor Browser 5.5.1.</p>

<p>Here is the complete changelog since 6.0a1:</p>

<p>Tor Browser 6.0a2 -- February 15 2016</p>

<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 38.6.1esr
    </li>
<li>Update NoScript to 2.9.0.3
    </li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18168" rel="nofollow">Bug 18168</a>: Don't clear an iframe's window.name (fix of #16620)
    </li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18137" rel="nofollow">Bug 18137</a>: Add two new obfs4 default bridges
  </li>
</ul>
</li>
<li>Windows
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18169" rel="nofollow">Bug 18169</a>: Whitelist zh-CN UI font
  </li>
</ul>
</li>
<li>OS X
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18172" rel="nofollow">Bug 18172</a>: Add Emoji support
  </li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://trac.torproject.org/projects/tor/ticket/18172" rel="nofollow">Bug 18172</a>: Add Emoji support
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-156590"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-156590" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2016</p>
    </div>
    <a href="#comment-156590">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-156590" class="permalink" rel="bookmark">;)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-159235"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-159235" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 29, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-156590" class="permalink" rel="bookmark">;)</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-159235">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-159235" class="permalink" rel="bookmark">Cool :)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Cool :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-156604"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-156604" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2016</p>
    </div>
    <a href="#comment-156604">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-156604" class="permalink" rel="bookmark">&quot;Experimental tor browser&quot;</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Experimental tor browser" hangs would supply your information.<br />
Best regards,</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-156720"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-156720" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">February 16, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-156604" class="permalink" rel="bookmark">&quot;Experimental tor browser&quot;</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-156720">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-156720" class="permalink" rel="bookmark">Yes, please: How can I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, please: How can I reproduce your issue?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-156698"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-156698" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 16, 2016</p>
    </div>
    <a href="#comment-156698">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-156698" class="permalink" rel="bookmark">hi, thank you for the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi, thank you for the update.<br />
But one of my issues is not yet solved. Sometimes I watch camchats where the chat partner  is to see (and to hear) as streaming video. This worked fine until - maybe-  beginning or middle of december. After an update it stopped working. Instead is to read: video format or MIME-TYP is not supported. By the way, it works with a normal version of Firefox. What plugin or addon I have to install to watch these camchat streamings? I have installed Shockwave Flash 20.0.0.306.<br />
Thanks for an answer and help.</p>
<p>Phillip.Th.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-156722"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-156722" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">February 16, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-156698" class="permalink" rel="bookmark">hi, thank you for the</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-156722">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-156722" class="permalink" rel="bookmark">5.0.5 had</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>5.0.5 had <a href="https://bugs.torporject.org/17207" rel="nofollow">https://bugs.torporject.org/17207</a> fixed which does not allow websites to enumerate your MIME-types any longer by default as this poses a non-negligible fingerprinting risk. Does unchecking "Change details that distinguish you from other users" "fix" your problem (you get there via the green onion on your toolbar and clicking on  Privacy and Security Settings...</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-156893"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-156893" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 17, 2016</p>
    </div>
    <a href="#comment-156893">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-156893" class="permalink" rel="bookmark">very good application we</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>very good application we need it here</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-157105"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-157105" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 20, 2016</p>
    </div>
    <a href="#comment-157105">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-157105" class="permalink" rel="bookmark">Next Firefox ESR is 45, I&#039;m</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Next Firefox ESR is 45, I'm guessing you are going to offer 64-bit at that point?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-157136"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-157136" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 20, 2016</p>
    </div>
    <a href="#comment-157136">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-157136" class="permalink" rel="bookmark">Seems to be either a problem</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Seems to be either a problem in 6.0a2 with using flickr, or they have changed something to prevent access to all the pages of their blogs. All I get is just the plain text. Anyone else?  I have privacy set to maximum but all worked like that under last version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-158755"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-158755" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 28, 2016</p>
    </div>
    <a href="#comment-158755">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-158755" class="permalink" rel="bookmark">My router configuration is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My router configuration is almost default, including "connection if required" checked, is that the key that Tor connection is failed with obfs3?</p>
</div>
  </div>
</article>
<!-- Comment END -->
