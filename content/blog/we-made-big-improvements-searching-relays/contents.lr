title: We Made Big Improvements to Searching for Relays
---
pub_date: 2017-12-20
---
author: irl
---
tags:

metrics
relays
bridges
---
categories:

circumvention
metrics
relays
---
summary: Relay Search, formerly known as Atlas, is a web application to learn about currently running Tor relays and bridges. You can search by fingerprint, nickname, country, flags, and contact information and be returned information about advertised bandwidth, uptime, exit policies, and more.
---
_html_body:

<p><a href="https://atlas.torproject.org/">Relay Search</a>, formerly known as Atlas, is a web application to learn about currently running Tor relays and bridges. You can search by fingerprint, nickname, country, flags, and contact information and be returned information about advertised bandwidth, uptime, exit policies, and more.</p>
<p><img alt="Screenshot of Relay Search" src="/static/images/blog/inline-images/relay-search.png" width="600" class="align-center" /></p>
<h3>What's New</h3>
<p>The biggest change has been the introduction of a new search type: <a href="https://atlas.torproject.org/#aggregate">aggregated search</a>.</p>
<p>The aggregated functionality is based on the functionality that <a href="https://compass.torproject.org/">Compass</a> provided. Tor Metrics plans to shut down Compass at the end of the year. The Relay Search implementation allows for aggregate searches using the existing search syntax and allows drilling down to further aggregate by other properties, or to display the individual relays in a particular group. The aggregated search results are shown in a table by default but it is also possible to view country-based aggregations in <a href="https://atlas.torproject.org/#map">a map view</a> with countries highlighted based on consensus weight, guard/middle/exit probabilities or advertised bandwidth.</p>
<p><img alt="Screenshot of new Aggregate Search function" src="/static/images/blog/inline-images/relay-search-aggregate.png" class="align-center" /></p>
<p align="center"><i>Relay Search shows a number of relays that have been named in honor of Dmitry Bogatov, a software developer under house arrest in Russia for running a Tor exit relay</i></p>
<p>The "<a href="https://atlas.torproject.org/#advanced">Advanced Search</a>" form will let you explore all the different search parameters that Relay Search offers for both individual relays and for the new aggregated queries.</p>
<p>Finally, we've made a number of other improvements. The highlights are listed here:</p>
<ul>
<li>Performance Improvements:
<ul>
<li>It is now possible to load up to 2000 relays in a single search query.</li>
<li>The "Top Relays" page now loads the top 250 relays, not just the top 10.</li>
</ul>
</li>
<li>New Features:
<ul>
<li>Many more details in the individual relay and bridge views can now be used to perform new searches [<a href="https://bugs.torproject.org/22175">#22175</a>]</li>
<li>Added the NoEdConsensus flag [<a href="https://bugs.torproject.org/21636">#21636</a>]</li>
<li>Added a number of synthetic flags including FallbackDir, Hibernating, ReachableIPv6, IPv6 Exit, and Unmeasured [<a href="https://bugs.torproject.org/21619">#21619</a>, <a href="https://bugs.torproject.org/10401">#10401</a>, <a href="https://bugs.torproject.org/24388">#24388</a>, <a href="https://bugs.torproject.org/22185">#22185</a>]</li>
</ul>
</li>
<li>UX Improvements:
<ul>
<li>Made the bandwidth and IP columns in the search view meaningfully sortable [<a href="https://bugs.torproject.org/15508">#15508</a>]</li>
<li>Merged the theme and branding from the Tor Metrics website to prepare for integration next year [<a href="https://bugs.torproject.org/23518">#23518</a>]</li>
<li>Disabled autocorrect and autocapitalisation to make it easier to search from iOS devices [<a href="https://bugs.torproject.org/23797">#23797</a>]</li>
<li>Disables plotting of graphs when in Tor Browser high-security mode while still providing other details [<a href="https://bugs.torproject.org/19654">#19654</a>]</li>
</ul>
</li>
</ul>
<p>Thanks to the work of Ana Custura, Sebastian Hahn, Mark Henderson, and anonymous contributors for their help in producing patches since I last <a href="https://blog.torproject.org/atlas-recent-improvements">wrote about improvements in this area</a>. If you have a feature you'd like to see, or if you spot something not working quite correctly, please do feel free to <a href="https://trac.torproject.org/projects/tor/newticket?component=Metrics/Relay%20Search">open a ticket about it</a>. If you would like to contribute to fixing some of our <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/MetricsTeam/RelaySearch#ActiveTasks">existing tickets</a>, we have <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/MetricsTeam/RelaySearch#Hacking">a guide for contributing to Relay Search</a>.</p>

---
_comments:

<a id="comment-273165"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273165" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
    <a href="#comment-273165">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273165" class="permalink" rel="bookmark">Thank you Tor patriots for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you Tor patriots for your service!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273168"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273168" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273165" class="permalink" rel="bookmark">Thank you Tor patriots for…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273168">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273168" class="permalink" rel="bookmark">?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-273173"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273173" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Si (not verified)</span> said:</p>
      <p class="date-time">December 20, 2017</p>
    </div>
    <a href="#comment-273173">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273173" class="permalink" rel="bookmark">Great job Tor!!!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great job Tor!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273176"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273176" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 21, 2017</p>
    </div>
    <a href="#comment-273176">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273176" class="permalink" rel="bookmark">How such tools like atlas…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How such tools like atlas find operating systems of tor relays? I cannot see it in tor consensus file in /var/lib/tor or in replies from tor ControlPort.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273178"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273178" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  irl
  </article>
    <div class="comment-header">
      <p class="comment__submitted">irl said:</p>
      <p class="date-time">December 21, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-273176" class="permalink" rel="bookmark">How such tools like atlas…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-273178">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273178" class="permalink" rel="bookmark">Relays publish server…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Relays publish server descriptors, which contain information about the relay such as its nickname, IP addresses and public keys. These also include a "platform" string that indicates the version of tor in use, and the operating system.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-273183"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273183" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to irl</p>
    <a href="#comment-273183">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273183" class="permalink" rel="bookmark">OK. Do you mean I need to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OK. Do you mean I need to enable option <span class="geshifilter"><code class="php geshifilter-php">FetchUselessDescriptors</code></span> in my tor client and then look at file <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #339933;">/</span><span style="color: #000000; font-weight: bold;">var</span><span style="color: #339933;">/</span>lib<span style="color: #339933;">/</span>tor<span style="color: #339933;">/</span>cache<span style="color: #339933;">-</span>descriptors</code></span>? That seems working, but... my tor client started to download that descriptors using 10-20 different Tor nodes, so it doesn't pool these descriptors through my guard nodes. It makes me distinguished among other tor users, which is bad. So,  2 other questions:</p>
<p>1) Can I download these descriptors through my entry guard nodes?<br />
2) Will my guard nodes be able to distinguish me from other tor clients because of these fetches? (I don't know, how exactly these descriptors are download, and what's the role of my guards).</p>
<p>Well, problems 1-2 can be mitigated if I use some extra private tor client only used to download full tor descriptors (e.g. via tor-over-tor), but it would be good to have more straightforward solution.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-273184"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273184" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 22, 2017</p>
    </div>
    <a href="#comment-273184">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273184" class="permalink" rel="bookmark">I checked it with google and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I checked it with google and man torrc, but couldn't find simple answer. Does any tor client need to connect to DirPort of any tor nodes? According to my observations, I have never seen such traffic in my tor client. So, should I forbid these connections in my firewall? (ORPort will be allowed).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273190"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273190" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>revin napise (not verified)</span> said:</p>
      <p class="date-time">December 23, 2017</p>
    </div>
    <a href="#comment-273190">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273190" class="permalink" rel="bookmark">Nice</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nice</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273195"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273195" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Paul Templeton (not verified)</span> said:</p>
      <p class="date-time">December 23, 2017</p>
    </div>
    <a href="#comment-273195">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273195" class="permalink" rel="bookmark">Thanx All,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanx All,</p>
<p>I love the new interface....</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273220"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273220" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>godle (not verified)</span> said:</p>
      <p class="date-time">December 25, 2017</p>
    </div>
    <a href="#comment-273220">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273220" class="permalink" rel="bookmark">tor bridges connection failed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>tor bridges connection failed</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273260"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273260" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>oscar (not verified)</span> said:</p>
      <p class="date-time">December 31, 2017</p>
    </div>
    <a href="#comment-273260">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273260" class="permalink" rel="bookmark">After logging into the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>After logging into the latest TorBrowser, why is it that the second relay down in the list (when checking) never changes...no matter how many time I change sites, the relay is still the same. I ran a check on the node displayed and it suspiciously refers to PureVPN which to my knowledge is rather dodgy to say the least. Is someone trying to track me and expose my IP or worse still, have they succeeded?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273282"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273282" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 02, 2018</p>
    </div>
    <a href="#comment-273282">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273282" class="permalink" rel="bookmark">Sorry but dont know what you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry but dont know what you want?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273283"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273283" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ausdiver (not verified)</span> said:</p>
      <p class="date-time">January 02, 2018</p>
    </div>
    <a href="#comment-273283">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273283" class="permalink" rel="bookmark">Do i need to download…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do i need to download anything to inprove TOR</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-273314"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-273314" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>was (not verified)</span> said:</p>
      <p class="date-time">January 05, 2018</p>
    </div>
    <a href="#comment-273314">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-273314" class="permalink" rel="bookmark">I can only use Tor when it…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can only use Tor when it is updating itself.<br />
Other time it doesn't work. I think it is a bug.<br />
maybe  the reason is that I exclude some nodes from some countries. because I am in China.<br />
some Nodes in China don't work correctly.<br />
Hope someone can help me with this problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
