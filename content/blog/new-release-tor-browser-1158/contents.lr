title: New Release: Tor Browser 11.5.8 (Android, Windows, macOS, Linux)
---
pub_date: 2022-11-22
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5.8 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.8 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.8/). This release will not be published on Google Play due to their target [API level requirements](https://developer.android.com/google/play/requirements/target-sdk). Assuming we do not run into any major problems, Tor Browser 11.5.9 will be an Android-only release that fixes this issue.

Tor Browser 11.5.8 backports the following security updates from Firefox ESR 102.5 to Firefox ESR 91.13 on Windows, macOS and Linux:
- [CVE-2022-43680: In libexpat through 2.4.9, there is a use-after free caused by overeager destruction of a shared DTD in XML_ExternalEntityParserCreate in out-of-memory situations.](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2022-43680)
- [CVE-2022-45403: Service Workers might have learned size of cross-origin media files](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45403)
- [CVE-2022-45404: Fullscreen notification bypass](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45404)
- [CVE-2022-45405: Use-after-free in InputStream implementation](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45405)
- [CVE-2022-45406: Use-after-free of a JavaScript Realm](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45406)
- [CVE-2022-45408: Fullscreen notification bypass via windowName](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45408)
- [CVE-2022-45409: Use-after-free in Garbage Collection](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45409)
- [CVE-2022-45410: ServiceWorker-intercepted requests bypassed SameSite cookie policy](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45410)
- [CVE-2022-45411: Cross-Site Tracing was possible via non-standard override headers](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45411)
- [CVE-2022-45412: Symlinks may resolve to partially uninitialized buffers](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45412)
- [CVE-2022-45416: Keystroke Side-Channel Leakage](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45416)
- [CVE-2022-45420: Iframe contents could be rendered outside the iframe](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45420)
- [CVE-2022-45421: Memory safety bugs fixed in Firefox 107 and Firefox ESR 102.5](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/#CVE-2022-45421)

Tor Browser 11.5.8 updates GeckoView on Android to Firefox ESR 102.5 and includes [important security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-48/). Tor Browser 11.5.8 backports the following security updates from Firefox 107 to Firefox ESR 102.5 on Android:
- [CVE-2022-45413: SameSite=Strict cookies could have been sent cross-site via intent URLs](https://www.mozilla.org/en-US/security/advisories/mfsa2022-47/#CVE-2022-45413)

The full changelog since [Tor Browser 11.5.7](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-11.5/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt) is:

- All Platforms
 - Update Translations
 - Update OpenSSL to 1.1.1s
 - Update NoScript to 11.4.12
 - Update tor to 0.4.7.11
 - Update zlib to 1.2.13
 - [Bug tor-browser-build#40622](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40622): Update obfs4proxy to 0.0.14 in Tor Browser
- Windows + macOS + Linux
  - [Bug tor-browser#31064](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/31064): Letterboxing is enabled in priviledged contexts too
  - [Bug tor-browser#32411](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/32411): Consider adding about:tor and others to the list of pages that do not need letterboxing
  - [Bug tor-browser#41105](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41105): Tor Does Not Clear CORS Preflight Cache despite creating a "New Identity"
  - [Bug tor-browser#41413](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41413): Backup intl.locale.requested in 11.5.x
  - [Bug tor-browser#41434](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41434): Letterboxing bypass through secondary tab (popup/popunder...)
  - [Bug tor-browser#41456](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41456): Backport ESR 102.5 security fixes to 91.13-based Tor Browser
  - [Bug tor-browser#41460](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41460): Migrate new identity and security level preferences in 11.5.8
  - [Bug tor-browser#41463](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41463): Backport fix for CVE-2022-43680
- Android
  - Update GeckoView to 102.5.0esr
  - [Bug tor-browser#41461](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41461): Backport Android-specific 107-rr security fixes to 102.5-esr based Geckoview
- Build
  - All Platforms
    - Update Go to 1.18.8
    - [Bug tor-browser-build#40658](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40658): Create an anticensorship team keyring
    - [Bug tor-browser-build#40690](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40690): Revert fix for zlib build break
