title: Tails 2.2 is out
---
pub_date: 2016-03-08
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_2.0.1/" rel="nofollow">many security issues</a> and users should upgrade as soon as possible.</p>

<h2>New features</h2>

<ul>
<li>Add support for viewing DVDs with DRM protection. (<a href="https://labs.riseup.net/code/issues/7674" rel="nofollow">#7674</a>)</li>
</ul>

<h2>Upgrades and changes</h2>

<ul>
<li>Replace <em>Vidalia</em>, which has been unmaintained for years, with:
<ul>
<li>a system status icon indicating whether Tails is connected to Tor or not,</li>
<li><a href="https://tails.boum.org/doc/anonymous_internet/Tor_status/" rel="nofollow"><em>Onion Circuits</em></a> to display a list of the current Tor circuits and connections.</li>
</ul>
</li>
</ul>

<ul>
<li>Automatically save the database of <em>KeePassX</em> after every change to prevent data loss when shutting down. (<a href="https://labs.riseup.net/code/issues/11147" rel="nofollow">#11147</a>)</li>
<li>Update <em>Tor Browser</em> to 5.5.3.
<ul>
<li>Improve Japanese-style glyph display.</li>
</ul>
</li>
<li>Upgrade <em>I2P</em> to <a href="https://geti2p.net/en/blog/post/2016/01/27/0.9.24-Release" rel="nofollow">0.9.24</a>.</li>
<li>Disable the <i>Alt + Shift</i> and <i>Left Shift + Right Shift</i> keyboard shortcuts that used to switch to the next keyboard layout. You can still use <i>Meta + Space</i> to change keyboard layout. (<a href="https://labs.riseup.net/code/issues/11042" rel="nofollow">#11042</a>)</li>
</ul>

<h2>Fixed problems</h2>

<ul>
<li>Fix <strong>optional PGP key</strong> feature of <em>WhisperBack</em>. (<a href="https://labs.riseup.net/code/issues/11033" rel="nofollow">#11033</a>)</li>
<li>Fix saving of <em>WhisperBack</em> report to a file when offline. (<a href="https://labs.riseup.net/code/issues/11133" rel="nofollow">#11133</a>)</li>
<li>Make Git verify the integrity of transferred objects. (<a href="https://labs.riseup.net/code/issues/11107" rel="nofollow">#11107</a>)</li>
</ul>

<p>For more details, see also our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">changelog</a>.</p>

<h2>Known issues</h2>

<p>See the current list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">known issues</a>.</p>

<h2>Install or upgrade</h2>

<p>To install, follow our <a href="https://tails.boum.org/install/" rel="nofollow">installation instructions</a>.</p>

<p>An automatic upgrade is available from 2.0.1 to 2.2.</p>

<p>If it is impossible to automatically upgrade your Tails, read our <a href="https://tails.boum.org/upgrade/" rel="nofollow">upgrade instructions</a>.</p>

<p>If your Tails fails to start after an automatic upgrade, please <a href="https://tails.boum.org/doc/first_steps/upgrade/#manual" rel="nofollow">try to do a manual upgrade</a>.</p>

<h2>What's coming up?</h2>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for April 19.</p>

<p>Have a look at our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/contribute/how/donate/" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/contribute/talk/" rel="nofollow">talk to us</a>!</p>

<h2>Support and feedback</h2>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

