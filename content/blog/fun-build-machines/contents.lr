title: Fun with build machines
---
pub_date: 2008-08-06
---
author: phobos
---
tags:

packages
centos4
osx tiger
osx 10.4
---
categories: releases
---
_html_body:

<p>Perhaps you've noticed that the packages for CentOS 4 and OSX Tiger/10.4 haven't been updated lately. Welcome to dead hard drives.</p>

<p>For a long while, I used <a href="http://www.vmware.com/products/server/" rel="nofollow">VMware Server</a> for guest OSes to build the various rpm and windows packages.  This mostly worked well.  And then both drives in the physical server I used to host the vmware instance failed.  A two-drive RAID 1 array doesn't like it when both drives fail.  I replaced the drives, re-installed Debian, and attempted to install vmware server again.  The vmware kernel module refused to load.  I tried the old tricks to get it to work, nothing.  I finally looked at some script/patch that I found via Google, and got the module to load.  Then my license key didn't work anymore.  </p>

<p>In frustration, I gave up and installed <a href="http://www.virtualbox.org/" rel="nofollow">VirtualBox</a>.  CentOS 4 defaults to using an SMP kernel on install, which Virtualbox (aka qemu) doesn't like at all.  CentOS 4 installs just fine, it just won't boot after install.  I haven't had time to further fix the problem.  For the time being, there's no CentOS 4 (Redhat 4 rpms) for Tor.  </p>

<p>As for OSX, well, there was no raid array of any kind, just a single drive in a mac mini.  It died in a fit of 0xE0030005 (Undefined) errors and now won't boot at all.  A new drive is on the way.  I expect to have OSX Tiger/10.4 packages in a week or so.  The good news is that the <a href="http://www.everymac.com/systems/apple/imac/stats/imac_se_dv_400.html" rel="nofollow">Panther Mac</a> continues to work just fine.  </p>

<p>Perhaps it's time to start using Amazon's EC2 or something similar instead of messing with all this hardware and virtualization software.  Or maybe I should work on hacking OSX 10.4 and 10.5 into virtualbox.</p>

<p>** Update 2008-08-06:  new drive was DOA.  And it appears the logic board on the mac mini is fried.  Ugh.</p>

---
_comments:

<a id="comment-164"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://www.slackware.it/">sid77 (not verified)</a> said:</p>
      <p class="date-time">August 06, 2008</p>
    </div>
    <a href="#comment-164">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164" class="permalink" rel="bookmark">maybe trying to fit OS X</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>maybe trying to fit OS X inside VirtualBox is not worth the hassle, on the other hand if you've a working OS X machine you can always build programs for the previous version of the operating system (in term of toolchain and basic libraries).<br />
10.3 can build programs up to 10.1 if you install the Xcode Legacy Tools (see the "Developer Tools" section under the <a href="https://connect.apple.com/" rel="nofollow">ADC member site</a> I'm pretty confident that Tiger and Leopard will have something similar (at least you can always install Tiger Xcode 2.x in Leopard alongside its own 3.x)<br />
btw, also my powerbook disk died recently :-/</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-166"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-166" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 06, 2008</p>
    </div>
    <a href="#comment-166">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-166" class="permalink" rel="bookmark">Well, we don&#039;t build the osx</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, we don't build the osx packages using xcode, but rather configure and makefiles.  Converting this system to xcode is not so easy; therefore this makes backporting challenging.  The other problem is I only have an older OS to use, not a newer.  So I can't forward compile the libs and such.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-172"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-172" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://www.slackware.it/">sid77 (not verified)</a> said:</p>
      <p class="date-time">August 08, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-172">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-172" class="permalink" rel="bookmark">misunderstood</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There's a little of misunderstood here :-p<br />
When I was talking about Xcode I was referring to the Xcode package itself rather then the single program.<br />
And installing the Xcode package brings you a whole toolchain (glibc, gcc and binutils) plus a gui program called Xcode (and, well, several others) ;-)<br />
So, in the end, there's no porting involving: you install Xcode and legacy tools (or the two versions of the package) and you can build programs with the toolchain of your current system and with the toolchains of the previous ones.<br />
But yes, the problem regarding the version of the only working OS X is a stopper right now :(</p>
<p>good luck!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-168"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-168" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Wearyman (not verified)</span> said:</p>
      <p class="date-time">August 07, 2008</p>
    </div>
    <a href="#comment-168">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-168" class="permalink" rel="bookmark">Backup?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not to sound snarky or rude, (God knows I certainly couldn't do the great Tor/Vidalia work you guys do) But, regardless of the virtualization issues you are having,  didn't you have a backup system in place?</p>
<p>I have a back up for all my systems at home and the most important things I have are digital photos of my kids and a pirate collection of all 6 Mythbusters seasons.  It seems remarkably foolish to be working on Tor source code and NOT have it (and your working environments) back up AT LEAST to a network-connected hard drive, if not a full tape system.</p>
<p>Of course, I have no clue about the financial situation you folks are in, so it may not be feasable to run a full tape backup.  But I would strongly recommend at least a big drive in a USB enclosure, or possibly uploading your data to a server somewhere between builds.</p>
<p>I will also freely admit that I could be completely mis-reading your post, and you did have everything backed-up, and your only issue is re-building your working environments for CentOS and OSx.  If that's the case, neeeeeveer miiiind!  ;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-188"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-188" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 17, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-168" class="permalink" rel="bookmark">Backup?</a> by <span>Wearyman (not verified)</span></p>
    <a href="#comment-188">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-188" class="permalink" rel="bookmark">Yes, everything is encrypted</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, everything is encrypted and backed up at least twice.  Getting replacement hardware that actually works is the issue.  I've been through two hard drives that arrived DOA.  I await drive #3 to arrive, hopefully this one works!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-225"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 31, 2008</p>
    </div>
    <a href="#comment-225">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225" class="permalink" rel="bookmark">Fixed.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The mac mini is repaired and back online.  I've uploaded 0.2.1.4-alpha packages.</p>
</div>
  </div>
</article>
<!-- Comment END -->
