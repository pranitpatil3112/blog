title: Tor Weekly News — August 20th, 2015
---
pub_date: 2015-08-20
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirty-second issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Browser 5.0.1 is out</h1>

<p>The Tor Browser team put out a new <a href="https://blog.torproject.org/blog/tor-browser-501-released" rel="nofollow">stable version</a> of the privacy-preserving browser. Version 5.0.1 fixes a crash bug in the recent 5.0 release that was hindering some users’ attempts to access popular websites like Google Maps and Tumblr. There are no other changes in this release.</p>

<p>Thanks to the new automatic update mechanism in the Tor Browser 5.x series, you are probably already running the upgraded version! If not, head to the <a href="https://www.torproject.org/download/download-easy.html" rel="nofollow">project page</a> to get your copy.</p>

<h1>Tor talks at Chaos Communication Camp 2015</h1>

<p>There was a heavy Tor presence at the recent <a href="https://events.ccc.de/camp/2015/wiki/Main_Page" rel="nofollow">Chaos Communication Camp</a> near Zehdenick, Germany, and as usual there were some Tor-related talks by community members that are now available to watch online. Tor and Debian developer Lunar, one of the minds behind Debian’s pioneering and highly successful <a href="https://wiki.debian.org/ReproducibleBuilds" rel="nofollow">reproducible builds project</a> (itself inspired by the <a href="https://blog.torproject.org/blog/deterministic-builds-part-one-cyberwar-and-global-compromise" rel="nofollow">Tor Browser team’s work in this line</a>) gave a talk entitled “<a href="https://media.ccc.de/browse/conferences/camp2015/camp2015-6657-how_to_make_your_software_build_reproducibly.html" rel="nofollow">How to make your software build reproducibly</a>”.</p>

<p>Tor Project Director of Communications Kate Krauss, meanwhile, participated in a talk entitled “<a href="https://media.ccc.de/browse/conferences/camp2015/camp2015-6900-what_s_the_catch.html" rel="nofollow">What’s the catch?</a>”, addressing the subject of free software projects receiving funding from State organizations, and the ways in which this does or does not affect the work of these projects.</p>

<p>Tor developers also participated in the “<a href="https://events.ccc.de/camp/2015/wiki/Session:Tor_Services_using_GNS" rel="nofollow">Tor Services using GNS</a>” session of the Youbroketheinternet village. The session was about Tor using GNS as its name resolution system, and about various ways that we could integrate GNUNet and other anonymity systems with Tor. It was decided that the discussion will continue on the tor-dev mailing list.</p>

<h1>Happy sixth birthday, Tails!</h1>

<p>In the small hours of Sunday night, the Tails project turned <a href="https://mailman.boum.org/pipermail/tails-project/2015-August/000297.html" rel="nofollow">six years old</a>. It may still have most of its milk teeth, but the anonymous live operating system is already the security tool of choice for a wide range of users. It has been endorsed by Reporters Without Borders, groups campaigning against domestic violence, and the team behind the Academy Award-winning documentary CITIZENFOUR (among many others), as Voice of America <a href="http://www.voanews.com/content/digital-solution-helps-shield-online-activists/2873529.html" rel="nofollow">reported last month</a>.</p>

<p>The Tails team has laid out its vision for the next two years in its <a href="https://tails.boum.org/blueprint/roadmap_2016-2017/" rel="nofollow">draft 2016-2017 roadmap</a>, and you can read a summary of its current activities in the last <a href="https://tails.boum.org/news/report_2015_07/" rel="nofollow">monthly report</a>. Congratulations to the team on reaching this anniversary!</p>

<h1>Miscellaneous news</h1>

<p>Hot on the heels of last week’s 2.4 release, Karsten Loesing put out <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009291.html" rel="nofollow">version 2.5</a> of Onionoo, the Tor network data observatory. This release adds a new optional field named “measured” to Onionoo’s details documents. “The main idea behind this new field is that relay operators and Tor network debuggers can now figure out easily whether a relay is affected by not being measured by a sufficient number of bandwidth authorities and as a result has lower usage than it could handle”, writes Karsten. The new field is not yet shown in Onionoo web interfaces like Globe and Atlas, but it is accessible through the Onionoo API. For more details, see the <a href="https://bugs.torproject.org/16020" rel="nofollow">relevant ticket</a>.</p>

<p>David Fifield announced that the recent outage affecting meek’s Microsoft Azure backend is <a href="https://lists.torproject.org/pipermail/tor-talk/2015-August/038780.html" rel="nofollow">now resolved</a>. Most users will have switched to the workaround version included in the most recent Tor Browser releases, but if for some reason you are still using the old configuration, it too should now be working once again.</p>

<p>David Stainton asked for brief code review of his <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009260.html" rel="nofollow">Twisted-based Tor HTTP proxy</a>. “Is this project worthy of your precious 10 minutes to review it... so I can improve the code quality?”</p>

<p>This issue of Tor Weekly News has been assembled by Harmony, Karsten Loesing, and George Kadianakis.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

