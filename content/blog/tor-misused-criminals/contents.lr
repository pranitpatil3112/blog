title: Tor misused by criminals
---
pub_date: 2014-10-21
---
author: phobos
---
tags:

tor
cryptolocker
ransomware
criminals
warning
help
---
categories: network
---
_html_body:

<h1>Tor misused by criminals</h1>

<p>Several people contacted The Tor Project recently because some software told them to install the Tor Browser to access a website. There is no affiliation between the criminals who wrote this software and Tor.</p>

<h1>What happened here?</h1>

<p>The computer is probably infected with what's called <a href="https://en.wikipedia.org/wiki/Ransomware" rel="nofollow">ransomware</a>. This is a kind of malicious software which restricts access to the files and demands a ransom. In this case the authors of the ransomware <a href="https://en.wikipedia.org/wiki/CryptoLocker" rel="nofollow">CryptoLocker</a> set up a website which is only reachable by using Tor. That is why people are thinking that the software is somehow related to The Tor Project.<br />
In fact, CryptoLocker is unrelated to The Tor Project. We didn't produce it, and we didn't ask to be included in the criminal infection of any computer. We cannot help you with your infection. However, according to the <a href="http://www.bbc.com/news/technology-28661463" rel="nofollow">BBC</a> you may be able to decrypt your files for free. If not, <a href="http://www.bleepingcomputer.com/virus-removal/cryptolocker-ransomware-information" rel="nofollow">Bleeping Computer</a> can provide more information.<br />
We, the people of Tor, are very sorry to hear that some individual misused the anonymity granted by our service. The vast majority of our users use Tor in a responsible way. Thank you for your understanding.</p>

---
_comments:

<a id="comment-77397"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77397" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2014</p>
    </div>
    <a href="#comment-77397">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77397" class="permalink" rel="bookmark">Don&#039;t go on the deep web I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Don't go on the deep web I guess :p</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77652"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77652" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77397" class="permalink" rel="bookmark">Don&#039;t go on the deep web I</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77652">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77652" class="permalink" rel="bookmark">The deep web is protecting</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The deep web is protecting free speech, freedom of thought, and civil rights. The deep web protects whistle blowers from getting murdered, and protecting their audience from censorship and ignorance. The deep web is a symbol of freedom and human rights. The criminals who misuse the deep web do nothing to harm the reputation of the deep web, contrary to the Stasi's dreams and hopes, contrary to dictators' aims and goals, contrary to criminals' intentions, and the deep web rocks on with freedom and safety.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-77412"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77412" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 24, 2014</p>
    </div>
    <a href="#comment-77412">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77412" class="permalink" rel="bookmark">The part that upsets me most</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The part that upsets me most here is that these jerks don't need Tor in order to be jerks. They could have put their "give us bitcoins" website in Malaysia or Russia or Panama, and it would have been impossible in practice for the authorities to shut down.</p>
<p>So by dragging Tor into it, they're making the "first contact" with Tor be a really miserable experience for their victims. :(</p>
<p>In short, this is another instance of "the bad guys are doing great on the Internet, and the good guys have very few options."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77484"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77484" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-77484">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77484" class="permalink" rel="bookmark">Can you explain why are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you explain why are those countries particularly safe for hosting clearnet websites?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77493"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77493" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 25, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77484" class="permalink" rel="bookmark">Can you explain why are</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77493">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77493" class="permalink" rel="bookmark">There are many more</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are many more countries like those -- I don't want you to misunderstand and conclude that there are three "safe" countries to go to.</p>
<p>As background, see the "Farmer's Market" discussion at<br />
<a href="https://blog.torproject.org/blog/trip-report-october-fbi-conference" rel="nofollow">https://blog.torproject.org/blog/trip-report-october-fbi-conference</a><br />
"I should still note that Tor doesn't introduce any magic new silver bullet that causes criminals to be uncatchable when before they weren't. The Farmer's Market people ran their webserver in some other foreign country before they switched to a Tor hidden service, and just the fact that the country didn't want to cooperate in busting them was enough to make that a dead end. Jurisdictional arbitrage is alive and well in the world."</p>
<p>I guess the next reading would be<br />
<a href="http://en.wikipedia.org/wiki/Russian_Business_Network" rel="nofollow">http://en.wikipedia.org/wiki/Russian_Business_Network</a></p>
<p>We, the Internet, are doing really poorly at keeping organized crime from doing whatever they want on the Internet. And that asymmetry a) makes Tor even more important, because right now the bad guys have lots of options and the good guys have very few options, and b) makes it even more sad when people focus on trying to get rid of Tor and think that will solve their other problems too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77504" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-77504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77504" class="permalink" rel="bookmark">It&#039;s useless to know the IP</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's useless to know the IP address of the service if the host country doesn't cooperate, of course. I just thought no country (probably except for China and NK) would refuse to cooperate with the FBI.</p>
<p>I also didn't know something illegal such as the RBN operates openly. I now understand why I was told to not visit .ru domains.</p>
<p>Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77567"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77567" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77504" class="permalink" rel="bookmark">It&#039;s useless to know the IP</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77567">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77567" class="permalink" rel="bookmark">legal and illegal should be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>legal and illegal should be used in context as in "illegal in usa". as in usa: it's legal for nsa to break into you house and say 'neighbours see you use a... bitcoins'. ( 7bln people on earth vs 300mln in usa )</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-77469"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77469" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2014</p>
    </div>
    <a href="#comment-77469">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77469" class="permalink" rel="bookmark">While speaking of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>While speaking of criminals:<br />
<a href="http://www.leviathansecurity.com/blog/the-case-of-the-modified-binaries/" rel="nofollow">http://www.leviathansecurity.com/blog/the-case-of-the-modified-binaries/</a><br />
claims that ExitNode $8361A794DFA231D863E109FC9EEEF21F4CF09DDD,<br />
Unnamed (Russia), ExitAddress 78.24.222.229 2014-10-22 02:08:01,<br />
modifies binaries in transit.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77475"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77475" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 25, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77469" class="permalink" rel="bookmark">While speaking of</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77475">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77475" class="permalink" rel="bookmark">Yep. Check</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yep. Check out<br />
<a href="https://lists.torproject.org/pipermail/tor-talk/2014-October/035340.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-talk/2014-October/035340.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-77485"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77485" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 25, 2014</p>
    </div>
    <a href="#comment-77485">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77485" class="permalink" rel="bookmark">Well, that is really your</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, that is really your problem. Who told you to install all these un-certified, un-authorized software to begin with? Your are just stupid and gullible. Do not blame Tor and Tor should not apologize. Idiots....</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77492"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77492" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 25, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77485" class="permalink" rel="bookmark">Well, that is really your</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77492">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77492" class="permalink" rel="bookmark">Well, I assume at least in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, I assume at least in some cases it's more complicated than that. A lot of infections happen these days from visiting the wrong website with your unpatched Internet Explorer. And "the wrong website" can vary by time, including e.g. the superbowl website.</p>
<p>So indeed you can call the average web user stupid and gullible, but that doesn't really resolve the problem, and depending on your audience it probably won't help to make it better.</p>
<p>As for whether Tor should apologize, I agree that we have nothing to apologize for. But we are still sad to see ordinary people get attacked by organized crime, and we are unhappy that the organized crime has decided to drag Tor's name into it. And as a final point, we'd like everybody to notice that this activity isn't "made possible" by Tor -- these criminals are doing just fine at being criminals when they're not using Tor too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77568"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77568" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-77568">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77568" class="permalink" rel="bookmark">well what is the problem for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>well what is the problem for nsa criminals  to inject such messages? 'organised crime' as google? criminals - are they defined by court?<br />
who profits from such PR action?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-77628"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77628" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-77628">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77628" class="permalink" rel="bookmark">It is reasonable to say that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It is reasonable to say that every organized crime unit uses fraud, deception, and the likes. Therefore, if Tor is mis-used by criminals, it is a foreseeable act, just like how Tor was mis-used by teh Silk Road organizers to hide their system of trade and products. You lose, I win.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-77498"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77498" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2014</p>
    </div>
    <a href="#comment-77498">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77498" class="permalink" rel="bookmark">So fsb doesnt control</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>So fsb doesnt control servers at russia?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-77508"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77508" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2014</p>
    </div>
    <a href="#comment-77508">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77508" class="permalink" rel="bookmark">It may be goo to mention</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It may be goo to mention that Classic Theme Restorer has been mentioned to change the window size by one pixel...in terms of anonymity set vs. TB with the crappy new UI.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77511"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77511" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 26, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77508" class="permalink" rel="bookmark">It may be goo to mention</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77511">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77511" class="permalink" rel="bookmark">I think you&#039;re in the wrong</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think you're in the wrong blog post here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-77509"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77509" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2014</p>
    </div>
    <a href="#comment-77509">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77509" class="permalink" rel="bookmark">I personally agree with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I personally agree with arma. The malaysian government is very aware of the jerk.<br />
Particularly they always afraid of someone who tried to condemn them online. I got so lucky to know Tor and other underground stuff, The most jerk in our country is actually the ruled government political party. They always been jerk for almost 50 years. They misused the power given by our peoples. It happen again and again. I really hate this kind of political people. </p>
<p>Bitcoins are not allowed in Malaysia. They dont even have single website for Bitcoins transaction especially buy and pay transaction online.</p>
<p>Freedom to Malaysia.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77570"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77570" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77509" class="permalink" rel="bookmark">I personally agree with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77570">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77570" class="permalink" rel="bookmark">if &#039;power given by our</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>if 'power given by our peoples' comes to some group it makes them lawmakers. so legal and illegal is as they said so.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-77575"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77575" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2014</p>
    </div>
    <a href="#comment-77575">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77575" class="permalink" rel="bookmark">Whoa! I see &quot;Please install</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Whoa! I see "Please install Tor Browser. We are Criminals(TM)"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77578"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77578" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 27, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77575" class="permalink" rel="bookmark">Whoa! I see &quot;Please install</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77578">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77578" class="permalink" rel="bookmark">See it where?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>See it where?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77641"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77641" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-77641">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77641" class="permalink" rel="bookmark">Right here.
&quot;Several people</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Right here.<br />
"Several people contacted The Tor Project recently because some software told them to install the Tor Browser to access a website. There is no affiliation between these <strong>criminals</strong> and Tor."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77644"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77644" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77641" class="permalink" rel="bookmark">Right here.
&quot;Several people</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77644">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77644" class="permalink" rel="bookmark">Ha. Yes indeed, that was</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ha. Yes indeed, that was kind of a muddy sentence. I've unmuddied it a bit. Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-77682"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77682" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77575" class="permalink" rel="bookmark">Whoa! I see &quot;Please install</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77682">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77682" class="permalink" rel="bookmark">well, tor is illegal in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>well, tor is illegal in china so tor developers are criminals ...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77907"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77907" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 31, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-77682" class="permalink" rel="bookmark">well, tor is illegal in</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77907">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77907" class="permalink" rel="bookmark">No, Tor isn&#039;t illegal in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, Tor isn't illegal in China, and no, Tor developers aren't criminals there.</p>
<p>(I don't mean to imply that using Tor in China will be safe in all cases -- but they care about what you're doing on the Internet, not what tools you're using.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-77745"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77745" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 30, 2014</p>
    </div>
    <a href="#comment-77745">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77745" class="permalink" rel="bookmark">Is it possible to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it possible to "get-off-the-grid" and have total security.  To be "cloaked" so to speak?<br />
Can this be done using Tor, VPN, Proxies, whatever new security devices that are out there that are not honey-pots for NSA, Feds etc. I am buying an apple lap top and will not connect to internet until I can find out what I can do to totally protect myself. I am not a criminal, terrorist, or Fed Agency, just want to set up like "Neo" if it is possible. No joke, just want to see what I can do to completely protect myself and others if possible.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-78184"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-78184" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 04, 2014</p>
    </div>
    <a href="#comment-78184">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-78184" class="permalink" rel="bookmark">Please any body can help me</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please any body can help me </p>
<p>I am using Tor Browser 4 in my work and McAfee antivirus see that    tor.exe    is a virus and delete it  </p>
<p>when I change the name of  Tor  to any name the McAfee antivirus doesn't delet it but when I open Start Tor Browser a message appears to me :</p>
<p>unable to start Tor.<br />
The Tor executable is missing.</p>
<p>I haven't any access on McAfee antivirus<br />
So what should I do else If I change the name of   Tor.exe  to (for example browser.exe) ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-78320"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-78320" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2014</p>
    </div>
    <a href="#comment-78320">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-78320" class="permalink" rel="bookmark">No comments on the announced</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No comments on the announced takedown today of more than 400 Dark Web hidden service sites? </p>
<p>SilkRoad 2.0 seems to have been the main target.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-78354"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-78354" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">November 09, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-78320" class="permalink" rel="bookmark">No comments on the announced</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-78354">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-78354" class="permalink" rel="bookmark">https://blog.torproject.org/b</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://blog.torproject.org/blog/thoughts-and-concerns-about-operation-onymous" rel="nofollow">https://blog.torproject.org/blog/thoughts-and-concerns-about-operation-…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
