title: Wrapping up Run a Tor bridge campaign
---
author: ggus
---
pub_date: 2022-02-02
---
categories: community
---
summary:
When we started the campaign on November 17, 2021, the Tor network had approximately 1,200 running bridges. The Tor network now has 2470 running bridges, i.e., the number of Tor bridges has almost doubled!

---
body:

Today, we're wrapping up the "Run a Tor bridge" campaign. If you missed our previous blog post, the Tor Project launched [a campaign to get more bridges last November](https://blog.torproject.org/run-a-bridge-campaign/). The campaign goal was to increase the Tor network size and get 200 new obfs4 bridges. Since then, we have seen new bridges joining the network every day.

Here is the good news: not only did we achieve our modest goal, but we also reverted the trend of declining bridges in the network! The Tor network now has 2470 running bridges, i.e., the number of Tor bridges has almost doubled! Over the past two months, we have been helping new bridge operators to set up their bridges, updating our documentation, and testing new bridges.

Just a quick recap: when we started the campaign on November 17, 2021, the Tor network had approximately 1,200 running bridges, which wasn't terribly bad. For example, in 2019, we had only 950 running bridges. But, as we saw a slight decreasing trend, we decided to act. 

![Network size graph](networksize.png)

The campaign has been such a success for a couple of reasons. First, if you check the [number of relays](https://metrics.torproject.org/networksize.csv?start=2021-10-30&end=2022-01-28), you can see that in 10 days after the campaign began, we had already achieved the goal of 200 new bridges. The community was really engaged and very excited to help other Tor users. Plus, it's nice to get Tor swag. As a Tor friend said during a meetup, "I hate shopping for clothes" is a very valid motivation. And, well, I can't agree more.

Second, following the relay metrics graph, you can see that after December 7th, the bridges graph increased considerably. This spike happened after we called everyone to fight against the emergent [Tor censorship in Russia](https://blog.torproject.org/tor-censorship-in-russia/). Russia has the second largest number of Tor users, just after the [United States](https://metrics.torproject.org/userstats-relay-table.html). As censors are blocking Tor bridges, adding new bridges will help users connect to the Tor network. It's a [censorship arms race](https://www.youtube.com/watch?v=ZB8ODpw_om8).

![Bridge usage in Russia](bridgesrussia.png)

*Note: In the [bridge users graph](https://metrics.torproject.org/userstats-bridge-country.html?start=2021-11-04&end=2022-02-02&country=ru) from Russia, you can see how spinning up more Tor bridges is helping get users connected.*

It's important to note that we observed some bridges that went offline in the middle of the last year, rejoining the network after our call. To put it another way, the Tor censorship in Russia not only engaged new operators, but also re-engaged former operators.

### Wrapping up the campaign, Tor censorship in Russia and next steps

We're working on the logistics to send your swag kits. If you spun up bridges during the campaign period and didn't get in touch, please email us at frontdesk at torproject.org as soon as possible. If you already did that, we'll contact you this week to get your postal details.

Although we're wrapping up the campaign, the Tor censorship in Russia is still ongoing - and it's also happening in other countries. While our censors are busy trying to find and block bridges, the Tor Anti-censorship and Community Teams have done a lot to fight back. We've set up a Telegram bot to automatically distribute bridges, engaged volunteers to run more bridges, fixed and unblocked Snowflake in Russia, supported more than 2500 users from Russia to connect to Tor, helped people in [Kazakhstan to circumvent censorship](https://forum.torproject.net/t/internet-shutdown-in-kazakhstan-how-to-circumvent-censorship-with-tor/1679), and translated our support articles in Russian.

The 'Run a Tor Bridge' campaign was an amazing effort and, at the same time, a powerful demonstration of the strength of Tor and the internet freedom community. Fighting against internet censorship and surveillance is part of the Tor Project mission, and we're glad that you've joined us!

Since you're here, don't forget to join our next Tor Relay operator meetup next Saturday, February 5th. You can [find more details here](https://forum.torproject.net/t/tor-relays-relay-operators-meetup-fosdem/1824).
