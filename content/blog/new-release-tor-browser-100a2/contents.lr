title: New Release: Tor Browser 10.0a2
---
pub_date: 2020-06-30
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.0
---
categories: applications
---
_html_body:

<p>Tor Browser 10.0a2 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.0a2/">distribution directory.</a></p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-951">latest stable release</a> instead.</p>
<p>This release updates Firefox to 68.10.0esr, Tor to 0.4.4.1-alpha, and NoScript 11.0.32.</p>
<p>This release also includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-25/">security updates</a> to Firefox.</p>
<p>The MacOS installer (.dmg) is now code signed using a different procedure. Please report any issues you encounter with this version.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">changelog</a> since Tor Browser 10.0a1 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 68.10.0esr</li>
<li>Update NoScript to 11.0.32</li>
<li>Update Tor to 0.4.4.1-alpha</li>
<li>Translations update</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34209">Bug 34209</a>: about:tor and about:tbupdate fail to load in debug build</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34250">Bug 34250</a>: Only listen to 'started' in noscript-control.js</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34383">Bug 34383</a>: Accept request if GetHost() in mixed content blocking check fails</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34361">Bug 34361:</a> "Prioritize .onion sites when known" appears under General</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34362">Bug 34362:</a> Improve Onion Service Authentication prompt</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34369">Bug 34369:</a> Fix learn more link in Onion Auth prompt</li>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34379">Bug 34379:</a> Fix learn more for Onion-Location</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/34372">Bug 34372</a>: Disable GeckoNetworkManager</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-288431"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288431" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 01, 2020</p>
    </div>
    <a href="#comment-288431">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288431" class="permalink" rel="bookmark">Why RFP uses 32bit version…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why RFP uses 32bit version useragent for http UA header?<br />
How about adding " Win64; x64;" ?</p>
<p>Mozilla/5.0 (Windows NT 10.0; rv:68.0) Gecko/20100101 Firefox/68.0<br />
↓<br />
Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288454"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288454" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 01, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288431" class="permalink" rel="bookmark">Why RFP uses 32bit version…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288454">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288454" class="permalink" rel="bookmark">Mozilla could add that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mozilla could add that. There is some other inconsistency, too. Usually this is based on the popularity of a platform, so maybe Windows 10 (32-bit) is more popular than 64-bit.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288508"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288508" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Thorin (not verified)</span> said:</p>
      <p class="date-time">July 02, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-288508">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288508" class="permalink" rel="bookmark">It was changed (back) due to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It was changed (back) due to compat fears. Basically all browsers on Windows return Win32: see [1]<br />
[1] <a href="https://www.fxsitecompat.dev/en-CA/docs/2018/navigator-platform-now-returns-win32-even-on-64-bit-firefox/" rel="nofollow">https://www.fxsitecompat.dev/en-CA/docs/2018/navigator-platform-now-ret…</a></p>
<p>Some tickets<br />
- <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1383495" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=1383495</a> - RFP spoofed as 64bit<br />
- <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1472618" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=1472618</a> - changed FF63/ESR68</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288556"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288556" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>x86_64 (not verified)</span> said:</p>
      <p class="date-time">July 05, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-288556">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288556" class="permalink" rel="bookmark">64-bit machines seem more…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>64-bit machines seem more popular than 32-bit.<br />
<a href="https://data.firefox.com/dashboard/hardware" rel="nofollow">https://data.firefox.com/dashboard/hardware</a><br />
64-bit: 75%<br />
32-bit: 25%</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-288432"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288432" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 01, 2020</p>
    </div>
    <a href="#comment-288432">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288432" class="permalink" rel="bookmark">In at least the latest…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In at least the latest stable release, no .onion alternative is offered in the address bar for the Tor Blog. I'm not even sure how to file a bug report because Tor Track is apparently being discontinued, and GitLab requires an account and is not allowing signup.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288455"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288455" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 01, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288432" class="permalink" rel="bookmark">In at least the latest…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-288455">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288455" class="permalink" rel="bookmark">This is a known issue…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is a known issue because the blog doesn't have an onion address. <a href="https://gitlab.torproject.org/tpo/web/blog-trac/-/issues/22397" rel="nofollow">https://gitlab.torproject.org/tpo/web/blog-trac/-/issues/22397</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288471"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288471" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 02, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to sysrqb</p>
    <a href="#comment-288471">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288471" class="permalink" rel="bookmark">RIP cypherpunks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>RIP cypherpunks</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-288526"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288526" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Geo (not verified)</span> said:</p>
      <p class="date-time">July 03, 2020</p>
    </div>
    <a href="#comment-288526">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288526" class="permalink" rel="bookmark">Well i installed Tor and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well i installed Tor and mine is still crashing after installing the latest version i am still experiencing Crash TAB. I tried disabling Javascript nothing is working. I am kinda felling frustrated as no topic on this subject. I will appreciate a little guide to resolve this problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-288540"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288540" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  sysrqb
  </article>
    <div class="comment-header">
      <p class="comment__submitted">sysrqb said:</p>
      <p class="date-time">July 04, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-288526" class="permalink" rel="bookmark">Well i installed Tor and…</a> by <span>Geo (not verified)</span></p>
    <a href="#comment-288540">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288540" class="permalink" rel="bookmark">Can you provide any context…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you provide any context or information about your computer? Is your computer running Windows or macOS X or Linux? Does Firefox work without any crashing? Do you receive any informative error messages?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
