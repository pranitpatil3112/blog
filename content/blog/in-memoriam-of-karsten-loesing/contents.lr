title: In memoriam of Karsten Loesing
---
pub_date: 2020-12-23
---
author: isabela
---
summary: It's with deep sorrow that we share that our dear friend, colleague, and Tor core contributor Karsten Loesing passed away on the afternoon of Friday, December 18, 2020. No one is prepared for such an unimaginable loss. Our deepest sympathies go to Karsten's family at this moment, his wife and his children.
---
_html_body:

<p>It's with deep sorrow that we share that our dear friend, colleague, and Tor core contributor Karsten Loesing passed away on the afternoon of Friday, December 18, 2020. No one is prepared for such an unimaginable loss. Our deepest sympathies go to Karsten's family at this moment, his wife and his children.</p>
<p>Karsten was part of the Tor community for 13 years and an amazing, smart, thoughtful, and gentle person who has touched us all. Over the course of these years we saw him not only grow as a colleague at Tor but as a father to his family. His positive, attentive, and kind presence helped us grow as people as well.</p>
<p>Dr. Karsten Loesing <a href="https://archives.seul.org/or/talk/Apr-2007/msg00037.html">joined Tor in 2007 as a Google Summer of Code student</a> to work on Distributed Tor Directory, and earned his PhD in Computer Science at Germany’s University of Bamberg in 2009 on a Tor-related topic, "<a href="https://www.uni-bamberg.de/fileadmin/uni/fakultaeten/wiai_lehrstuehle/praktische_informatik/Dateien/Forschung/Tor/loesing-distributed-storage.pdf">Distributed Storage for Tor Hidden Service Descriptors</a>".</p>
<p>Since 2009, Karsten was lead developer of all Java-based code in <a href="https://metrics.torproject.org">Tor metrics</a> and became Metrics team lead in 2015. He was the original author of Tor Metrics and was one of the main authors of OnionPerf. You can <a href="https://www.youtube.com/watch?v=fh3IoGsyN68">watch a demonstration he did about OnionPerf</a> at one of our recent demo days at Tor.</p>
<p>We all loved him and his contribution to the Tor Project will always be remembered from the depth of our hearts. We will be dedicating our next release of core tor to Karsten's memory. </p>
<p>Rest in peace, Karsten.</p>
<p><em>Image credit: uni-bamberg.de press office</em>, <a href="https://www.uni-bamberg.de/forschung/profil/kulturgut/dossier/artikel/kein-haeuten-der-zwiebel/">Kein Häuten der Zwiebel</a> (2007).</p>

