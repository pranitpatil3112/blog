title: Tor Weekly News — September 3rd, 2014
---
pub_date: 2014-09-03
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirty-fifth issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Tor Browser 3.6.5 and 4.0-alpha-2 are out</h1>

<p>The Tor Browser team put out two new releases of the privacy-preserving web browser. Among the major changes, version 3.6.5 upgrades Firefox to 24.8.0esr, and includes an improved prompt to help users defend against <a href="https://lists.torproject.org/pipermail/tor-talk/2014-July/033969.html" rel="nofollow">HTML5 canvas image fingerprinting</a>, following a <a href="https://bugs.torproject.org/12684" rel="nofollow">patch</a> by Isis Lovecruft. Version 4.0-alpha-2 additionally includes the code for the forthcoming Tor Browser auto-updater (switched off by default) and <a href="https://lists.torproject.org/pipermail/tor-qa/2014-September/000458.html" rel="nofollow">“better hardening for Windows and Linux builds”</a>.</p>

<p>As ever, you can download the new releases along with their signature files from the Tor Project’s <a href="https://www.torproject.org/dist/torbrowser/" rel="nofollow">distribution directory</a>. Please upgrade as soon as you can.</p>

<h1>Tails 1.1.1 is out</h1>

<p>The Tails team <a href="https://tails.boum.org/news/version_1.1.1/" rel="nofollow">released</a> version 1.1.1 of the Debian- and Tor-based live operating system. As well as upgrading key components like Tor, Iceweasel, and Linux, this release disables I2P by default when Tails is booted, in response to the <a href="https://tails.boum.org/security/Security_hole_in_I2P_0.9.13/" rel="nofollow">vulnerability recently disclosed by Exodus Intelligence</a>. Like Truecrypt, “i2p” must now be specified as a parameter on booting by users who wish to use it.</p>

<p>A number of other security fixes and routine improvements make this an important update for all Tails users. See the full changelog in the team’s announcement, then update from a running copy of Tails 1.1 if you have one, or head to the <a href="https://tails.boum.org/download/" rel="nofollow">download page</a> if you don’t.</p>

<h1>Helping Internet services accept anonymous users</h1>

<p>Without a large and diverse network, run by thousands of dedicated volunteers, Tor would be nowhere near as useful or popular as it currently is. Although the current situation might at times seem fragile, there are still many places where it is feasible to host Tor exit nodes.</p>

<p>However, Tor would become much less attractive to users if they found themselves unable to reach or interact with their favorite websites while using it, a situation that is unfortunately growing more common as site administrators and engineers react negatively to instances of abusive Tor traffic by banning anonymous connections outright. Tor users and developers, as well as members of other online communities (such as <a href="https://meta.wikimedia.org/wiki/Grants:IdeaLab/Partnership_between_Wikimedia_community_and_Tor_community" rel="nofollow">Wikimedia</a>), have tried to address the issue before, but real progress has yet to be made.</p>

<p>Roger Dingledine wrote a <a href="https://blog.torproject.org/blog/call-arms-helping-internet-services-accept-anonymous-users" rel="nofollow">“call to arms”</a> explaining the problem in detail and exploring possible paths to a solution: “Step one is to enumerate the set of websites and other Internet services that handle Tor connections differently from normal connections […]. Step two is to sort the problem websites based on how amenable they would be to our help”.</p>

<p>Since the problem involves humans as much as it does machines, anyone working on it will have to be both “technical” but also ”good at activism”. If you fit that description, OTF has expressed interest in funding work on this issue through their <a href="https://www.opentechfund.org/labs/fellowships" rel="nofollow">Information Controls Fellowship Program</a>. Please read Roger’s blog post in full for more details.</p>

<h1>Monthly status reports for August 2014</h1>

<p>The wave of regular monthly reports from Tor project members for the month of August has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2014-August/000626.html" rel="nofollow">Damian Johnson</a> released his report first, followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2014-August/000627.html" rel="nofollow">Georg Koppen</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000628.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000629.html" rel="nofollow">Noel Torres</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000630.html" rel="nofollow">Kevin P Dyer</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000633.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000635.html" rel="nofollow">Lunar</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000636.html" rel="nofollow">Arthur D. Edelstein</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000637.html" rel="nofollow">Karsten Loesing</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000638.html" rel="nofollow">Andrew Lewman</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000639.html" rel="nofollow">Arlo Breault</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000640.html" rel="nofollow">Pearl Crescent</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000641.html" rel="nofollow">Michael Schloh von Bennewitz</a>.</p>

<p>Lunar also reported on behalf of the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000634.html" rel="nofollow">help desk</a>, and Mike Perry did the same for the <a href="https://lists.torproject.org/pipermail/tor-reports/2014-September/000642.html" rel="nofollow">Tor Browser team</a>.</p>

<h1>Miscellaneous news</h1>

<p>Yawning Angel <a href="https://lists.torproject.org/pipermail/tor-dev/2014-August/007420.html" rel="nofollow">released</a> a new set of experimental Tor Browser builds containing the proposed obfs4 pluggable transport, along with a changelog; “questions, comments, feedback” are welcome on the email thread or the <a href="https://bugs.torproject.org/12130" rel="nofollow">bug ticket</a> tracking the deployment of obfs4.</p>

<p>Arturo Filastò <a href="https://lists.torproject.org/pipermail/tor-dev/2014-September/007450.html" rel="nofollow">announced</a> the release of version 1.1.0 of oonibackend, the tool “<a href="https://pypi.python.org/pypi/oonibackend" rel="nofollow">used by ooniprobe</a> to discover the addresses of test helpers (via the bouncer) to submit reports to (via the collector) and to perform some measurements that require a backend system to talk to (via test helpers)”.</p>

<p>meejah <a href="https://lists.torproject.org/pipermail/tor-dev/2014-August/007426.html" rel="nofollow">posted</a> a list of tasks to be completed in order to bring Tor Weather to a deployable state, following the recent rewrite effort and the Google Summer of Code project by Sreenatha Bhatlapenumarthi.</p>

<p>Israel Leiva submitted a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-August/007427.html" rel="nofollow">summary</a> of work completed as part of the “Revamp GetTor” Google Summer of Code project: “The plan for now is to keep doing tests and deploy it asap (hopefully during September).”</p>

<p>Mike Perry <a href="https://lists.torproject.org/pipermail/tor-dev/2014-August/007417.html" rel="nofollow">posted</a> an <a href="https://gitweb.torproject.org/user/mikeperry/torspec.git/blob/refs/heads/multihop-padding-primitives:/proposals/ideas/xxx-multihop-padding-primitives.txt" rel="nofollow">updated version</a> of the proposal for website fingerprinting countermeasures which he co-authored with Marc Juarez as part of the latter’s Google Summer of Code project.</p>

<p>Lunar gave a <a href="http://meetings-archive.debian.net/pub/debian-meetings/2014/debconf14/webm/Reproducible_Builds_for_Debian_a_year_later.webm" rel="nofollow">talk</a> at this year’s DebConf on the effort to build Debian packages deterministically, which is inspired in large part by <a href="https://blog.torproject.org/blog/deterministic-builds-part-one-cyberwar-and-global-compromise" rel="nofollow">Tor Browser’s use of the same technology</a>. <a href="http://lists.alioth.debian.org/pipermail/reproducible-builds/Week-of-Mon-20140901/000198.html" rel="nofollow">Major progress</a> was achieved during the conference.</p>

<p>David Fifield submitted a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-August/007429.html" rel="nofollow">breakdown</a> of the costs incurred by the infrastructure that supports the <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">meek</a> pluggable transport since its introduction. The total to date from both the Google App Engine and Amazon AWS front domains? $6.56.</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-August/000653.html" rel="nofollow">P D</a> and <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-August/000673.html" rel="nofollow">Daniel Pajonzeck</a> for running mirrors of the Tor Project website and software!</p>

<p>Also on the subject of mirrors, Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-mirrors/2014-September/000675.html" rel="nofollow">alerted</a> the tor-mirrors mailing list to the fact that the Tor Project website (specifically the distribution directory) will shortly be increasing in size to eight or nine gigabytes, as a result of the soon-to-be-implemented <a href="https://bugs.torproject.org/4234" rel="nofollow">Tor Browser updater</a>. Mirror operators will need to ensure that they can provide enough disk space to accommodate the change.</p>

<p>whonixqubes <a href="https://lists.torproject.org/pipermail/tor-talk/2014-August/034562.html" rel="nofollow">announced</a> the release of an integrated version of the Whonix and Qubes operating systems: “I look forward to helping make Qubes + Whonix integration even tighter and more seamless throughout the future.”</p>

<h1>Tor help desk roundup</h1>

<p>The help desk has been asked if Tor can make a website visit appear to come from China. Tor connections appear to originate from the country where the exit relay in use is located; since Tor is blocked in China, there are zero exit relays in China. A visualization of the different country-locations of exit relays can be found on Tor’s <a href="https://metrics.torproject.org/bubbles.html#country-exits-only" rel="nofollow">metrics page</a>.</p>

<h1>News from Tor StackExchange</h1>

<p>Anony Mouse wanted to know why Facebook shows the location of the user’s last login over Tor as <a href="https://tor.stackexchange.com/q/3364/88" rel="nofollow">Baghdad or Dhaka</a>, instead of the real location of the exit relay. qbi posted a <a href="https://twitter.com/qbi/status/506550322308055040" rel="nofollow">screenshot</a> showing this issue. According to Facebook, this information is based on an approximation, but this approximation locates all Tor exit relays either in Baghdad or in Dhaka.</p>

<p>user3500 <a href="https://tor.stackexchange.com/q/3961/88" rel="nofollow">wants to contribute to Tor</a> and asks how this can be done as an inexperienced developer. Jens Kubieziel replied with several possibilities, including reading the volunteer page and Tor Weekly News: in particular, the section containing easy development tasks might be a good start. Roya pointed out that any contribution is better than no contribution, and encouraged user3500 to just get started. Umut Seven recommended writing unit tests.</p>

<p>Kras wants to use FoxyProxy in connection with Tor Browser Bundle and <a href="https://tor.stackexchange.com/q/3239/88" rel="nofollow">asks if it is safe to do so</a>. At the moment, there is only an answer saying “yes”, without any explanation. What is your experience? Is it safe for a user to install and use FoxyProxy?</p>

<p>This issue of Tor Weekly News has been assembled by harmony, Matt Pagan, Lunar, qbi, and Arlo Breault.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

