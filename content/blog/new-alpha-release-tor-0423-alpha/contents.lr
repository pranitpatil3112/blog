title: New Alpha Release: Tor 0.4.2.3-alpha
---
pub_date: 2019-10-24
---
author: nickm
---
tags: alpha release
---
categories: releases
---
summary: Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.2.3-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release in a couple of weeks.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>This release fixes several bugs from the previous alpha release, and from earlier versions of Tor.</p>
<h2>Changes in version 0.4.2.3-alpha - 2019-10-24</h2>
<ul>
<li>Major bugfixes (relay):
<ul>
<li>Relays now respect their AccountingMax bandwidth again. When relays entered "soft" hibernation (which typically starts when we've hit 90% of our AccountingMax), we had stopped checking whether we should enter hard hibernation. Soft hibernation refuses new connections and new circuits, but the existing circuits can continue, meaning that relays could have exceeded their configured AccountingMax. Fixes bug <a href="https://bugs.torproject.org/32108">32108</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Major bugfixes (v3 onion services):
<ul>
<li>Onion services now always use the exact number of intro points configured with the HiddenServiceNumIntroductionPoints option (or fewer if nodes are excluded). Before, a service could sometimes pick more intro points than configured. Fixes bug <a href="https://bugs.torproject.org/31548">31548</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor feature (onion services, control port):
<ul>
<li>The ADD_ONION command's keyword "BEST" now defaults to ED25519-V3 (v3) onion services. Previously it defaulted to RSA1024 (v2). Closes ticket <a href="https://bugs.torproject.org/29669">29669</a>.</li>
</ul>
</li>
<li>Minor features (testing):
<ul>
<li>When running tests that attempt to look up hostnames, replace the libc name lookup functions with ones that do not actually touch the network. This way, the tests complete more quickly in the presence of a slow or missing DNS resolver. Closes ticket <a href="https://bugs.torproject.org/31841">31841</a>.</li>
</ul>
</li>
<li>Minor features (testing, continuous integration):
<ul>
<li>Disable all but one Travis CI macOS build, to mitigate slow scheduling of Travis macOS jobs. Closes ticket <a href="https://bugs.torproject.org/32177">32177</a>.</li>
<li>Run the chutney IPv6 networks as part of Travis CI. Closes ticket <a href="https://bugs.torproject.org/30860">30860</a>.</li>
<li>Simplify the Travis CI build matrix, and optimise for build time. Closes ticket <a href="https://bugs.torproject.org/31859">31859</a>.</li>
<li>Use Windows Server 2019 instead of Windows Server 2016 in our Appveyor builds. Closes ticket <a href="https://bugs.torproject.org/32086">32086</a>.</li>
</ul>
</li>
<li>Minor bugfixes (build system):
<ul>
<li>Interpret "--disable-module-dirauth=no" correctly. Fixes bug <a href="https://bugs.torproject.org/32124">32124</a>; bugfix on 0.3.4.1-alpha.</li>
<li>Interpret "--with-tcmalloc=no" correctly. Fixes bug <a href="https://bugs.torproject.org/32124">32124</a>; bugfix on 0.2.0.20-rc.</li>
<li>Stop failing when jemalloc is requested, but tcmalloc is not found. Fixes bug <a href="https://bugs.torproject.org/32124">32124</a>; bugfix on 0.3.5.1-alpha.</li>
<li>When pkg-config is not installed, or a library that depends on pkg-config is not found, tell the user what to do to fix the problem. Fixes bug <a href="https://bugs.torproject.org/31922">31922</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (connections):
<ul>
<li>Avoid trying to read data from closed connections, which can cause needless loops in Libevent and infinite loops in Shadow. Fixes bug <a href="https://bugs.torproject.org/30344">30344</a>; bugfix on 0.1.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (error handling):
<ul>
<li>Always lock the backtrace buffer before it is used. Fixes bug <a href="https://bugs.torproject.org/31734">31734</a>; bugfix on 0.2.5.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (mainloop, periodic events, in-process API):
<ul>
<li>Reset the periodic events' "enabled" flag when Tor is shut down cleanly. Previously, this flag was left on, which caused periodic events not to be re-enabled when Tor was relaunched in-process with tor_api.h after a shutdown. Fixes bug <a href="https://bugs.torproject.org/32058">32058</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (process management):
<ul>
<li>Remove overly strict assertions that triggered when a pluggable transport failed to launch. Fixes bug <a href="https://bugs.torproject.org/31091">31091</a>; bugfix on 0.4.0.1-alpha.</li>
<li>Remove an assertion in the Unix process backend. This assertion would trigger when we failed to find the executable for a child process. Fixes bug <a href="https://bugs.torproject.org/31810">31810</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Avoid intermittent test failures due to a test that had relied on inconsistent timing sources. Fixes bug <a href="https://bugs.torproject.org/31995">31995</a>; bugfix on 0.3.1.3-alpha.</li>
<li>When testing port rebinding, don't busy-wait for tor to log. Instead, actually sleep for a short time before polling again. Also improve the formatting of control commands and log messages. Fixes bug <a href="https://bugs.torproject.org/31837">31837</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (tls, logging):
<ul>
<li>Log bugs about the TLS read buffer's length only once, rather than filling the logs with similar warnings. Fixes bug <a href="https://bugs.torproject.org/31939">31939</a>; bugfix on 0.3.0.4-rc.</li>
</ul>
</li>
<li>Minor bugfixes (v3 onion services):
<ul>
<li>Fix an implicit conversion from ssize_t to size_t discovered by Coverity. Fixes bug <a href="https://bugs.torproject.org/31682">31682</a>; bugfix on 0.4.2.1-alpha.</li>
<li>Fix a memory leak in an unlikely error code path when encoding HS DoS establish intro extension cell. Fixes bug <a href="https://bugs.torproject.org/32063">32063</a>; bugfix on 0.4.2.1-alpha.</li>
<li>When cleaning up intro circuits for a v3 onion service, don't remove circuits that have an established or pending circuit, even if they ran out of retries. This way, we don't remove a circuit on its last retry. Fixes bug <a href="https://bugs.torproject.org/31652">31652</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Correct the description of "GuardLifetime". Fixes bug <a href="https://bugs.torproject.org/31189">31189</a>; bugfix on 0.3.0.1-alpha.</li>
<li>Make clear in the man page, in both the bandwidth section and the AccountingMax section, that Tor counts in powers of two, not powers of ten: 1 GByte is 1024*1024*1024 bytes, not one billion bytes. Resolves ticket <a href="https://bugs.torproject.org/32106">32106</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-284905"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284905" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2019</p>
    </div>
    <a href="#comment-284905">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284905" class="permalink" rel="bookmark">I&#039;ve been using tor with the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've been using tor with the obfs4 bridges. After many months, my entry guard node has recently changed. Looking at the circuits, now I see a new guard consistently being the first node.</p>
<p>However, at the beginning of each tor connection there are also 2 other nodes listed at the top of circuit list, and they disappear shortly. Strangely, I recognize one of them as my former long-term guard last year. The other has only a slightly different name; must be a node from the same family.</p>
<p>Is this normal for the tor nightly master 0.4.3 alpha package? If not, any advice?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285002"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285002" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 25, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284905" class="permalink" rel="bookmark">I&#039;ve been using tor with the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-285002">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285002" class="permalink" rel="bookmark">This is normal for Tor (and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is normal for Tor (and not just the 0.4.2 alphas). Your main guard is called your "entry guard", and the three of them together make up your "directory guards". The directory guards fetch information about the Tor network, but only the entry guard is used for circuits that will carry your application traffic.</p>
<p>In an ideal world you would use your one entry guard as your one directory guard, but it's a balance: the reason you have multiple directory guards is to limit attacks where a single directory guard might choose to not provide you information about certain relays, thus letting it influence the paths your client can make.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285165"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285165" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Same poster (not verified)</span> said:</p>
      <p class="date-time">October 31, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-285165">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285165" class="permalink" rel="bookmark">OK. But then, is it normal…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OK. But then, is it normal that the 2 directory guards are named nearly identically (differing in the end numbers)? Should not they be diversified?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-284907"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284907" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Yeetus (not verified)</span> said:</p>
      <p class="date-time">October 24, 2019</p>
    </div>
    <a href="#comment-284907">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284907" class="permalink" rel="bookmark">Make it so we can turn off…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Make it so we can turn off the letterboxing. More security is nice and all but this looks like trash.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-284926"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284926" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">October 25, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284907" class="permalink" rel="bookmark">Make it so we can turn off…</a> by <span>Yeetus (not verified)</span></p>
    <a href="#comment-284926">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284926" class="permalink" rel="bookmark">You are commenting in the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You are commenting in the wrong blog post. This one is about Tor and not Tor Browser. However, to get back to your request: disabling letterboxing, while not recommended, is of course possible. You need to flip the governing preference, <span class="geshifilter"><code class="php geshifilter-php">privacy<span style="color: #339933;">.</span>resistFingerprinting<span style="color: #339933;">.</span>letterboxing</code></span> , on <span class="geshifilter"><code class="php geshifilter-php">about<span style="color: #339933;">:</span>config</code></span>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-284909"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-284909" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2019</p>
    </div>
    <a href="#comment-284909">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-284909" class="permalink" rel="bookmark">&gt; 1 GByte is 1024*1024*1024…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; 1 GByte is 1024*1024*1024 bytes<br />
<a href="https://en.wikipedia.org/wiki/Gigabyte" rel="nofollow">https://en.wikipedia.org/wiki/Gigabyte</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285003"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285003" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 25, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284909" class="permalink" rel="bookmark">&gt; 1 GByte is 1024*1024*1024…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-285003">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285003" class="permalink" rel="bookmark">Yep. Would we rather try to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yep. Would we rather try to teach people what we mean by gigabyte, or teach people that there's a surprise word they've never heard of called gibibyte, or change Tor's notion of the word and then teach a different set of people what we mean by it? Hard to win. At least now we're making it even clearer what Tor does.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285014"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285014" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-285014">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285014" class="permalink" rel="bookmark">ISO decided and clarified…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ISO decided and clarified that for the whole world. So, it's "gibi" since 2008 (but we still can't manage ourselves to accept it, because it was "giga" for so long :) ).<br />
<a href="https://en.wikipedia.org/wiki/ISO/IEC_80000" rel="nofollow">https://en.wikipedia.org/wiki/ISO/IEC_80000</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285023"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285023" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 26, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-284909" class="permalink" rel="bookmark">&gt; 1 GByte is 1024*1024*1024…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-285023">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285023" class="permalink" rel="bookmark">Those self-appointed people…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Those self-appointed people have no right to dictate what words actually mean and make everybody else in the world use the words they create to replace the old ones.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285061"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285061" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 27, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285023" class="permalink" rel="bookmark">Those self-appointed people…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-285061">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285061" class="permalink" rel="bookmark">Which self-appointed people?…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Which self-appointed people?  Consumers or scientists and engineers?  The prefix <a href="https://en.wikipedia.org/wiki/Giga-" rel="nofollow">giga-</a> exists for all metric units throughout the sciences and has meant the base-10 <a href="https://en.wikipedia.org/wiki/Significand#Example" rel="nofollow">characteristic</a> x10^9 since the 1920s.  As x10^9, it has been an IUPAC international chemistry standard since 1947 or earlier and an SI metric standard since 1960.  Computer drive and network hardware manufacturers and have always expressed capacities in base-10.  Macintosh computers since launching in 1984, base-10.  Microsoft Windows since 1985, base-2.  And that, I think, is when <a href="https://en.wikipedia.org/wiki/Gigabyte#Consumer_confusion" rel="nofollow">customer confusion</a> began.  IEEE 100-1988 in 1988 defined a gigabyte as base-2. Some organizations proposed base-10 adherence and new base-2 prefixes in the 1990s.  IEEE switched from base-2 adherence to base-10 in 1997, and on it goes.</p>
<p>So I blame Microsoft.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-285004"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285004" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>JR (not verified)</span> said:</p>
      <p class="date-time">October 25, 2019</p>
    </div>
    <a href="#comment-285004">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285004" class="permalink" rel="bookmark">Gotta say Tails 4.0 is a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Gotta say Tails 4.0 is a huge step backwards for me.  I have been running Tails/Tor from a 2GB live USB drive for years.  The .img instead of .iso thing was annoying (I dont want to download a 100+MB program just to install a USB image since usb universal installer never works) but I overcame that with Rufus.</p>
<p>But upon boot, I am informed that an *8GB* drive is needed and Tails fails to load.  Needing 4 times the drive to run ??? What gives?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285030"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285030" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">October 26, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285004" class="permalink" rel="bookmark">Gotta say Tails 4.0 is a…</a> by <span>JR (not verified)</span></p>
    <a href="#comment-285030">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285030" class="permalink" rel="bookmark">I know the Tails folks don&#039;t…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I know the Tails folks don't make it easy to provide feedback, but this is a post about the program named Tor. So Tails feedback doesn't make sense on it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285123"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285123" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285004" class="permalink" rel="bookmark">Gotta say Tails 4.0 is a…</a> by <span>JR (not verified)</span></p>
    <a href="#comment-285123">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285123" class="permalink" rel="bookmark">&gt; But upon boot... Tails…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; But upon boot... Tails fails to load [because it wants an 8GB drive]</p>
<p>I don't understand what you mean.  LiveUSBs don't work that way.  If the image wasn't written correctly, the writer should've told you.  If you're trying to install to a hard drive, you're doing it wrong.  If it's asking for a hard drive, <em>it's</em> doing something wrong.  If it meant 8GB of RAM, I might start to understand.  Live-booting images are compressed.  Anyway, ask Tails developers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285105"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285105" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ali (not verified)</span> said:</p>
      <p class="date-time">October 28, 2019</p>
    </div>
    <a href="#comment-285105">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285105" class="permalink" rel="bookmark">I heven&#039;t been able to use…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I heven't been able to use Tor since the last update. I tried re-installing it multiple times now. i keep getting the following error: "The program can't start because api-ms-win-crt-convert-l1-1-0.dll is missing from your computer. Try reinstalling the program to fix this problem."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285122"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285122" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 29, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285105" class="permalink" rel="bookmark">I heven&#039;t been able to use…</a> by <span>Ali (not verified)</span></p>
    <a href="#comment-285122">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285122" class="permalink" rel="bookmark">You&#039;re asking in the wrong…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You're asking in the wrong place.  You are talking about Tor Browser, but this post is about the tor network daemon.  Read the post titles.</p>
<p>Anyway, run Windows Update or manually install the Universal C Runtime (CRT) update:<br />
<a href="https://support.microsoft.com/en-us/help/2999226/update-for-universal-c-runtime-in-windows" rel="nofollow">https://support.microsoft.com/en-us/help/2999226/update-for-universal-c…</a><br />
For specifics of what's inside it such as the dll, click "File information".</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285148"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285148" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 30, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285105" class="permalink" rel="bookmark">I heven&#039;t been able to use…</a> by <span>Ali (not verified)</span></p>
    <a href="#comment-285148">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285148" class="permalink" rel="bookmark">The preferred method to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The preferred method to centrally install the Universal CRT is to use Microsoft Windows Update. The Universal CRT is a Recommended update for all supported Microsoft Windows operating systems, so by default, most machines install it as part of the regular update process. The initial release of the Universal CRT was KB2999226. A later update with various bug fixes was made in KB3118401, and there have been additional updates with further bug fixes and new features. For more recent updates, search support.microsoft.com for Universal C Runtime or Universal CRT.</p>
<p>Not all Microsoft Windows computers regularly install updates by use of Windows Update, and some may not install all Recommended updates. To support the use of applications built by using the Visual Studio 2015 and later C++ toolsets on those machines, there are Universal CRT redistributable files available for offline distribution. Those redistributable files may be downloaded from one of the KB links above. The Universal CRT redistributable requires that the machine has been updated to the current service pack. So, for example, the redistributable for Windows 7 will only install onto Windows 7 SP1, not Windows 7 RTM.</p>
<p>Because the Universal CRT is a fundamental dependency of the C++ libraries, the Visual C++ redistributable (VCRedist) installs the initial version of the Universal CRT (version 10.0.10240) on machines that don't already have one installed. This version is sufficient to satisfy the C++ library dependencies. If your application depends on a more recent version of the Universal CRT, you must use Windows Update to bring your machine fully up-to-date, or install that version explicitly. It's best to install the Universal C Runtime via Windows Update or an MSU before installing the VCRedist, to avoid potential multiple required reboots.</p>
<p>Not all operating systems are eligible for the most recent Universal C Runtime via Windows Update. On Windows 10, the centrally deployed version matches the version of the operating system. To update the Universal C Runtime further, you must update the operating system. For Windows Vista through Windows 8.1, the latest available Universal C Runtime is currently based on the version included in the Windows 10 Anniversary Update, with additional fixes (version 10.0.14393).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-285150"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285150" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 30, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285105" class="permalink" rel="bookmark">I heven&#039;t been able to use…</a> by <span>Ali (not verified)</span></p>
    <a href="#comment-285150">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285150" class="permalink" rel="bookmark">https://www.ghacks.net/2017…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.ghacks.net/2017/06/06/the-program-cant-start-because-api-ms-win-crt-runtime-l1-1-0-dll-is-missing/" rel="nofollow">https://www.ghacks.net/2017/06/06/the-program-cant-start-because-api-ms…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-285244"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285244" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="....000.....=====......000.">....000.....==… (not verified)</span> said:</p>
      <p class="date-time">November 04, 2019</p>
    </div>
    <a href="#comment-285244">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285244" class="permalink" rel="bookmark">Thanks for your work Dr…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for your work Dr. Nick, is it possible to create a TORRC command to let the user control the minimum level of which nodes version it will connect to which would be more aggressive than the hardcoded Tor version firewall? </p>
<p>This could also allow alpha testing features better in real world, if you wanted to run a client only connect to alpha builds of a certain minimum version.  </p>
<p>Probably a command would be needed each for alpha and 1 for stable tor.  For instance if only stable tor is set minimum tor will act on alpha command as the built in tor version firewall and the same the other way around.  But for testing purposes it might need to accept a higher version than actually exists so you could cut off all connections when testing clients for either the alpha or stable branches.  If possible not sure about that but it would be nice.</p>
<p>It would just be nice if somehow the user could have a more hardened version firewall if he chose that adhered to {se},{de} type notations and strictnodes 1.</p>
<p>Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285326"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285326" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">November 06, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-285244" class="permalink" rel="bookmark">Thanks for your work Dr…</a> by <span content="....000.....=====......000.">....000.....==… (not verified)</span></p>
    <a href="#comment-285326">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285326" class="permalink" rel="bookmark">That would be interesting,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That would be interesting, but a bit dangerous: It's not a great idea for privacy to build paths that look very different from those made by other users.  That said, you're right that it might be useful for testing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-285362"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285362" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Dr.Know (not verified)</span> said:</p>
      <p class="date-time">November 08, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to nickm</p>
    <a href="#comment-285362">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285362" class="permalink" rel="bookmark">Yes, I find it really is…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, I find it really is only useful for big jumps like 4.15 ect......and other big improvements, I am more concerned with testing those services than blending in perfectly and alot of operators are slow to upgrade or don't even have time to follow certain upgraded developments.</p>
<p>But I am glad you like the idea, I do this manually which is why I ask lol maybe I should say pretty please.....but since it's on your radar hope you can assess it's usefulness and capabilities of allowing user controlled firewall.</p>
<p>It would save me a lot of time.  As if certain country codes are set certain actors always like to dump bad nodes with old software which is hard to manually Exclude them when they always change or add and disappear.  </p>
<p>But again it is mostly to be up to date with big jumps for testing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-285251"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-285251" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Know (not verified)</span> said:</p>
      <p class="date-time">November 05, 2019</p>
    </div>
    <a href="#comment-285251">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-285251" class="permalink" rel="bookmark">Actually, I think it would…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Actually, I think it would be better once user tor sets something like Torrc command StableFirewall 4.1.5 or just the first two 4.1 ect......</p>
<p>Than once the firewall adheres to Strictnodes 1 and {se},{ca} ect....isolate weather you want the firewall to adhere individually from the global setting using </p>
<p>EntryNodesUseFirewall 0 or 1 default is 1<br />
MiddleNodesUseFirewall 0 or 1 default is 1<br />
ExitNodesUseFirewall 0 or 1 default is 1</p>
<p>But if StableFirewall is 1 than it adheres globally even when UseEntryGuards 0 is set.</p>
</div>
  </div>
</article>
<!-- Comment END -->
