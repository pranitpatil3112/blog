title: Tor 0.2.2.28-beta and 0.2.2.29-beta are out
---
pub_date: 2011-06-24
---
author: erinn
---
tags:

tor
bug fixes
alpha release
---
categories:

network
releases
---
_html_body:

<p><strong>Changes in version 0.2.2.29-beta - 2011-06-20</strong><br />
  Tor 0.2.2.29-beta reverts an accidental behavior change for users who<br />
  have bridge lines in their torrc but don't want to use them; gets<br />
  us closer to having the control socket feature working on Debian;<br />
  and fixes a variety of smaller bugs.</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Revert the UseBridges option to its behavior before 0.2.2.28-beta.<br />
      When we changed the default behavior to "use bridges if any<br />
      are listed in the torrc", we surprised users who had bridges<br />
      in their torrc files but who didn't actually want to use them.<br />
      Partial resolution for bug 3354.</li>
</ul>

<p><strong>Privacy fixes:</strong></p>

<ul>
<li>Don't attach new streams to old rendezvous circuits after SIGNAL<br />
      NEWNYM. Previously, we would keep using an existing rendezvous<br />
      circuit if it remained open (i.e. if it were kept open by a<br />
      long-lived stream, or if a new stream were attached to it before<br />
      Tor could notice that it was old and no longer in use). Bugfix on<br />
      0.1.1.15-rc; fixes bug 3375.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Fix a bug when using ControlSocketsGroupWritable with User. The<br />
      directory's group would be checked against the current group, not<br />
      the configured group. Patch by Jérémy Bobbio. Fixes bug 3393;<br />
      bugfix on 0.2.2.26-beta.</li>
<li>Make connection_printf_to_buf()'s behaviour sane. Its callers<br />
      expect it to emit a CRLF iff the format string ends with CRLF;<br />
      it actually emitted a CRLF iff (a) the format string ended with<br />
      CRLF or (b) the resulting string was over 1023 characters long or<br />
      (c) the format string did not end with CRLF *and* the resulting<br />
      string was 1021 characters long or longer. Bugfix on 0.1.1.9-alpha;<br />
      fixes part of bug 3407.</li>
<li>Make send_control_event_impl()'s behaviour sane. Its callers<br />
      expect it to always emit a CRLF at the end of the string; it<br />
      might have emitted extra control characters as well. Bugfix on<br />
      0.1.1.9-alpha; fixes another part of bug 3407.</li>
<li>Make crypto_rand_int() check the value of its input correctly.<br />
      Previously, it accepted values up to UINT_MAX, but could return a<br />
      negative number if given a value above INT_MAX+1. Found by George<br />
      Kadianakis. Fixes bug 3306; bugfix on 0.2.2pre14.</li>
<li>Avoid a segfault when reading a malformed circuit build state<br />
      with more than INT_MAX entries. Found by wanoskarnet. Bugfix on<br />
      0.2.2.4-alpha.</li>
<li>When asked about a DNS record type we don't support via a<br />
      client DNSPort, reply with NOTIMPL rather than an empty<br />
      reply. Patch by intrigeri. Fixes bug 3369; bugfix on 2.0.1-alpha.</li>
<li>Fix a rare memory leak during stats writing. Found by coverity.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Update to the June 1 2011 Maxmind GeoLite Country database.
</li>
</ul>

<p><strong>Code simplifications and refactoring:</strong></p>

<ul>
<li>Remove some dead code as indicated by coverity.</li>
<li>Remove a few dead assignments during router parsing. Found by<br />
      coverity.</li>
<li>Add some forgotten return value checks during unit tests. Found<br />
      by coverity.</li>
<li>Don't use 1-bit wide signed bit fields. Found by coverity.</li>
</ul>

<p><strong>Changes in version 0.2.2.28-beta - 2011-06-04</strong><br />
  Tor 0.2.2.28-beta makes great progress towards a new stable release: we<br />
  fixed a big bug in whether relays stay in the consensus consistently,<br />
  we moved closer to handling bridges and hidden services correctly,<br />
  and we started the process of better handling the dreaded "my Vidalia<br />
  died, and now my Tor demands a password when I try to reconnect to it"<br />
  usability issue.</p>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Don't decide to make a new descriptor when receiving a HUP signal.<br />
      This bug has caused a lot of 0.2.2.x relays to disappear from the<br />
      consensus periodically. Fixes the most common case of triggering<br />
      bug 1810; bugfix on 0.2.2.7-alpha.</li>
<li>Actually allow nameservers with IPv6 addresses. Fixes bug 2574.</li>
<li>Don't try to build descriptors if "ORPort auto" is set and we<br />
      don't know our actual ORPort yet. Fix for bug 3216; bugfix on<br />
      0.2.2.26-beta.</li>
<li>Resolve a crash that occurred when setting BridgeRelay to 1 with<br />
      accounting enabled. Fixes bug 3228; bugfix on 0.2.2.18-alpha.</li>
<li>Apply circuit timeouts to opened hidden-service-related circuits<br />
      based on the correct start time. Previously, we would apply the<br />
      circuit build timeout based on time since the circuit's creation;<br />
      it was supposed to be applied based on time since the circuit<br />
      entered its current state. Bugfix on 0.0.6; fixes part of bug 1297.</li>
<li>Use the same circuit timeout for client-side introduction<br />
      circuits as for other four-hop circuits, rather than the timeout<br />
      for single-hop directory-fetch circuits; the shorter timeout may<br />
      have been appropriate with the static circuit build timeout in<br />
      0.2.1.x and earlier, but caused many hidden service access attempts<br />
      to fail with the adaptive CBT introduced in 0.2.2.2-alpha. Bugfix<br />
      on 0.2.2.2-alpha; fixes another part of bug 1297.</li>
<li>In ticket 2511 we fixed a case where you could use an unconfigured<br />
      bridge if you had configured it as a bridge the last time you ran<br />
      Tor. Now fix another edge case: if you had configured it as a bridge<br />
      but then switched to a different bridge via the controller, you<br />
      would still be willing to use the old one. Bugfix on 0.2.0.1-alpha;<br />
      fixes bug 3321.</li>
</ul>

<p><strong>Major features:</strong></p>

<ul>
<li>Add an __OwningControllerProcess configuration option and a<br />
      TAKEOWNERSHIP control-port command. Now a Tor controller can ensure<br />
      that when it exits, Tor will shut down. Implements feature 3049.</li>
<li>If "UseBridges 1" is set and no bridges are configured, Tor will<br />
      now refuse to build any circuits until some bridges are set.<br />
      If "UseBridges auto" is set, Tor will use bridges if they are<br />
      configured and we are not running as a server, but otherwise will<br />
      make circuits as usual. The new default is "auto". Patch by anonym,<br />
      so the Tails LiveCD can stop automatically revealing you as a Tor<br />
      user on startup.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Fix warnings from GCC 4.6's "-Wunused-but-set-variable" option.</li>
<li>Remove a trailing asterisk from "exit-policy/default" in the<br />
      output of the control port command "GETINFO info/names". Bugfix<br />
      on 0.1.2.5-alpha.</li>
<li>Use a wide type to hold sockets when built for 64-bit Windows builds.<br />
      Fixes bug 3270.</li>
<li>Warn when the user configures two HiddenServiceDir lines that point<br />
      to the same directory. Bugfix on 0.0.6 (the version introducing<br />
      HiddenServiceDir); fixes bug 3289.</li>
<li>Remove dead code from rend_cache_lookup_v2_desc_as_dir. Fixes<br />
      part of bug 2748; bugfix on 0.2.0.10-alpha.</li>
<li>Log malformed requests for rendezvous descriptors as protocol<br />
      warnings, not warnings. Also, use a more informative log message<br />
      in case someone sees it at log level warning without prior<br />
      info-level messages. Fixes the other part of bug 2748; bugfix<br />
      on 0.2.0.10-alpha.</li>
<li>Clear the table recording the time of the last request for each<br />
      hidden service descriptor from each HS directory on SIGNAL NEWNYM.<br />
      Previously, we would clear our HS descriptor cache on SIGNAL<br />
      NEWNYM, but if we had previously retrieved a descriptor (or tried<br />
      to) from every directory responsible for it, we would refuse to<br />
      fetch it again for up to 15 minutes. Bugfix on 0.2.2.25-alpha;<br />
      fixes bug 3309.</li>
<li>Fix a log message that said "bits" while displaying a value in<br />
      bytes. Found by wanoskarnet. Fixes bug 3318; bugfix on<br />
      0.2.0.1-alpha.</li>
<li>When checking for 1024-bit keys, check for 1024 bits, not 128<br />
      bytes. This allows Tor to correctly discard keys of length 1017<br />
      through 1023. Bugfix on 0.0.9pre5.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Relays now log the reason for publishing a new relay descriptor,<br />
      so we have a better chance of hunting down instances of bug 1810.<br />
      Resolves ticket 3252.</li>
<li>Revise most log messages that refer to nodes by nickname to<br />
      instead use the "$key=nickname at address" format. This should be<br />
      more useful, especially since nicknames are less and less likely<br />
      to be unique. Resolves ticket 3045.</li>
<li>Log (at info level) when purging pieces of hidden-service-client<br />
      state because of SIGNAL NEWNYM.</li>
</ul>

<p><strong>Removed options:</strong></p>

<ul>
<li>Remove undocumented option "-F" from tor-resolve: it hasn't done<br />
      anything since 0.2.1.16-rc.</li>
</ul>

