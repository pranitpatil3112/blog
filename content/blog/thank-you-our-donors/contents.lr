title: Thank you to our donors
---
pub_date: 2011-12-28
---
author: phobos
---
tags:

tor
anonymity online
donor appreciation
hugs
thanks
fighting for the future
---
categories:

fundraising
network
---
_html_body:

<p>2011 was an exciting year for communications security. Online communications helped to support activists in the Middle East's "Arab Spring" as they toppled Tunisia's Ben Ali and Egypt's Mubarak. Tor's entry nodes and hidden "bridge" entry points have seen <a href="https://metrics.torproject.org/users.html" rel="nofollow">increased usage</a> from Iran and Syria, as citizens there seek to communicate securely and evade government censorship.  Secretary of State Clinton has made Internet Freedom part of the U.S. State Department's agenda, while here in the United States, advertisers have developed more sophisticated ways to track browsers' online activity.</p>

<p>The Tor Project can help, but the censors and snoops are never very far behind. We must keep improving our software and network, researching its security against new threats, and training users to communicate safely. As a non-profit, we depend on your donations of money, relays, and advocacy to keep making progress.</p>

<p>In the past year, Tor released new versions to improve security and blocking resistance, <a href="https://blog.torproject.org/blog/iran-blocks-tor-tor-releases-same-day-fix" rel="nofollow">including a same-day fix</a> to a block detected in Iran. We have enhanced translations in more than a dozen languages including Farsi, Arabic, and Chinese; presented security and anonymity research; and taught security practices to groups including journalists, activists, law enforcement, and survivors of domestic violence.</p>

<p>Please help us keep the Internet open and private for all.</p>

<p>If you would like to keep up to date with Tor, please visit our donor thank you page at <a href="https://www.torproject.org/donate/thankyou" rel="nofollow">https://www.torproject.org/donate/thankyou</a>.</p>

<p>Donate securely online at <a href="https://www.torproject.org/donate" rel="nofollow">https://www.torproject.org/donate</a></p>

