title: Arm Release 1.4.3
---
pub_date: 2011-07-17
---
author: atagar
---
tags: arm
---
categories: network
---
_html_body:

<p>Hi all. A new release of <a href="http://www.atagar.com/arm/" rel="nofollow">arm</a> is now available. This completes the codebase refactoring project that's been a year in the works and provides numerous performance, usability, and stability improvements...</p>

<ul>
<li>Relay Setup Wizard
<p>Setting up a relay can be tricky for new users. In headless environments this means navigating Tor's massive, user unfriendly man page and even when Vidalia's an option it makes relatively poor exit configurations. Starting arm before Tor now provides instructions for auto-generating a good relay setup...</p>
<ul>
<li><a href="http://www.atagar.com/transfer/tmp/arm_wizard1.png" rel="nofollow">Selection for what you'd like to be</a></li>
<li><a href="http://www.atagar.com/transfer/tmp/arm_wizard2.png" rel="nofollow">Picking your relay options</a></li>
<li><a href="http://www.atagar.com/transfer/tmp/arm_wizard3.png" rel="nofollow">Confirmation for the configuration it's making</a></li>
</ul>
</li>
<li>Menu Interface
<p>All of arm's capabilities are now available via a <a href="http://www.atagar.com/transfer/tmp/arm_menu.png" rel="nofollow">simple menu interface</a>.</p>
</li>
<li>Arm Gui Prototype
<p>Over this summer Kamran Khan has been <a href="http://inspirated.com/2011/06/28/summer-of-code-progress-graphs-logs-and-acid" rel="nofollow">working on a GTK frontend</a> for arm as part of Google Summer of Code. The initial prototype is ready!</p>
</li>
<li>Performance Improvements
<p>Several arm and TorCtl performance fixes providing a 83% faster startup time, 12% lower memory usage, and instantaneous shutdown.</p>
</li>
<li>Improved Platform Support
<p>Vastly better support for Mac OSX. Arm has also been <a href="http://packages.debian.org/squeeze-backports/tor-arm" rel="nofollow">backported to Debian Squeeze</a> and <a href="https://bugs.launchpad.net/maverick-backports/+bug/721886" rel="nofollow">Ubuntu Lucid / Maverick</a>.</p>
</li>
<li>... etc
<p>Options for requesting a new identity, shutting down Tor, reconnecting if Tor's been restarted and <a href="http://www.atagar.com/arm/releaseNotes.php#1.4.3" rel="nofollow">many, many bugfixes</a>.</p>
</li>
</ul>

<p>Cheers! -Damian</p>

---
_comments:

<a id="comment-10563"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10563" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 21, 2011</p>
    </div>
    <a href="#comment-10563">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10563" class="permalink" rel="bookmark">I would like to know </a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would like to know  whether Tor could be useful to me too for the use I make of internet. I use internet mostly to buy things on ebay or amazon and I would like to know if Tor is also usefull to keep my credit card account completely safe from hackers and so on. I use firefox and if I download Tor which version is good for me? I hope you can help me. Thank you</p>
</div>
  </div>
</article>
<!-- Comment END -->
