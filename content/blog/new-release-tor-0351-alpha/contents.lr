title: New release: Tor 0.3.5.1-alpha
---
pub_date: 2018-09-18
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.3.5.1-alpha from the usual place on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release some time this week.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.3.5.1-alpha is the first release of the 0.3.5.x series. It adds client authorization for modern (v3) onion services, improves bootstrap reporting, begins reorganizing Tor's codebase, adds optional support for NSS in place of OpenSSL, and much more.</p>
<h2>Changes in version 0.3.5.1-alpha - 2018-09-18</h2>
<ul>
<li>Major features (onion services, UI change):
<ul>
<li>For a newly created onion service, the default version is now 3. Tor still supports existing version 2 services, but the operator now needs to set "HiddenServiceVersion 2" in order to create a new version 2 service. For existing services, Tor now learns the version by reading the key file. Closes ticket <a href="https://bugs.torproject.org/27215">27215</a>.</li>
</ul>
</li>
<li>Major features (relay, UI change):
<ul>
<li>Relays no longer run as exits by default. If the "ExitRelay" option is auto (or unset), and no exit policy is specified with ExitPolicy or ReducedExitPolicy, we now treat ExitRelay as 0. Previously in this case, we allowed exit traffic and logged a warning message. Closes ticket <a href="https://bugs.torproject.org/21530">21530</a>. Patch by Neel Chauhan.</li>
<li>Tor now validates that the ContactInfo config option is valid UTF- 8 when parsing torrc. Closes ticket <a href="https://bugs.torproject.org/27428">27428</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major features (bootstrap):
<ul>
<li>Don't report directory progress until after a connection to a relay or bridge has succeeded. Previously, we'd report 80% progress based on cached directory information when we couldn't even connect to the network. Closes ticket <a href="https://bugs.torproject.org/27169">27169</a>.</li>
</ul>
</li>
<li>Major features (new code layout):
<ul>
<li>
<p>Nearly all of Tor's source code has been moved around into more logical places. The "common" directory is now divided into a set of libraries in "lib", and files in the "or" directory have been split into "core" (logic absolutely needed for onion routing), "feature" (independent modules in Tor), and "app" (to configure and invoke the rest of Tor). See doc/HACKING/CodeStructure.md for more information. Closes ticket <a href="https://bugs.torproject.org/26481">26481</a>.</p>
<p>This refactoring is not complete: although the libraries have been refactored to be acyclic, the main body of Tor is still too interconnected. We will attempt to improve this in the future.</p>
</li>
</ul>
</li>
<li>Major features (onion services v3):
<ul>
<li>Implement onion service client authorization at the descriptor level: only authorized clients can decrypt a service's descriptor to find out how to contact it. A new torrc option was added to control this client side: ClientOnionAuthDir &lt;path&gt;. On the service side, if the "authorized_clients/" directory exists in the onion service directory path, client configurations are read from the files within. See the manpage for more details. Closes ticket <a href="https://bugs.torproject.org/27547">27547</a>. Patch done by Suphanat Chunhapanya (haxxpop).</li>
<li>Improve revision counter generation in next-gen onion services. Onion services can now scale by hosting multiple instances on different hosts without synchronization between them, which was previously impossible because descriptors would get rejected by HSDirs. Addresses ticket <a href="https://bugs.torproject.org/25552">25552</a>.</li>
</ul>
</li>
<li>Major features (portability, cryptography, experimental, TLS):
<ul>
<li>
<p>Tor now has the option to compile with the NSS library instead of OpenSSL. This feature is experimental, and we expect that bugs may remain. It is mainly intended for environments where Tor's performance is not CPU-bound, and where NSS is already known to be installed. To try it out, configure Tor with the --enable-nss flag. Closes tickets 26631, 26815, and 26816.</p>
<p>If you are experimenting with this option and using an old cached consensus, Tor may fail to start. To solve this, delete your "cached-consensus" and "cached-microdesc-consensus" files, (if present), and restart Tor.</p>
</li>
</ul>
</li>
<li>Major bugfixes (directory authority):
<ul>
<li>Actually check that the address we get from DirAuthority configuration line is valid IPv4. Explicitly disallow DirAuthority address to be a DNS hostname. Fixes bug <a href="https://bugs.torproject.org/26488">26488</a>; bugfix on 0.1.2.10-rc.</li>
</ul>
</li>
<li>Major bugfixes (restart-in-process):
<ul>
<li>Fix a use-after-free error that could be caused by passing Tor an impossible set of options that would fail during options_act(). Fixes bug <a href="https://bugs.torproject.org/27708">27708</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor features (admin tools):
<ul>
<li>Add a new --key-expiration option to print the expiration date of the signing cert in an ed25519_signing_cert file. Resolves issue 19506.</li>
</ul>
</li>
<li>Minor features (build):
<ul>
<li>If you pass the "--enable-pic" option to configure, Tor will try to tell the compiler to build position-independent code suitable to link into a dynamic library. (The default remains -fPIE, for code suitable for a relocatable executable.) Closes ticket <a href="https://bugs.torproject.org/23846">23846</a>.</li>
</ul>
</li>
<li>Minor features (code correctness, testing):
<ul>
<li>Tor's build process now includes a "check-includes" make target to verify that no module of Tor relies on any headers from a higher- level module. We hope to use this feature over time to help refactor our codebase. Closes ticket <a href="https://bugs.torproject.org/26447">26447</a>.</li>
</ul>
</li>
<li>Minor features (code layout):
<ul>
<li>We have a new "lowest-level" error-handling API for use by code invoked from within the logging module. With this interface, the logging code is no longer at risk of calling into itself if a failure occurs while it is trying to log something. Closes ticket <a href="https://bugs.torproject.org/26427">26427</a>.</li>
</ul>
</li>
<li>Minor features (compilation):
<ul>
<li>Tor's configure script now supports a --with-malloc= option to select your malloc implementation. Supported options are "tcmalloc", "jemalloc", "openbsd" (deprecated), and "system" (the default). Addresses part of ticket <a href="https://bugs.torproject.org/20424">20424</a>. Based on a patch from Alex Xu.</li>
</ul>
</li>
<li>Minor features (config):
<ul>
<li>The "auto" keyword in torrc is now case-insensitive. Closes ticket <a href="https://bugs.torproject.org/26663">26663</a>.</li>
</ul>
</li>
<li>Minor features (continuous integration):
<ul>
<li>Don't do a distcheck with --disable-module-dirauth in Travis. Implements ticket <a href="https://bugs.torproject.org/27252">27252</a>.</li>
<li>Install libcap-dev and libseccomp2-dev so these optional dependencies get tested on Travis CI. Closes ticket <a href="https://bugs.torproject.org/26560">26560</a>.</li>
<li>Only run one online rust build in Travis, to reduce network errors. Skip offline rust builds on Travis for Linux gcc, because they're redundant. Implements ticket <a href="https://bugs.torproject.org/27252">27252</a>.</li>
<li>Skip gcc on OSX in Travis CI, because it's rarely used. Skip a duplicate hardening-off build in Travis on Tor 0.2.9. Skip gcc on Linux with default settings, because all the non-default builds use gcc on Linux. Implements ticket <a href="https://bugs.torproject.org/27252">27252</a>.</li>
</ul>
</li>
<li>Minor features (controller):
<ul>
<li>Emit CIRC_BW events as soon as we detect that we processed an invalid or otherwise dropped cell on a circuit. This allows vanguards and other controllers to react more quickly to dropped cells. Closes ticket <a href="https://bugs.torproject.org/27678">27678</a>.</li>
<li>For purposes of CIRC_BW-based dropped cell detection, track half- closed stream ids, and allow their ENDs, SENDMEs, DATA and path bias check cells to arrive without counting it as dropped until either the END arrives, or the windows are empty. Closes ticket <a href="https://bugs.torproject.org/25573">25573</a>.</li>
<li>Implement a 'GETINFO md/all' controller command to enable getting all known microdescriptors. Closes ticket <a href="https://bugs.torproject.org/8323">8323</a>.</li>
<li>The GETINFO command now support an "uptime" argument, to return Tor's uptime in seconds. Closes ticket <a href="https://bugs.torproject.org/25132">25132</a>.</li>
</ul>
</li>
<li>Minor features (denial-of-service avoidance):
<ul>
<li>Make our OOM handler aware of the DNS cache so that it doesn't fill up the memory. This check is important for our DoS mitigation subsystem. Closes ticket <a href="https://bugs.torproject.org/18642">18642</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (development):
<ul>
<li>Tor's makefile now supports running the "clippy" Rust style tool on our Rust code. Closes ticket <a href="https://bugs.torproject.org/22156">22156</a>.</li>
</ul>
</li>
<li>Minor features (directory authority):
<ul>
<li>There is no longer an artificial upper limit on the length of bandwidth lines. Closes ticket <a href="https://bugs.torproject.org/26223">26223</a>.</li>
<li>When a bandwidth file is used to obtain the bandwidth measurements, include this bandwidth file headers in the votes. Closes ticket <a href="https://bugs.torproject.org/3723">3723</a>.</li>
<li>Improved support for networks with only a single authority or a single fallback directory. Patch from Gabriel Somlo. Closes ticket <a href="https://bugs.torproject.org/25928">25928</a>.</li>
</ul>
</li>
<li>Minor features (embedding API):
<ul>
<li>The Tor controller API now supports a function to launch Tor with a preconstructed owning controller FD, so that embedding applications don't need to manage controller ports and authentication. Closes ticket <a href="https://bugs.torproject.org/24204">24204</a>.</li>
<li>The Tor controller API now has a function that returns the name and version of the backend implementing the API. Closes ticket <a href="https://bugs.torproject.org/26947">26947</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the September 6 2018 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/27631">27631</a>.</li>
</ul>
</li>
<li>Minor features (memory management):
<ul>
<li>Get Libevent to use the same memory allocator as Tor, by calling event_set_mem_functions() during initialization. Resolves ticket <a href="https://bugs.torproject.org/8415">8415</a>.</li>
</ul>
</li>
<li>Minor features (memory usage):
<ul>
<li>When not using them, store legacy TAP public onion keys in DER- encoded format, rather than as expanded public keys. This should save several megabytes on typical clients. Closes ticket <a href="https://bugs.torproject.org/27246">27246</a>.</li>
</ul>
</li>
<li>Minor features (OpenSSL):
<ul>
<li>When possible, use RFC5869 HKDF implementation from OpenSSL rather than our own. Resolves ticket <a href="https://bugs.torproject.org/19979">19979</a>.</li>
</ul>
</li>
<li>Minor features (Rust, code quality):
<ul>
<li>Improve rust code quality in the rust protover implementation by making it more idiomatic. Includes changing an internal API to take &amp;str instead of &amp;String. Closes ticket <a href="https://bugs.torproject.org/26492">26492</a>.</li>
</ul>
</li>
<li>Minor features (testing):
<ul>
<li>Add scripts/test/chutney-git-bisect.sh, for bisecting using chutney. Implements ticket <a href="https://bugs.torproject.org/27211">27211</a>.</li>
</ul>
</li>
<li>Minor features (tor-resolve):
<ul>
<li>The tor-resolve utility can now be used with IPv6 SOCKS proxies. Side-effect of the refactoring for ticket <a href="https://bugs.torproject.org/26526">26526</a>.</li>
</ul>
</li>
<li>Minor features (UI):
<ul>
<li>Log each included configuration file or directory as we read it, to provide more visibility about where Tor is reading from. Patch from Unto Sten; closes ticket <a href="https://bugs.torproject.org/27186">27186</a>.</li>
<li>Lower log level of "Scheduler type KIST has been enabled" to INFO. Closes ticket <a href="https://bugs.torproject.org/26703">26703</a>.</li>
</ul>
</li>
<li>Minor bugfixes (bootstrap):
<ul>
<li>Try harder to get descriptors in non-exit test networks, by using the mid weight for the third hop when there are no exits. Fixes bug <a href="https://bugs.torproject.org/27237">27237</a>; bugfix on 0.2.6.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (C correctness):
<ul>
<li>Avoid casting smartlist index to int implicitly, as it may trigger a warning (-Wshorten-64-to-32). Fixes bug <a href="https://bugs.torproject.org/26282">26282</a>; bugfix on 0.2.3.13-alpha, 0.2.7.1-alpha and 0.2.1.1-alpha.</li>
<li>Use time_t for all values in predicted_ports_prediction_time_remaining(). Rework the code that computes difference between durations/timestamps. Fixes bug <a href="https://bugs.torproject.org/27165">27165</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (client, memory usage):
<ul>
<li>When not running as a directory cache, there is no need to store the text of the current consensus networkstatus in RAM. Previously, however, clients would store it anyway, at a cost of over 5 MB. Now, they do not. Fixes bug <a href="https://bugs.torproject.org/27247">27247</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (client, reachableaddresses):
<ul>
<li>Instead of adding a "reject *:*" line to ReachableAddresses when loading the configuration, add one to the policy after parsing it in parse_reachable_addresses(). This prevents extra "reject *.*" lines from accumulating on reloads. Fixes bug <a href="https://bugs.torproject.org/20874">20874</a>; bugfix on 0.1.1.5-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (code quality):
<ul>
<li>Rename sandbox_getaddrinfo() and other functions to no longer misleadingly suggest that they are sandbox-only. Fixes bug <a href="https://bugs.torproject.org/26525">26525</a>; bugfix on 0.2.7.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (configuration, Onion Services):
<ul>
<li>In rend_service_parse_port_config(), disallow any input to remain after address-port pair was parsed. This will catch address and port being whitespace-separated by mistake of the user. Fixes bug <a href="https://bugs.torproject.org/27044">27044</a>; bugfix on 0.2.9.10.</li>
</ul>
</li>
<li>Minor bugfixes (continuous integration):
<ul>
<li>Stop reinstalling identical packages in our Windows CI. Fixes bug <a href="https://bugs.torproject.org/27464">27464</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller):
<ul>
<li>Consider all routerinfo errors other than "not a server" to be transient for the purpose of "GETINFO exit-policy/*" controller request. Print stacktrace in the unlikely case of failing to recompute routerinfo digest. Fixes bug <a href="https://bugs.torproject.org/27034">27034</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory connection shutdown):
<ul>
<li>Avoid a double-close when shutting down a stalled directory connection. Fixes bug <a href="https://bugs.torproject.org/26896">26896</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (HTTP tunnel):
<ul>
<li>Fix a bug warning when closing an HTTP tunnel connection due to an HTTP request we couldn't handle. Fixes bug <a href="https://bugs.torproject.org/26470">26470</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (ipv6):
<ul>
<li>In addrs_in_same_network_family(), we choose the subnet size based on the IP version (IPv4 or IPv6). Previously, we chose a fixed subnet size of /16 for both IPv4 and IPv6 addresses. Fixes bug <a href="https://bugs.torproject.org/15518">15518</a>; bugfix on 0.2.3.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>As a precaution, do an early return from log_addr_has_changed() if Tor is running as client. Also, log a stack trace for debugging as this function should only be called when Tor runs as server. Fixes bug <a href="https://bugs.torproject.org/26892">26892</a>; bugfix on 0.1.1.9-alpha.</li>
<li>Refrain from mentioning bug <a href="https://bugs.torproject.org/21018">21018</a> in the logs, as it is already fixed. Fixes bug <a href="https://bugs.torproject.org/25477">25477</a>; bugfix on 0.2.9.8.</li>
</ul>
</li>
<li>Minor bugfixes (logging, documentation):
<ul>
<li>When SafeLogging is enabled, scrub IP address in channel_tls_process_netinfo_cell(). Also, add a note to manpage that scrubbing is not guaranteed on loglevels below Notice. Fixes bug <a href="https://bugs.torproject.org/26882">26882</a>; bugfix on 0.2.4.10-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (netflow padding):
<ul>
<li>Ensure circuitmux queues are empty before scheduling or sending padding. Fixes bug <a href="https://bugs.torproject.org/25505">25505</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v2):
<ul>
<li>Log at level "info", not "warning", in the case that we do not have a consensus when a .onion request comes in. This can happen normally while bootstrapping. Fixes bug <a href="https://bugs.torproject.org/27040">27040</a>; bugfix on 0.2.8.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v3):
<ul>
<li>When the onion service directory can't be created or has the wrong permissions, do not log a stack trace. Fixes bug <a href="https://bugs.torproject.org/27335">27335</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (OS compatibility):
<ul>
<li>Properly handle configuration changes that move a listener to/from wildcard IP address. If the first attempt to bind a socket fails, close the old listener and try binding the socket again. Fixes bug <a href="https://bugs.torproject.org/17873">17873</a>; bugfix on 0.0.8pre-1.</li>
</ul>
</li>
<li>Minor bugfixes (performance)::
<ul>
<li>Rework node_is_a_configured_bridge() to no longer call node_get_all_orports(), which was performing too many memory allocations. Fixes bug <a href="https://bugs.torproject.org/27224">27224</a>; bugfix on 0.2.3.9.</li>
</ul>
</li>
<li>Minor bugfixes (relay statistics):
<ul>
<li>Update relay descriptor on bandwidth changes only when the uptime is smaller than 24h, in order to reduce the efficiency of guard discovery attacks. Fixes bug <a href="https://bugs.torproject.org/24104">24104</a>; bugfix on 0.1.1.6-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relays):
<ul>
<li>Consider the fact that we'll be making direct connections to our entry and guard nodes when computing the fraction of nodes that have their descriptors. Also, if we are using bridges and there is at least one bridge with a full descriptor, treat the fraction of guards available as 100%. Fixes bug <a href="https://bugs.torproject.org/25886">25886</a>; bugfix on 0.2.4.10-alpha. Patch by Neel Chauhan.</li>
<li>Update the message logged on relays when DirCache is disabled. Since 0.3.3.5-rc, authorities require DirCache (V2Dir) for the Guard flag. Fixes bug <a href="https://bugs.torproject.org/24312">24312</a>; bugfix on 0.3.3.5-rc.</li>
</ul>
</li>
<li>Minor bugfixes (rust, protover):
<ul>
<li>Compute protover votes correctly in the rust version of the protover code. Previously, the protover rewrite in 24031 allowed repeated votes from the same voter for the same protocol version to be counted multiple times in protover_compute_vote(). Fixes bug <a href="https://bugs.torproject.org/27649">27649</a>; bugfix on 0.3.3.5-rc.</li>
<li>Reject protover names that contain invalid characters. Fixes bug <a href="https://bugs.torproject.org/27687">27687</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Fix two unit tests to work when HOME environment variable is not set. Fixes bug <a href="https://bugs.torproject.org/27096">27096</a>; bugfix on 0.2.8.1-alpha.</li>
<li>If a unit test running in a subprocess exits abnormally or with a nonzero status code, treat the test as having failed, even if the test reported success. Without this fix, memory leaks don't cause the tests to fail, even with LeakSanitizer. Fixes bug <a href="https://bugs.torproject.org/27658">27658</a>; bugfix on 0.2.2.4-alpha.</li>
<li>When logging a version mismatch in our openssl_version tests, report the actual offending version strings. Fixes bug <a href="https://bugs.torproject.org/26152">26152</a>; bugfix on 0.2.9.1-alpha.</li>
<li>Fix forking tests on Windows when there is a space somewhere in the path. Fixes bug <a href="https://bugs.torproject.org/26437">26437</a>; bugfix on 0.2.2.4-alpha.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>'updateFallbackDirs.py' now ignores the blacklist file, as it's not longer needed. Closes ticket <a href="https://bugs.torproject.org/26502">26502</a>.</li>
<li>Include paths to header files within Tor are now qualified by directory within the top-level src directory.</li>
<li>Many structures have been removed from the centralized "or.h" header, and moved into their own headers. This will allow us to reduce the number of places in the code that rely on each structure's contents and layout. Closes ticket <a href="https://bugs.torproject.org/26383">26383</a>.</li>
<li>Remove ATTR_NONNULL macro from codebase. Resolves ticket <a href="https://bugs.torproject.org/26527">26527</a>.</li>
<li>Remove GetAdaptersAddresses_fn_t. The code that used it was removed as part of the 26481 refactor. Closes ticket <a href="https://bugs.torproject.org/27467">27467</a>.</li>
<li>Rework Tor SOCKS server code to use Trunnel and benefit from autogenerated functions for parsing and generating SOCKS wire format. New implementation is cleaner, more maintainable and should be less prone to heartbleed-style vulnerabilities. Implements a significant fraction of ticket <a href="https://bugs.torproject.org/3569">3569</a>.</li>
<li>Split sampled_guards_update_from_consensus() and select_entry_guard_for_circuit() into subfunctions. In entry_guards_update_primary() unite three smartlist enumerations into one and move smartlist comparison code out of the function. Closes ticket <a href="https://bugs.torproject.org/21349">21349</a>.</li>
<li>Tor now assumes that you have standards-conformant stdint.h and inttypes.h headers when compiling. Closes ticket <a href="https://bugs.torproject.org/26626">26626</a>.</li>
<li>Unify our bloom filter logic. Previously we had two copies of this code: one for routerlist filtering, and one for address set calculations. Closes ticket <a href="https://bugs.torproject.org/26510">26510</a>.</li>
<li>Use the simpler strcmpstart() helper in rend_parse_v2_service_descriptor instead of strncmp(). Closes ticket <a href="https://bugs.torproject.org/27630">27630</a>.</li>
<li>Utility functions that can perform a DNS lookup are now wholly separated from those that can't, in separate headers and C modules. Closes ticket <a href="https://bugs.torproject.org/26526">26526</a>.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Copy paragraph and URL to Tor's code of conduct document from CONTRIBUTING to new CODE_OF_CONDUCT file. Resolves ticket <a href="https://bugs.torproject.org/26638">26638</a>.</li>
<li>Remove old instructions from INSTALL document. Closes ticket <a href="https://bugs.torproject.org/26588">26588</a>.</li>
<li>Warn users that they should not include MyFamily line(s) in their torrc when running Tor bridge. Closes ticket <a href="https://bugs.torproject.org/26908">26908</a>.</li>
</ul>
</li>
<li>Removed features:
<ul>
<li>Tor no longer supports building with the dmalloc library. For debugging memory issues, we suggest using gperftools or msan instead. Closes ticket <a href="https://bugs.torproject.org/26426">26426</a>.</li>
<li>Tor no longer attempts to run on Windows environments without the GetAdaptersAddresses() function. This function has existed since Windows XP, which is itself already older than we support.</li>
<li>Remove Tor2web functionality for version 2 onion services. The Tor2webMode and Tor2webRendezvousPoints options are now obsolete. (This feature was never shipped in vanilla Tor and it was only possible to use this feature by building the support at compile time. Tor2webMode is not implemented for version 3 onion services.) Closes ticket <a href="https://bugs.torproject.org/26367">26367</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-277398"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277398" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 19, 2018</p>
    </div>
    <a href="#comment-277398">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277398" class="permalink" rel="bookmark">Is there an up to date list…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there an up to date list of country codes for use with Tor?</p>
<p>Like {US} = United States, for example</p>
<p>(I don't mean the geoip files)</p>
<p>I found an old listing online but it is outdated for some countries.</p>
<p>Any pointers would be great, thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277412"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277412" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 20, 2018</p>
    </div>
    <a href="#comment-277412">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277412" class="permalink" rel="bookmark">Can I find &quot;man tor&quot; page…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can I find "man tor" page for this tor version somewhere in web?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277415"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277415" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>tor blocked (not verified)</span> said:</p>
      <p class="date-time">September 20, 2018</p>
    </div>
    <a href="#comment-277415">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277415" class="permalink" rel="bookmark">many block tor.....many many…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>many block tor.....many many site or servers blocking tor</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277446"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277446" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">September 22, 2018</p>
    </div>
    <a href="#comment-277446">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277446" class="permalink" rel="bookmark">What does this means and how…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What does this means and how it works? How can we do that?</p>
<p>"Onion services can now scale by hosting multiple instances on different hosts".</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277559"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277559" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 24, 2018</p>
    </div>
    <a href="#comment-277559">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277559" class="permalink" rel="bookmark">In rend-spec-v3.txt I see
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In <a href="https://gitweb.torproject.org/torspec.git/tree/rend-spec-v3.txt" rel="nofollow">rend-spec-v3.txt</a> I see</p>
<p><em>[TODO: Also specify stealth client authorization.]</em></p>
<p>but nothing is written about it. Only one type of auth ("descriptor") is described. Does it mean other auth types will be also added to specs and tor code? What is not "stealth" in the current "descriptor" auth?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278121"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278121" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 18, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-277559" class="permalink" rel="bookmark">In rend-spec-v3.txt I see
…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278121">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278121" class="permalink" rel="bookmark">Recetly it was marked as…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Recetly it was marked as closed by <a href="https://trac.torproject.org/projects/tor/ticket/27953" rel="nofollow">#27953</a>, but I cannot see how those extra (parent and others) tickets address the issue. Will 'intro' and 'standard' auth types be permanently abandoned?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
