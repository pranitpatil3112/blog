# lektor-tor-blog plugin

This plugin provides some functions specific to the Tor Blog website.

## Global variables

### `default_image_cutoff`

This is a datetime object containing the date after which we stop displaying the
default lead image, to avoid older index pages where lead images weren't commonly
used, to avoid ugly pages with a wall of Tor logos. See tpo/web/blog#40019

### `today`

This is a datetime object set to the current date. It allows
`template/sidebar.html` to filter out past events from the sidebar.

## Build flags

Extra build flags can be passed to the `lektor build` command via the `-f`
switch to alter the build process. Example: `lektor build -f alt-sidebar`.

### `partial-build`

This will cause the plugin to parse all the blog pages in `content/blog` and move
aside any post which is older than 90 days. This results in a *much* faster
build, useful to quick reviews during the authoring process.

This is enabled for CI builds of merge requests.

`partial-build:keep` may be used to bypass the post-build process that moves
back into place the affected content files. This is useful when working with
`lektor serve`.

### `alt-sidebar`

This will trigger the creation of a `sidebar.html` page which contains only the
sidebar content, as well as replacing the sidebar template include (in the base
layout) with an HTML placeholder. After the build, the HTML placeholder is
replaced the actual sidebar HTML. This is another workaround implemented to speed
up Lektor builds.

This is enabled in all CI builds.

### `convert-webp`

This will create WebP alternatives of PNG, JPG or GIF-formatted lead images, for
bandwidth savings and overall performance of blog pages. Navigators which do not
support WebP images such as Safari on older releases of OSX will fall-back
gracefully to the original formats.
